#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)

#include <stdio.h>
#include "../shared/tree.h"
#include <string>

#include <vtkSmartPointer.h>
#include <vtkVersion.h>

#include <vtkParametricFunctionSource.h>
#include <vtkTupleInterpolator.h>
#include <vtkTubeFilter.h>
#include <vtkParametricSpline.h>

#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkSTLWriter.h>
#include <vtkTriangleFilter.h>
#include <vtkAppendPolyData.h>



bool export2stl(Tree* tree, const char* filename)
{
    const int determine_derivative = 3;
    const bool interpoate_radius_spline = false;
    const double nodes_density_decimator = 1.0;
    const char* radius_name = "Radius";
    bool use_boolean = false;
    double downsize = 0.95;

    vtkSmartPointer<vtkPolyData> currentUnion = vtkSmartPointer<vtkPolyData>::New();

    unsigned int branch_index_count = tree->count();
    //branch_index_count = 2;

    for (unsigned int branch_index = 0; branch_index<branch_index_count; branch_index++)
    {
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        vtkSmartPointer<vtkTupleInterpolator> interpolatedRadius = vtkSmartPointer<vtkTupleInterpolator> ::New();

        if(interpoate_radius_spline) interpolatedRadius->SetInterpolationTypeToSpline();
        else interpolatedRadius->SetInterpolationTypeToLinear();

        interpolatedRadius->SetNumberOfComponents(1);
        unsigned int node_index_count = tree->count(branch_index);
        for(unsigned int node_index = 0; node_index < node_index_count; node_index++)
        {
            NodeIn3D node = tree->node(branch_index, node_index);
            points->InsertPoint(node_index, node.x, node.y, node.z);
            double radius = fabs(node.radius);
            interpolatedRadius->AddTuple(node_index, &radius);

            if(node.radius > 0)
            {
                //node.radius = -node.radius * downsize;
                tree->setNode(node, tree->nodeIndex(branch_index, node_index));
                tree->setDiameter(tree->nodeIndex(branch_index, node_index), -node.radius * downsize);
            }

        }
        vtkSmartPointer<vtkParametricSpline> spline = vtkSmartPointer<vtkParametricSpline>::New();
        spline->SetPoints(points);
        spline->ParameterizeByLengthOff();
        spline->SetLeftConstraint(determine_derivative);
        spline->SetRightConstraint(determine_derivative);
        vtkSmartPointer<vtkParametricFunctionSource> functionSource = vtkSmartPointer<vtkParametricFunctionSource>::New();
        functionSource->SetParametricFunction(spline);
        functionSource->SetUResolution(nodes_density_decimator * node_index_count);
        functionSource->Update();

        vtkSmartPointer<vtkDoubleArray> tubeRadius = vtkSmartPointer<vtkDoubleArray>::New();
        tubeRadius->SetName(radius_name);
        unsigned int interpolated_node_index_count = functionSource->GetOutput()->GetNumberOfPoints();
        tubeRadius->SetNumberOfTuples(interpolated_node_index_count);
        double tMin = interpolatedRadius->GetMinimumT();
        double tMax = interpolatedRadius->GetMaximumT();

        for (unsigned int interpolated_node_index = 0; interpolated_node_index < interpolated_node_index_count; ++interpolated_node_index)
        {
            double r[1];
            double t = (tMax - tMin) / (interpolated_node_index_count - 1) * interpolated_node_index + tMin;
            interpolatedRadius->InterpolateTuple(t, r);
            tubeRadius->SetTuple1(interpolated_node_index, r[0]);
        }

        vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
        polyData = functionSource->GetOutput();
        polyData->GetPointData()->AddArray(tubeRadius);
        polyData->GetPointData()->SetActiveScalars(radius_name);

        vtkSmartPointer<vtkTubeFilter> tuber = vtkTubeFilter::New();

#if VTK_MAJOR_VERSION <= 5
        tuber->SetInput(polyData);
#else
        tuber->SetInputData(polyData);
#endif
        tuber->SetNumberOfSides(20);
        tuber->SetVaryRadiusToVaryRadiusByAbsoluteScalar();
        tuber->CappingOn();

        vtkSmartPointer< vtkTriangleFilter > triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
        triangleFilter->SetInputConnection( tuber->GetOutputPort() );
        triangleFilter->Update();

        if(branch_index == 0)
        {
            currentUnion = triangleFilter->GetOutput();
        }
        else if(use_boolean)
        {
            vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
            booleanOperation->SetOperationToUnion();
#if VTK_MAJOR_VERSION <= 5
            booleanOperation->SetInputConnection( 0, currentUnion);
            booleanOperation->SetInputConnection( 1, triangleFilter->GetProducerPort() );
#else
            booleanOperation->SetInputData( 0, currentUnion );
            booleanOperation->SetInputData( 1, triangleFilter->GetOutput());
#endif
            booleanOperation->Update();
            currentUnion = booleanOperation->GetOutput();
        }
        else
        {
            vtkSmartPointer<vtkAppendPolyData> appendFilter =  vtkSmartPointer<vtkAppendPolyData>::New();
#if VTK_MAJOR_VERSION <= 5
            appendFilter->AddInputConnection(currentUnion);
            appendFilter->AddInputConnection(triangleFilter->GetProducerPort());
#else
            appendFilter->AddInputData(currentUnion);
            appendFilter->AddInputData(triangleFilter->GetOutput());
#endif
            appendFilter->Update();
            currentUnion = appendFilter->GetOutput();
        }
    }

    vtkSmartPointer<vtkSTLWriter> stlWriter = vtkSmartPointer<vtkSTLWriter>::New();
    stlWriter->SetFileName(filename);
    stlWriter->SetFileTypeToASCII();
#if VTK_MAJOR_VERSION <= 5
    stlWriter->SetInputConnection(currentUnion);
#else
    stlWriter->SetInputData(currentUnion);
#endif
    stlWriter->Write();

//    vtkSmartPointer<vtkPolyDataMapper> tubeMapper =
//      vtkSmartPointer<vtkPolyDataMapper>::New();
//    tubeMapper->SetInputData(currentUnion);
//    //tubeMapper->SetInputConnection(triangleFilter->GetOutputPort());
//    //tubeMapper->SetScalarRange(tubePolyData->GetScalarRange());
//    vtkSmartPointer<vtkActor> tubeActor = vtkSmartPointer<vtkActor>::New();
//    tubeActor->SetMapper(tubeMapper);
//    vtkSmartPointer<vtkRenderer> renderer =
//      vtkSmartPointer<vtkRenderer>::New();
//    vtkSmartPointer<vtkRenderWindow> renderWindow =
//      vtkSmartPointer<vtkRenderWindow>::New();
//    renderWindow->AddRenderer(renderer);
//    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
//      vtkSmartPointer<vtkRenderWindowInteractor>::New();
//    renderWindowInteractor->SetRenderWindow(renderWindow);
//    renderer->AddActor(tubeActor);
//    renderer->SetBackground(.4, .5, .6);
//    renderWindow->Render();
//    renderWindowInteractor->Start();
    return true;
}


//=============================================================================
// Main function
int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        printf("Usage: %s input output\n", argv[0]);
        printf("Converts vessel tree models saved in VesselKnife format to STL data format\n");
        printf("Version 2015.05.15 by Piotr M. Szczypinski\n");
        return 1;
    }
    Tree tree;
    if(!tree.load(argv[1])) return 2;
    if(!export2stl(&tree, argv[2])) return 4;
    return 0;
}
