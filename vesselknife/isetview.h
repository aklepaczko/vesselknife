#ifndef ISETVIEW_H
#define ISETVIEW_H
typedef enum {NORMAL, THREELEVEL, OFF} ImageVisibilityMode;

class iSetView
{
public:
    virtual void setBackgroundColor(unsigned int background) = 0;
    virtual void setZoom(int z) = 0;
    virtual void setCrossSection(int direction, unsigned int index) = 0;
    virtual void setCrossSection(int direction, bool show) = 0;
    virtual void setImageChannel(char ichannel) = 0;
//    virtual void setImageBrightnessLow(double iminbrightness) = 0;
//    virtual void setImageBrightnessHigh(double imaxbrightness) = 0;
    virtual void setImageGrayWindowLow(double iminwindow) = 0;
    virtual void setImageGrayWindowHigh(double imaxwindow) = 0;
    virtual void setImageVisibility(ImageVisibilityMode ivisibility) = 0;
    virtual void setImageVisibilityAll(char ichannel, double iminwindow, double imaxwindow, ImageVisibilityMode ivisibility) = 0;
    virtual void getCrossSection(int direction, unsigned int* index, bool* show) = 0;
    virtual void getBackgroundColor(unsigned int* background) = 0;
    virtual void getZoom(int* z) = 0;
    virtual void getImageVisibilityAll(char* ochannel, double* ominwindow, double* omaxwindow, ImageVisibilityMode* ovisibility) = 0;
};

#endif // ISETVIEW_H
