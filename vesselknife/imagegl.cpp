#define _USE_MATH_DEFINES

#include <math.h>

#ifdef _WIN32
#include <windows.h>
#endif

//#include <GL/gl.h>		   // Open Graphics Library (OpenGL) header
#include <GL/glu.h>
#include "imagegl.h"

#ifdef _WIN32
#define lround(x) int(x+0.5)
#endif

#define FRUSTUM_FRONT_DISTANCE 30.0f
#define FRUSTUM_BACK_DISTANCE 3000.0f
#define FRUSTUM_SIDE_TO_SIDE 5.0f
#define CENTER_OF_PROJECTION_DISTANCE 100.0f
//#define USE_ALPHA_CHANNEL_IN_OPENGL





void ImageGl::setIdentityMatrix(GLdouble W[16])
{
    W[0] = 1; W[1] = 0; W[2] = 0; W[3] = 0;
    W[4] = 0; W[5] = 1; W[6] = 0; W[7] = 0;
    W[8] = 0; W[9] = 0; W[10] = 1; W[11] = 0;
    W[12] = 0; W[13] = 0; W[14] = 0; W[15] = 1;
}
//---------------------------------------------------------------------------
void ImageGl::copy4to4(GLdouble W[14], GLdouble W1[14])
{
    for(int k =0; k<16; k++) W[k]=W1[k];
}

//---------------------------------------------------------------------------
/*
void ImageGl::copy3to3(GLdouble W[14], GLdouble W1[14])
{
  for(int k =0; k<16; k++) W[k]=W1[k];
}
*/



void ImageGl::shift(int qx, int qy)
{
//    if(uninverted)invert3(mat_1, mat);
//    uninverted = false;
//    mat[12] += (mat_1[4]*qy+mat_1[0]*qx);
//    mat[13] += (mat_1[5]*qy+mat_1[1]*qx);
//    mat[14] += (mat_1[6]*qy+mat_1[2]*qx);
    modelViewMat[12] -= ((qx*modelViewMat[14])/1000);
    modelViewMat[13] -= ((qy*modelViewMat[14])/1000);
}



//---------------------------------------------------------------------------
void ImageGl::rotate(bool d, int l1, int l2)
{
    GLdouble a, b;
    for(int k =0; k<12; k+=4)
    {
        a = cosa * modelViewMat[k + l1] + (d ?sina :-sina) * modelViewMat[k + l2];
        b = (d ?-sina :sina) * modelViewMat[k + l1] + cosa * modelViewMat[k + l2];
        modelViewMat[k + l1] = a;
        modelViewMat[k + l2] = b;
    }
}

//---------------------------------------------------------------------------
void ImageGl::rotatem(double rot, int l1, int l2, GLdouble W[14])
{
    GLdouble a, b;
    GLdouble sina = sin((GLdouble)rot/320.0);
    GLdouble cosa = cos((GLdouble)rot/320.0);

    for(int k =0; k<12; k+=4)
    {
        a = cosa * W[k + l1] + sina * W[k + l2];
        b = -sina * W[k + l1] + cosa * W[k + l2];
        W[k + l1] = a;
        W[k + l2] = b;
    }
}

////---------------------------------------------------------------------------
//void ImageGl::rotatemo(int rot, int l1, int l2)
//{
//    GLdouble matt[16];

//    if(uninverted) invert3(mat_1, mat);
//    uninverted = false;
//    multiply3x3(matt, mato, mat);
//    rotatem(rot, l1, l2, matt);
//    multiply3x3(mato, matt, mat_1);
//}

////---------------------------------------------------------------------------
//void ImageGl::sizemo(GLdouble prop)
//{
//    for(int k =0; k<12; k++)
//    {
//        mato[k] *= prop;
//    }
//}

/*------------------------------------------------------------------------*/
GLdouble ImageGl::determinant(GLdouble W[14])
{
    return
            W[0*4+0]*
            (
            W[1*4+1]*(W[2*4+2]*W[3*4+3]-W[2*4+3]*W[3*4+2])
            -W[1*4+2]*(W[2*4+1]*W[3*4+3]-W[2*4+3]*W[3*4+1])
            +W[1*4+3]*(W[2*4+1]*W[3*4+2]-W[2*4+2]*W[3*4+1])
            )
            -W[0*4+1]*
            (
            W[1*4+0]*(W[2*4+2]*W[3*4+3]-W[2*4+3]*W[3*4+2])
            -W[1*4+2]*(W[2*4+0]*W[3*4+3]-W[2*4+3]*W[3*4+0])
            +W[1*4+3]*(W[2*4+0]*W[3*4+2]-W[2*4+2]*W[3*4+0])
            )
            +W[0*4+2]*
            (
            W[1*4+0]*(W[2*4+1]*W[3*4+3]-W[2*4+3]*W[3*4+1])
            -W[1*4+1]*(W[2*4+0]*W[3*4+3]-W[2*4+3]*W[3*4+0])
            +W[1*4+3]*(W[2*4+0]*W[3*4+1]-W[2*4+1]*W[3*4+0])
            )
            -W[0*4+3]*
            (
            W[1*4+0]*(W[2*4+1]*W[3*4+2]-W[2*4+2]*W[3*4+1])
            -W[1*4+1]*(W[2*4+0]*W[3*4+2]-W[2*4+2]*W[3*4+0])
            +W[1*4+2]*(W[2*4+0]*W[3*4+1]-W[2*4+1]*W[3*4+0])
            );
}

/*------------------------------------------------------------------------*/
GLdouble ImageGl::subdet3of4(GLdouble W[16], int w, int k)
{
    int w0, w1, w2;
    int k0, k1, k2;

    w0=0; if(w0==w) w0++;
    w1=w0+1; if(w1==w) w1++;
    w2=w1+1; if(w2==w) w2++;

    k0=0; if(k0==k) k0++;
    k1=k0+1; if(k1==k) k1++;
    k2=k1+1; if(k2==k) k2++;

    return
            W[w0*4+k0]*(W[w1*4+k1]*W[w2*4+k2]-W[w1*4+k2]*W[w2*4+k1])
            -W[w0*4+k1]*(W[w1*4+k0]*W[w2*4+k2]-W[w1*4+k2]*W[w2*4+k0])
            +W[w0*4+k2]*(W[w1*4+k0]*W[w2*4+k1]-W[w1*4+k1]*W[w2*4+k0]);
}

/*------------------------------------------------------------------------*/
GLdouble ImageGl::invert3(GLdouble I[16], GLdouble W[16])
{
    GLdouble det =
            W[0]*(W[5]*W[10]-W[6]*W[9])
            +W[1]*(W[6]*W[8]-W[4]*W[10])
            +W[2]*(W[4]*W[9]-W[5]*W[8]);

    if(det==0) return 0;
    I[0] = (W[5]*W[10]-W[6]*W[9])/det;
    I[4] = (W[6]*W[8]-W[4]*W[10])/det;
    I[8] = (W[4]*W[9]-W[5]*W[8])/det;
    I[12] = 0.0;
    I[1] = (W[2]*W[9]-W[1]*W[10])/det;
    I[5] = (W[0]*W[10]-W[2]*W[8])/det;
    I[9] = (W[1]*W[8]-W[0]*W[9])/det;
    I[13] = 0.0;
    I[2] = (W[1]*W[6]-W[2]*W[5])/det;
    I[6] = (W[2]*W[4]-W[0]*W[6])/det;
    I[10] = (W[0]*W[5]-W[1]*W[4])/det;
    I[14] = 0.0;
    I[3] = 0.0;
    I[7] = 0.0;
    I[11] = 0.0;
    I[15] = 1.0;
    return det;
}


/*------------------------------------------------------------------------*/
int ImageGl::invert4(GLdouble W[16], GLdouble W1[16])
{
    int k, l;
    GLdouble det;
    det=determinant(W1);
    if(det==0) return 1;

    for(k=0; k<4; k++)
        for(l=0; l<4; l++)
        {
            if((k+l)%2) W[l*4+k]=-subdet3of4(W1, k, l)/det;
            else W[l*4+k]=subdet3of4(W1, k, l)/det;
        }
    return 0;
}

/*------------------------------------------------------------------------*/
void ImageGl::multiply4x4(GLdouble W[16], GLdouble W1[16], GLdouble W2[16])
{
    for(int i1 = 0; i1 < 4; i1++)
        for(int i2 = 0; i2 < 4; i2++)
            W[i1*4+i2] = W2[i1*4]*W1[i2]
                    + W2[i1*4+1]*W1[i2+4]
                    + W2[i1*4+2]*W1[i2+8]
                    + W2[i1*4+3]*W1[i2+12];
}

/*------------------------------------------------------------------------*/
void ImageGl::multiply3x3(GLdouble W[16], GLdouble W1[16], GLdouble W2[16])
{
    for(int i1 = 0; i1 < 3; i1++)
        for(int i2 = 0; i2 < 3; i2++)
            W[i1*4+i2] = W1[i1*4]*W2[i2]+ W1[i1*4+1]*W2[i2+4]+ W1[i1*4+2]*W2[i2+8];
}



























ImageGl::ImageGl(QWidget *parent) :
    QGLWidget(parent, 0, 0)
{
//    for(int n = 0; n < 4; n++) renderlist[n] = 0;
    setIdentityMatrix(modelViewMat);
    modelViewMat[14] = -CENTER_OF_PROJECTION_DISTANCE;
//    setIdentityMatrix(mato);
//    setIdentityMatrix(mat_1);
//    uninverted = true;
    freeze = false;
    image[0] = NULL;
    image[1] = NULL;
    image[2] = NULL;
    image[3] = NULL;
    lastdirectionofvolume = 0;

//    channel = 0;
    minwindow = 0;
    maxwindow = 255;
//    visibility = NORMAL;
    zoomlevel = 0;

    sliceindex[0] = 0;
    sliceindex[1] = 0;
    sliceindex[2] = 0;
    renderslice[0] = true;
    renderslice[1] = true;
    renderslice[2] = true;
    rendervolume = false;
    rendertrees = false;
    //viewballs = 0;
    stereo = 0;
    stereodepth = 0;


//    treeee = NULL;
}

ImageGl::~ImageGl()
{
}



void ImageGl::initializeGL()
{
    initializeGLFunctions();


    // Wartosci i wspolrzedne zrodel swiatla
    GLfloat  diffuseLight[] = {0.7f, 0.7f, 0.7f, 1.0f };
    GLfloat  specular[] = { 0.7f, 0.7f, 0.7f, 1.0f};
    GLfloat  lightPos[] = { 500.0f, -2000.0f, 2000.0f, 1.0f };
    GLfloat  ambientLight[] = {0.3f, 0.3f, 0.3f, 1.0f };
    GLfloat  specref[] =  { 0.4f, 0.4f, 0.4f, 1.0f };

    glEnable(GL_DEPTH_TEST); // Wlaczenie metdy bufora glebokosci
    glEnable(GL_CULL_FACE); // Wlaczenie metody eliminowania niewidocznych scianek
    glEnable(GL_LIGHTING); // Wlaczenie metody obliczania oswietlenia
    //glEnable(GL_NORMALIZE); // Ustawienie automatycznego obliczania normalnych do powierzchni
    //glEnable(GL_AUTO_NORMAL);

    glEnable(GL_RESCALE_NORMAL);

    //glDisable(GL_NORMALIZE);
    //glDisable(GL_AUTO_NORMAL);

    // Przygotowanie i wlaczenie swiatla GL_LIGHT0
    //  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambientLight);

    glLightfv(GL_LIGHT0,GL_AMBIENT,ambientLight);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuseLight);
    glLightfv(GL_LIGHT0,GL_SPECULAR,specular);
    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);
    glEnable(GL_LIGHT0);

    // Ustawienie wlaciwosci odbijania swiatla dla materialu
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specref);
    glMateriali(GL_FRONT,GL_SHININESS,64);

    QColor background = Qt::lightGray;
    if(parentWidget() != NULL)
        background = parentWidget()->palette().background().color();
    backgroundcolor = background.rgb();
    ((unsigned char*)(&backgroundcolor))[0] /= 8;
    ((unsigned char*)(&backgroundcolor))[1] /= 8;
    ((unsigned char*)(&backgroundcolor))[2] /= 8;
    ((unsigned char*)(&backgroundcolor))[3] /= 8;

//    glClearColor((float)background.red()/255.0,
//                 (float)background.green()/255.0,
//                 (float)background.blue()/255.0,
//                 0.0f );

    glClearColor((float)((backgroundcolor>>16) & 0xff)/255.0,
                 (float)((backgroundcolor>>8) & 0xff)/255.0,
                 (float)((backgroundcolor) & 0xff)/255.0,
                 0.0f );

    //glClearColor(0,0,0,1);

    glDisable(GL_BLEND);

    volumelist = glGenLists(2);
    treeslist = volumelist+1;
//    renderslice[0]

    //createSphear(3);
    //createCube(3, 2, 5);
    //createTube(3, 2, 5);

}


/*
void ImageGl::paintGL()
{
    if(size().height()<=0 || size().width()<=0) return;
    GLdouble rangeh, rangev;
    rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().height()/(double)size().width());
    rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().width()/(double)size().height());
    //GLdouble mato_x_mat[16];
    glViewport(0, 0, size().width(), size().height());
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glColorMask(1, 1, 1, 1);
    glColor4f((float)((backgroundcolor>>16) & 0xff)/255.0,
                 (float)((backgroundcolor>>8) & 0xff)/255.0,
                 (float)((backgroundcolor) & 0xff)/255.0,
                 1.0f );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    switch(stereo)
    {
    case 1:
        glFrustum(-rangev, rangev, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glLoadMatrixd(mat);
        paintSingleGL();
        break;
    case 2:
        glFrustum(-rangev, rangev, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glLoadMatrixd(mat);
        paintSingleGL();
        break;

    default:
        glFrustum(-rangev, rangev, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glLoadMatrixd(mat);
        paintSingleGL();
        break;

    }

    glFlush();
}
















void ImageGl::paintSingleGL(void)
{
//    if(rendertrees)
//    {
//        multiply4x4(mato_x_mat, mat, mato);
//        glLoadMatrixd(mato_x_mat);
//        glColor3f(1.0, 0.0, 0.0); //Kolor Roi
//        glCallList(treeslist);
//    }
    for(int k = 0; k < 3; k++)
    {
        if(renderslice[k])
        {
            createCrossSection(k+1, sliceindex[k]);
        }
    }

    if(rendertrees && glIsList(treeslist))
    {
        glCallList(treeslist);
    }

    if(rendervolume)
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        if(minwindow <= maxwindow)glBlendEquation(GL_FUNC_ADD);
        else glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
        glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
        int dirn = getFacingDirection();
        if(lastdirectionofvolume == dirn && glIsList(volumelist)) glCallList(volumelist);
        else
        {
            glNewList(volumelist, GL_COMPILE_AND_EXECUTE);
            createVolumetricView();
            glEndList();
        }
        lastdirectionofvolume = dirn;
        glDisable(GL_BLEND);
    }

    //createSphear(1);
}



*/


/*
void ImageGl::paintGL()
{
    if(size().height()<=0 || size().width()<=0) return;

    glColorMask(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, size().width(), size().height());

    Coordinates3Dto2D(NULL, NULL, true);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, size().width(), 0.0, size().height(), 0.0, 1.0);
    glMatrixMode (GL_MODELVIEW);
    createTreeTest();
    glFlush();
}
*/

//static GLdouble projectionMat[16];
//static GLint viewportMat[4];

void ImageGl::paintGLSetMatrices(GLdouble modelview[16], GLdouble projection[16], GLint viewport[4])
{
    GLdouble rangeh, rangev;
    rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().height()/(double)size().width());
    rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().width()/(double)size().height());
    glViewport(0, 0, size().width(), size().height());

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-rangev, rangev, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixd(modelViewMat);
    if(modelview != NULL) glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    if(projection != NULL) glGetDoublev(GL_PROJECTION_MATRIX, projection);
    if(viewport != NULL) glGetIntegerv(GL_VIEWPORT, viewport);
}

void ImageGl::paintGL()
{
    if(size().height()<=0 || size().width()<=0) return;

    glColorMask(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    glViewport(0, 0, size().width(), size().height());
    GLdouble rangeh, rangev;
//    rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().height()/(double)size().width());
//    rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().width()/(double)size().height());
    glColor4f((float)((backgroundcolor>>16) & 0xff)/255.0,
                 (float)((backgroundcolor>>8) & 0xff)/255.0,
                 (float)((backgroundcolor) & 0xff)/255.0,
                 1.0f );

    GLdouble stshift = -40 *(double)stereodepth/200.0;
    GLdouble stdepth = -1.5625 *(double)stereodepth/200.0;
    GLdouble mat12 = modelViewMat[12];

    switch(stereo)
    {
    case 1:
        glViewport(0, 0, size().width(), size().height());
        rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().height()/(double)size().width());
        rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().width()/(double)size().height());
        glColorMask(1, 0, 0, 1);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-rangev-stdepth, rangev-stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12-stshift;
        glLoadMatrixd(modelViewMat);
        paintSingleGL();

        glColorMask(0, 0, 1, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-rangev+stdepth, rangev+stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12+stshift;
        glLoadMatrixd(modelViewMat);

        paintSingleGL();
        modelViewMat[12]=mat12;
        glColorMask(1, 1, 1, 1);
        break;

    case 2:
        glViewport(0, 0, size().width(), size().height());
        rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().height()/(double)size().width());
        rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)size().width()/(double)size().height());
        paintGLSetMatrices(false);
        glFrustum(-rangev-stdepth, rangev-stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12-stshift;
        glLoadMatrixd(modelViewMat);
        glDrawBuffer(GL_BACK_LEFT);
        paintSingleGL();

        glClear(GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-rangev+stdepth, rangev+stdepth, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
        glMatrixMode(GL_MODELVIEW);
        modelViewMat[12]=mat12+stshift;
        glLoadMatrixd(modelViewMat);
        glDrawBuffer(GL_BACK_RIGHT);
        paintSingleGL();
        modelViewMat[12]=mat12;
        glDrawBuffer(GL_BACK);
        break;

    default:
        paintGLSetMatrices();
//        glMatrixMode(GL_PROJECTION);
//        glLoadIdentity();
//        glFrustum(-rangev, rangev, -rangeh, rangeh, FRUSTUM_FRONT_DISTANCE, FRUSTUM_BACK_DISTANCE);
//        glMatrixMode(GL_MODELVIEW);
//        glLoadMatrixd(modelViewMat);
        paintSingleGL();
        break;
    }
    glFlush();
}



void ImageGl::paintSingleGL()
{
    bool something = false;

    for(int k = 0; k < 3; k++)
    {
        if(renderslice[k])
        {
            if(createCrossSection(k+1, sliceindex[k])) something = true;
        }
    }

    if(rendertrees && glIsList(treeslist))
    {
        glCallList(treeslist);
        something = true;
    }

    if(rendervolume)
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        if(minwindow <= maxwindow)glBlendEquation(GL_FUNC_ADD);
        else glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
        glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
        int dirn = getFacingDirection();
        if(lastdirectionofvolume == dirn && glIsList(volumelist))
        {
            glCallList(volumelist);
            something = true;
        }
        else
        {
            glNewList(volumelist, GL_COMPILE_AND_EXECUTE);
            if(createVolumetricView()) something = true;
            glEndList();
        }
        lastdirectionofvolume = dirn;
        glDisable(GL_BLEND);

    }

    if(!something)
    {
        glBegin(GL_LINES);
        glNormal3f(0, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0.2, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0.2, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, 0.3);
        glEnd();
        createSphear(0.1, 10);
    }


}










void ImageGl::setVolumeView(bool view)
{
    lastdirectionofvolume = 0;
    rendervolume = view;
    glNewList(volumelist, GL_COMPILE);
    glEndList();
    update();
}

void ImageGl::setCrossView(int direction, bool view)
{
    switch(direction)
    {
    case 1: renderslice[0] = view; break;
    case 2: renderslice[1] = view; break;
    case 3: renderslice[2] = view; break;
    }
    update();
}

void ImageGl::setTreesView(std::vector<Tree*>& trees, std::vector<unsigned int>& colors, int viewballs)
{
    glNewList(treeslist, GL_COMPILE);
    for(int t = 0; t < trees.size(); t++)
    {
        unsigned int color = 0xcccc66ff;
        if(t < colors.size()) color = colors[t];
        createTree(trees[t], color, viewballs);
    }
    glEndList();
    if(trees.size() > 0) rendertrees = true;
    else rendertrees = false;
    update();
}


void ImageGl::setBackgroundColor(unsigned int background)
{
    backgroundcolor = background;
    glClearColor((float)((background>>16) & 0xff)/255.0,
                 (float)((background>>8) & 0xff)/255.0,
                 (float)((background) & 0xff)/255.0,
                 1.0f );
    lastdirectionofvolume = 0;
    update();
}


Byte3DImage* ImageGl::getImage(unsigned int channel)
{
    if(channel >= 4) return NULL;
    return image[channel];
}

void ImageGl::setImage(unsigned int channel, Byte3DImage* rimage, bool upd)
{
    image[channel] = rimage;
    lastdirectionofvolume = 0;
    //initView();
    if(upd) update();
}


void ImageGl::setStereo(int s, int d)
{
    stereo = s;
    stereodepth = d;
    update();
}


void ImageGl::setZoom(int z)
{
    zoomlevel = z;
    if(zoomlevel > 256) zoomlevel = 256;
    if(zoomlevel < -256) zoomlevel = -256;
    modelViewMat[14] = -(double)FRUSTUM_FRONT_DISTANCE - (((double)FRUSTUM_BACK_DISTANCE-FRUSTUM_FRONT_DISTANCE) * (zoomlevel+256)*(zoomlevel+256))/(513.0*513.0);

//    uninverted = true;
    update();
    emit zoomChanged(zoomlevel);
}


void ImageGl::setCrossSection(int direction, unsigned int index)
{
    if(abs(direction) > 3 || abs(direction) < 1) return;
    sliceindex[abs(direction-1)] = index;
    update();
}
void ImageGl::setCrossSection(int direction, bool show)
{
    if(abs(direction) > 3 || abs(direction) < 1) return;

    if(show) renderslice[abs(direction-1)] = true;
    else renderslice[abs(direction-1)] = false;
    update();
}


//void ImageGl::setImageChannel(char ichannel)
//{
////    channel = ichannel;
//}
//void ImageGl::setImageBrightnessLow(double iminbrightness)
//{
//    minbrightness = iminbrightness;
//    lastdirection = 0;
//    initView();
//    update();
//}
//void ImageGl::setImageBrightnessHigh(double imaxbrightness)
//{
//    maxbrightness = imaxbrightness;
//    lastdirection = 0;
//    initView();
//    update();
//}
void ImageGl::setImageGrayWindowLow(double iminwindow)
{
    minwindow = iminwindow;
    lastdirectionofvolume = 0;
    //initView();
    update();
}
void ImageGl::setImageGrayWindowHigh(double imaxwindow)
{
    maxwindow = imaxwindow;
    lastdirectionofvolume = 0;
    //initView();
    update();
}
//void ImageGl::setImageVisibility(ImageVisibilityMode ivisibility)
//{
////    visibility = ivisibility;
//}
//void ImageGl::setImageVisibilityAll(char ichannel, double iminwindow, double imaxwindow, ImageVisibilityMode ivisibility)
//{
//   // channel = ichannel;

//    minwindow = iminwindow;
//    maxwindow = imaxwindow;
//    //visibility = ivisibility;
//}

//void ImageGl::getCrossSection(int direction, unsigned int* index, bool* show)
//{}
//void ImageGl::getBackgroundColor(unsigned int* background)
//{}
//void ImageGl::getZoom(int* z)
//{}
//void ImageGl::getImageVisibilityAll(char* ochannel, double* ominwindow, double* omaxwindow, ImageVisibilityMode* ovisibility)
//{
//    if(ochannel != NULL) *ochannel = 0;
//    if(ominwindow != NULL) *ominwindow = minwindow;
//    if(omaxwindow != NULL) *omaxwindow = maxwindow;
//    if(ovisibility != NULL) *ovisibility = 0;
//}





















void ImageGl::createCube(float bottomRadius, float topRadius, float halfHeight)
{
    float f = (bottomRadius - topRadius)/2.0f;
    float g = sqrt(halfHeight*halfHeight + f*f);
    float nz = f/g;
    float nn = halfHeight/g;
    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glFrontFace(GL_CCW);
    glBegin(GL_QUADS);
        glNormal3f(nn, 0, 0);
        glVertex3f(bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(topRadius, topRadius, halfHeight);
        glVertex3f(topRadius, -topRadius, halfHeight);
        glVertex3f(bottomRadius, -bottomRadius, -halfHeight);
        glNormal3f(0, 0, -1.0f);
        glVertex3f(-bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(bottomRadius, -bottomRadius, -halfHeight);
        glVertex3f(-bottomRadius, -bottomRadius, -halfHeight);
        glNormal3f(0, nn, nz);
        glVertex3f(-topRadius, topRadius, halfHeight);
        glVertex3f(topRadius, topRadius, halfHeight);
        glVertex3f(bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(-bottomRadius, bottomRadius, -halfHeight);
        glNormal3f(-nn, 0, nz);
        glVertex3f(-topRadius, topRadius, halfHeight);
        glVertex3f(-bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(-bottomRadius, -bottomRadius, -halfHeight);
        glVertex3f(-topRadius, -topRadius, halfHeight);
        glNormal3f(0, 0, 1.0f);
        glVertex3f(topRadius, topRadius, halfHeight);
        glVertex3f(-topRadius, topRadius, halfHeight);
        glVertex3f(-topRadius, -topRadius, halfHeight);
        glVertex3f(topRadius, -topRadius, halfHeight);
        glNormal3f(0, -nn, nz);
        glVertex3f(topRadius, -topRadius, halfHeight);
        glVertex3f(-topRadius, -topRadius, halfHeight);
        glVertex3f(-bottomRadius, -bottomRadius, -halfHeight);
        glVertex3f(bottomRadius, -bottomRadius, -halfHeight);
    glEnd();
}



void ImageGl::createTube(float bottomRadius, float topRadius, float halfHeight)
{
    const int NNN = 256;
    float f = (bottomRadius - topRadius)/2.0f;
    float g = sqrt(halfHeight*halfHeight + f*f);
    float nz = f/g;
    float nn = halfHeight/g;

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);

    glFrontFace(GL_CCW);
    glBegin(GL_TRIANGLES);
    for(int k = 0; k < NNN; k++)
    {
        int kp = (k+1)%NNN;
        float kkk = k*2*M_PI/NNN;
        float kkp = kp*2*M_PI/NNN;
        float cc = cos(kkk);
        float cp = cos(kkp);
        float ss = sin(kkk);
        float sp = sin(kkp);

        glNormal3f(0, 0, 1.0f);
        glVertex3f(topRadius*sp, topRadius*cp, halfHeight);
        glVertex3f(topRadius*ss, topRadius*cc, halfHeight);
        glVertex3f(0.0f, 0.0f, halfHeight);
        glNormal3f(0, 0, -1.0f);
        glVertex3f(bottomRadius*ss, bottomRadius*cc, -halfHeight);
        glVertex3f(bottomRadius*sp, bottomRadius*cp, -halfHeight);
        glVertex3f(0.0f, 0.0f, -halfHeight);
    }
    glEnd();

    glBegin(GL_QUADS);
    for(int k = 0; k < NNN; k++)
    {
        int kp = (k+1)%NNN;
        float kkk = k*2*M_PI/NNN;
        float kkp = kp*2*M_PI/NNN;
        float cc = cos(kkk);
        float cp = cos(kkp);
        float ss = sin(kkk);
        float sp = sin(kkp);

        glNormal3f(sp*nn, cp*nn, nz);
        glVertex3f(topRadius*sp, topRadius*cp, halfHeight);
        glVertex3f(bottomRadius*sp, bottomRadius*cp, -halfHeight);
        glNormal3f(ss*nn, cc*nn, nz);
        glVertex3f(bottomRadius*ss, bottomRadius*cc, -halfHeight);
        glVertex3f(topRadius*ss, topRadius*cc, halfHeight);
    }
    glEnd();
}


void ImageGl::createSphear(float radius, int divisions)
{
    Sphear sphear(divisions);
    unsigned int tn = sphear.trianglesNumber();
    unsigned int* tr = new unsigned int[tn*3];
    sphear.triangles(tr);

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);

    glFrontFace(GL_CCW);
    glBegin(GL_TRIANGLES);
    double vertex[3];

    for(unsigned int k = 0; k < tn; k++)
    {
        for(unsigned int v = 2; v < 3; v--)
        {
            sphear.vertex(tr[k*3+v], vertex);
            glNormal3f(vertex[0], vertex[1], vertex[2]);
            glVertex3f(radius*vertex[0], radius*vertex[1], radius*vertex[2]);
        }
    }
    glEnd();
    delete[] tr;
}

void ImageGl::setFalseColor(float level)
{
    float r, g, b;
    if(level <= 0.0) {r = 0.0; g = 0.0; b = 0.0;}
    else if(level < 1.0/6.0) {r = 0.0; g = 0.0; b = 6.0*level;}
    else if(level < 2.0/6.0) {r = 0.0; g = 6.0*(level-1.0/6.0); b = 1.0;}
    else if(level < 3.0/6.0) {r = 6.0*(level-2.0/6.0); g = 1.0; b = 1.0-r;}
    else if(level < 4.0/6.0) {r = 1.0; g = 1.0 - 6.0*(level-3.0/6.0); b = 0.0;}
    else if(level < 5.0/6.0) {r = 1.0; g = 0.0; b = 6.0*(level-4.0/6.0);}
    else if(level < 1.0) {r = 1.0; g = 6.0*(level-5.0/6.0); b = 1.0;}
    else {r = 1.0; g = 1.0; b = 1.0;}
    glColor4f(r, g, b, 1.0f);
}










/*
void ImageGl::createTreeTest(void)
{
    glColor4f(0.5, 0.5, 0.5, 1.0f );

    if(treeee == NULL) return;
    Byte3DImage* img = NULL;
    for(int d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    Sphear sphear(2);
    unsigned int tn = sphear.trianglesNumber();
    unsigned int* tr = new unsigned int[tn*3];
    sphear.triangles(tr);
    GLfloat sx, sy, sz;

    if(img == NULL)
    {
        sx = 0;
        sy = 0;
        sz = 0;
    }
    else
    {
        sx = (GLfloat)img->size[0]*img->voxelsize[0]/2.0f;
        sy = (GLfloat)img->size[1]*img->voxelsize[1]/2.0f;
        sz = (GLfloat)img->size[2]*img->voxelsize[2]/2.0f;
    }


    int minx = 1000000;
    int maxx = -1000000;
    int miny = 1000000;
    int maxy = -1000000;



    glBegin(GL_LINES);
    glNormal3f(0, 0, 0);
    int branchcount = treeee->count();
    for(int b = 0; b < branchcount; b++)
    {
        int nodecount = treeee->count(b);
        for(int k = 1; k < nodecount; k++)
        {
            NodeIn3D nodeb = treeee->node(b, k);
            NodeIn3D nodee = treeee->node(b, k-1);



            double input[3];
            double output[3];
            input[0] = nodeb.x-sx;
            input[1] = nodeb.y-sy;
            input[2] = nodeb.z-sz;
            Coordinates3Dto2D(input, output, false);
            glVertex2f(output[0], output[1]);


            input[0] = nodee.x-sx;
            input[1] = nodee.y-sy;
            input[2] = nodee.z-sz;
            Coordinates3Dto2D(input, output, false);
            glVertex2f(output[0], output[1]);

            if(minx > output[0]) minx = output[0];
            if(miny > output[1]) miny = output[1];
            if(maxx < output[0]) maxx = output[0];
            if(maxy < output[1]) maxy = output[1];



        }
    }
    glEnd();


    delete[] tr;
}
*/








/**
 * @brief ImageGl::createTree renders tree view
 * @param tree pointer to tree data
 * @param color color of the tree
 * @param viewballs mode of rendering
 */
void ImageGl::createTree(Tree* tree, unsigned int color, int viewballs)
{
    glColor4f((float)((color>>16) & 0xff)/255.0,
                 (float)((color>>8) & 0xff)/255.0,
                 (float)((color) & 0xff)/255.0,
                 1.0f );

    Byte3DImage* img = NULL;
    for(int d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    Sphear sphear(2);
    unsigned int tn = sphear.trianglesNumber();
    unsigned int* tr = new unsigned int[tn*3];
    sphear.triangles(tr);
    GLfloat sx, sy, sz;

    if(img == NULL)
    {
        sx = 0;
        sy = 0;
        sz = 0;
    }
    else
    {
        unsigned int* size = img->size;
        sx = img->voxelsize[0]*(GLfloat)size[0]/2.0f-0.5;
        sy = img->voxelsize[1]*(GLfloat)size[1]/2.0f-0.5;
        sz = img->voxelsize[2]*(GLfloat)size[2]/2.0f-0.5;
    }

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);

    if(viewballs & 2)
    {
        glBegin(GL_LINES);
        glNormal3f(0, 0, 0);

        double maxr = 0;
        unsigned int nnodes = tree->nodeCount();
        for(unsigned int n = 0; n < nnodes; n++)
        {
            if(maxr < tree->node(n).radius) maxr = tree->node(n).radius;
        }

        int branchcount = tree->count();
        for(int b = 0; b < branchcount; b++)
        {
            int nodecount = tree->count(b);
            for(int k = 1; k < nodecount; k++)
            {
                NodeIn3D nodeb = tree->node(b, k);
                NodeIn3D nodee = tree->node(b, k-1);
                setFalseColor(nodeb.radius/maxr);
                glVertex3f(nodeb.x-sx, nodeb.y-sy, nodeb.z-sz);
                setFalseColor(nodee.radius/maxr);
                glVertex3f(nodee.x-sx, nodee.y-sy, nodee.z-sz);
            }
        }
        glEnd();

        glColor4f((float)((color>>16) & 0xff)/255.0,
                     (float)((color>>8) & 0xff)/255.0,
                     (float)((color) & 0xff)/255.0,
                     1.0f );
    }
    else
    {
        glBegin(GL_LINES);
        glNormal3f(0, 0, 0);
        int branchcount = tree->count();
        for(int b = 0; b < branchcount; b++)
        {
            int nodecount = tree->count(b);
            for(int k = 1; k < nodecount; k++)
            {
                NodeIn3D nodeb = tree->node(b, k);
                NodeIn3D nodee = tree->node(b, k-1);
                glVertex3f(nodeb.x-sx, nodeb.y-sy, nodeb.z-sz);
                glVertex3f(nodee.x-sx, nodee.y-sy, nodee.z-sz);
            }
        }
        glEnd();
    }

    if(viewballs & 1)
    {
        glBegin(GL_TRIANGLES);
        glFrontFace(GL_CCW);
        double vertex[3];
        int nodecount = tree->nodeCount();
        for(int i = 0; i < nodecount; i++)
        {
            NodeIn3D node = tree->node(i);
            for(unsigned int k = 0; k < tn; k++)
            {
                double diam = node.radius+0.1;
                for(unsigned int v = 2; v < 3; v--)
                {
                    sphear.vertex(tr[k*3+v], vertex);
                    glNormal3f(vertex[0], vertex[1], vertex[2]);
                    glVertex3f(diam*vertex[0]+node.x-sx, diam*vertex[1]+node.y-sy, diam*vertex[2]+node.z-sz);
                }
            }
        }
        glEnd();
    }
    delete[] tr;
}



int ImageGl::getFacingDirection(void)
{
    int direction = 0;
    if(image != NULL)
    {
        GLdouble x = fabs(modelViewMat[2]);
        GLdouble y = fabs(modelViewMat[6]);
        GLdouble z = fabs(modelViewMat[10]);
        if(x>z && x>y)
        {
            if(modelViewMat[2] > 0) direction = 1;
            else direction = -1;
        }
        else if(y>z && y>x)
        {
            if(modelViewMat[6] > 0) direction = 2;
            else direction = -2;
        }
        else
        {
            if(modelViewMat[10] > 0) direction = 3;
            else direction = -3;
        }
    }
//      printf("Direction = %i\n", direction);
//      fflush(stdout);
    return direction;
}



void ImageGl::getMatrix(double m[16])
{
    for(int i = 0; i < 16; i++) m[i] = modelViewMat[i];
}
void ImageGl::setMatrix(double m[16])
{
    for(int i = 0; i < 16; i++) modelViewMat[i] = m[i];
//    uninverted = true;
    update();
}


bool ImageGl::fillCrossSection(GLubyte* bits, unsigned int stride, unsigned int direction, unsigned int sliceindex, bool noalpha)
{
    unsigned int stepx;
    unsigned int stepy;
    unsigned int sizex;
    unsigned int sizey;
    unsigned int x, y;
    uint8* start;
    unsigned int d;
    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    switch(direction)
    {
    case 1: //x
        if(sliceindex >= img->size[0]) return false;
        sizex = img->size[1];
        sizey = img->size[2];
        stepx = img->size[0];
        stepy = stepx*img->size[1];
        start = (uint8*)img->data + sliceindex;
        break;
    case 2: //y
        if(sliceindex >= img->size[1]) return false;
        sizex = img->size[0];
        sizey = img->size[2];
        stepx = 1;
        stepy = img->size[0]*img->size[1];
        start = (uint8*)img->data + img->size[0]*sliceindex;
        break;
    default: //z
        if(sliceindex >= img->size[2]) return false;
        sizex = img->size[0];
        sizey = img->size[1];
        stepx = 1;
        stepy = img->size[0];
        start = (uint8*)img->data + img->size[0]*img->size[1]*sliceindex;
        break;
    }

    int minb = minwindow;
    int maxb = maxwindow;
    uint8* src;
    GLubyte* dst;

    for(d = 0; d < 3; d++)
    {
        if(image[d] == NULL)
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                for(x = 0; x < sizex; x++)
                {
                    *dst = 0; dst+=4;
                }
            }
            continue;
        }

        switch(direction)
        {
        case 1: //x
            start = (uint8*)image[d]->data + sliceindex;
            break;
        case 2: //y
            start = (uint8*)image[d]->data + image[d]->size[0]*sliceindex;
            break;
        default: //z
            start = (uint8*)image[d]->data + image[d]->size[0]*image[d]->size[1]*sliceindex;
            break;
        }


        if(minb <= maxb)
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
                    if(*src <= minb) *dst = 0;
                    else if(*src > maxb) *dst = 255;
                    else *dst = 255*(*src-minb)/(maxb-minb);
                    dst+=4;
                    src+=stepx;
                }
            }
        }
        else
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
//                    uint8 out = 255 - *src;
//                    *dst = out;
                    if(*src >= minb) *dst = 0;
                    else if(*src < maxb) *dst = 255;
                    else *dst = 255*(minb-*src)/(minb-maxb);
                    dst+=4;
                    src+=stepx;
                }
            }
        }
    }

//Alfa
    if(image[d] == NULL || noalpha)
    {
        for(y = 0; y < sizey; y++)
        {
            dst = bits + y*stride + d;
            for(x = 0; x < sizex; x++)
            {
                *dst = 255; dst+=4;
            }
        }
    }
    else
    {
        switch(direction)
        {
        case 1: //x
            start = (uint8*)image[3]->data + sliceindex;
            break;
        case 2: //y
            start = (uint8*)image[3]->data + image[3]->size[0]*sliceindex;
            break;
        default: //z
            start = (uint8*)image[3]->data + image[3]->size[0]*image[3]->size[1]*sliceindex;
            break;
        }

        if(minb <= maxb)
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
                    if(*src <= minb) *dst = 0;
                    else if(*src > maxb) *dst = 255;
                    else
                    {
                        *dst = 255*(*src-minb)/(maxb-minb);
                    }
                    dst+=4;
                    src+=stepx;
                }
            }
        }
        else
        {
            for(y = 0; y < sizey; y++)
            {
                dst = bits + y*stride + d;
                src = start + y*stepy;
                for(x = 0; x < sizex; x++)
                {
                    if(*src >= minb) *dst = 0;
                    else if(*src < maxb) *dst = 255;
                    else
                    {
                        *dst = 255*(minb-*src)/(minb-maxb);
                    }
                    dst+=4;
                    src+=stepx;
                }
            }
        }
    }

    return true;
}


/**
 * @brief ImageGl::createCrossSection
 * @param direction
 * @param sliceindex
 */
bool ImageGl::createCrossSection(unsigned int direction, unsigned int sliceindex)
{
    unsigned int i, d;
    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    if(img == NULL) return false;
    unsigned int* size = img->size;
    unsigned int sizeimg[2];
    //GLuint listindex;
    switch(direction)
    {
    case 1: //x
        //listindex = firstlist+1;
        sizeimg[0] = size[1];
        sizeimg[1] = size[2];
        break;
    case 2: //y
        //listindex = firstlist+2;
        sizeimg[0] = size[0];
        sizeimg[1] = size[2];
        break;
    default: //z
        //listindex = firstlist+3;
        sizeimg[0] = size[0];
        sizeimg[1] = size[1];
        break;
    }


    unsigned int sizetex[2];

    for(d = 0; d < 2; d++)
    {
        sizetex[d] = 1;
        for(i = 0; i < sizeof(unsigned int)*8; i++)
        {
            if(sizetex[d] >= sizeimg[d]) break;
            sizetex[d] <<= 1;
        }
    }

    unsigned int stride = sizetex[0]*4;
    GLubyte* bits = new GLubyte[sizetex[0]*sizetex[1]*4];

    //glNewList(listindex, GL_COMPILE);
    glDisable(GL_CULL_FACE);
    fillCrossSection(bits, stride, direction, sliceindex, true);
    //fillCrossSectionVolume(bits, stride, image, direction, sliceindex);


///////////////////////
//    GLuint texname; //handle to a texture
//    glGenTextures(1, &texname); //Gen a new texture and store the handle in texname
//    glBindTexture(GL_TEXTURE_2D, texname);




    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// Force 4-byte alignment
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glDisable(GL_TEXTURE_1D);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glFrontFace(GL_CCW);
    glNormal3f(0.0, 0.0, 0.0);
    renderCrossSection(direction, sliceindex, bits, sizeimg, sizetex, stride);


/////////////////////////
//    glDeleteTextures(1, &texname);


    //glEndList();
    delete[] bits;
    return true;
}







/**
 * @brief ImageGl::renderCrossSection
 * @param direction
 * @param sliceindex
 * @param bits
 * @param sizeimg
 * @param sizetex
 * @param stride
 */
void ImageGl::renderCrossSection(unsigned int direction, unsigned int sliceindex, GLubyte* bits, unsigned int sizeimg[2], unsigned int sizetex[2], unsigned int stride)
{
    unsigned int d;
    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    glTexImage2D(GL_TEXTURE_2D, 0, 4, sizetex[0], sizetex[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, bits);

    glBegin(GL_QUADS);
    unsigned int* size = img->size;
    GLfloat sx, sy, sz;
    switch(direction)
    {
    case 1: //x
        sx = img->voxelsize[0]*((GLfloat)sliceindex - (GLfloat)size[0]/2.0f + 0.5f);
        sy = img->voxelsize[1]*(GLfloat)size[1]/2.0f;
        sz = img->voxelsize[2]*(GLfloat)size[2]/2.0f;
        glTexCoord2f(0.0, 0.0);
        glVertex3f(sx, -sy, -sz);
        glTexCoord2f((GLfloat)sizeimg[0]/sizetex[0], 0.0);
        glVertex3f(sx, sy, -sz);
        glTexCoord2f((GLfloat)sizeimg[0]/sizetex[0], (GLfloat)sizeimg[1]/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(0.0, (GLfloat)sizeimg[1]/sizetex[1]);
        glVertex3f(sx, -sy, sz);
        break;
    case 2: //y
        sx = img->voxelsize[0]*(GLfloat)size[0]/2.0f;
        sy = img->voxelsize[1]*((GLfloat)sliceindex - (GLfloat)size[1]/2.0f + 0.5f);
        sz = img->voxelsize[2]*(GLfloat)size[2]/2.0f;
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-sx, sy, -sz);
        glTexCoord2f((GLfloat)sizeimg[0]/sizetex[0], 0.0);
        glVertex3f(sx, sy, -sz);
        glTexCoord2f((GLfloat)sizeimg[0]/sizetex[0], (GLfloat)sizeimg[1]/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(0.0, (GLfloat)sizeimg[1]/sizetex[1]);
        glVertex3f(-sx, sy, sz);
        break;
    default: //z
        sx = img->voxelsize[0]*(GLfloat)size[0]/2.0f;
        sy = img->voxelsize[1]*(GLfloat)size[1]/2.0f;
        sz = img->voxelsize[2]*((GLfloat)sliceindex - (GLfloat)size[2]/2.0f + 0.5f);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-sx, -sy, sz);
        glTexCoord2f((GLfloat)sizeimg[0]/sizetex[0], 0.0);
        glVertex3f(sx, -sy, sz);
        glTexCoord2f((GLfloat)sizeimg[0]/sizetex[0], (GLfloat)sizeimg[1]/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(0.0, (GLfloat)sizeimg[1]/sizetex[1]);
        glVertex3f(-sx, sy, sz);
        break;
    }
    glEnd();
}

/**
 * @brief ImageGl::volumetricView creates transparent view of cross-sections similar to MIP
 */
bool ImageGl::createVolumetricView(void)
{
    unsigned int d;

    Byte3DImage* img = NULL;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }

    if(img == NULL) return false;
    unsigned int* size = img->size;
    unsigned int sizeimg[3];
    int direction = getFacingDirection();

    switch(abs(direction))
    {
    case 1: //x
        sizeimg[0] = size[1];
        sizeimg[1] = size[2];
        sizeimg[2] = size[0];
        break;
    case 2: //y
        sizeimg[0] = size[0];
        sizeimg[1] = size[2];
        sizeimg[2] = size[1];
        break;
    default: //z
        sizeimg[0] = size[0];
        sizeimg[1] = size[1];
        sizeimg[2] = size[2];
        break;
    }

    unsigned int i;
    unsigned int sizetex[2];

    for(d = 0; d < 2; d++)
    {
        sizetex[d] = 1;
        for(i = 0; i < sizeof(unsigned int)*8; i++)
        {
            if(sizetex[d] >= sizeimg[d]) break;
            sizetex[d] <<= 1;
        }
    }
    glDisable(GL_CULL_FACE);

    unsigned int stride = sizetex[0]*4;
    GLubyte* bits = new GLubyte[sizetex[0]*sizetex[1]*4];

    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// Force 4-byte alignment
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glDisable(GL_TEXTURE_1D);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glFrontFace(GL_CCW);
    glNormal3f(0.0, 0.0, 0.0);


    unsigned int sliceindex, sliceindexstart, sliceindexmax, sliceindexstep;
    if(direction > 0)
    {
        sliceindexstart = 0;
        sliceindexstep = 1;
    }
    else
    {
        sliceindexstart = sizeimg[2]-1;
        sliceindexstep = -1;
    }
    sliceindexmax = sizeimg[2];
    for(sliceindex = sliceindexstart; sliceindex < sliceindexmax; sliceindex+=sliceindexstep)
    {
        fillCrossSection(bits, stride, abs(direction), sliceindex);
        renderCrossSection(abs(direction), sliceindex, bits, sizeimg, sizetex, stride);
    }

    delete[] bits;
    return true;
}

/**
 * @brief ImageGl::Coordinates3Dto2D computes coordinates in render window based on 3D coordinates
 * @param input 3D coordinates
 * @param output 2D coordinates and distance
 * @param update_matrices true to update matrices
 * @return zero on success
 */

bool ImageGl::Coordinates3Dto2D(double input[3], double output[3], bool update_matrices)
{
    static GLdouble projectionMat[16];
    static GLint viewportMat[4];
    static GLdouble modelViewMat[16];
    static GLfloat sx, sy, sz;
    if(size().height()<=0 || size().width()<=0) return false;

    if(update_matrices)
    {
        paintGLSetMatrices(modelViewMat, projectionMat, viewportMat);

        Byte3DImage* img = NULL;
        for(int d = 0; d < 4; d++)
        {
            if(image[d] != NULL) img = image[d];
        }
        if(img == NULL)
        {
            sx = 0;
            sy = 0;
            sz = 0;
        }
        else
        {
            unsigned int* size = img->size;
            sx = img->voxelsize[0]*(GLfloat)size[0]/2.0f-0.5;
            sy = img->voxelsize[1]*(GLfloat)size[1]/2.0f-0.5;
            sz = img->voxelsize[2]*(GLfloat)size[2]/2.0f-0.5;
        }
    }
    else
    {
        GLdouble winX, winY, winZ;//2D point
        GLdouble posX, posY, posZ;//3D point
        posX = input[0] - sx;
        posY = input[1] - sy;
        posZ = input[2] - sz;
        if(gluProject(posX, posY, posZ, modelViewMat, projectionMat, viewportMat, &winX, &winY, &winZ) == GLU_FALSE) return false;
        output[0] = winX;
        output[1] = viewportMat[3] - winY - 1;
        output[2] = winZ;
    }

    /*
    GLdouble viewVector[3];
    viewVector[0]=modelview[8];
    viewVector[1]=modelview[9];
    viewVector[2]=modelview[10];
    if(viewVector[0]*posX+viewVector[1]*posY+viewVector[2]*posZ<0){OK;}
    */
    return true;
}

/**
 * @brief ImageGl::Coordinates2Dto3D finds voxel index pointed at render window
 * @param input coordinates in render window
 * @param output voxel index
 * @return true on success
 */
bool ImageGl::Coordinates2Dto3D(int input[2], int output[3])
{
    unsigned int imgindex;
    Byte3DImage* img = NULL;
    if(size().height()<=0 || size().width()<=0) return false;
    for(imgindex = 0; imgindex < 4; imgindex++) if(image[imgindex] != NULL) img = image[imgindex];
    if(img == NULL) return false;

    GLdouble projectionMat[16];
    GLint viewportMat[4];
    GLdouble modelViewMat[16];
    paintGLSetMatrices(modelViewMat, projectionMat, viewportMat);

    GLdouble inpd[3];
    GLdouble outd[3];
    GLdouble vecd[3];

    inpd[0] = input[0];
    inpd[1] = viewportMat[3] - input[1] - 1;
    inpd[2] = 0.0;
    if(gluUnProject(inpd[0], inpd[1], inpd[2], modelViewMat, projectionMat, viewportMat, &(outd[0]), &(outd[1]), &(outd[2])) == GLU_FALSE) return false;
    inpd[2] = 1.0;
    if(gluUnProject(inpd[0], inpd[1], inpd[2], modelViewMat, projectionMat, viewportMat, &(vecd[0]), &(vecd[1]), &(vecd[2])) == GLU_FALSE) return false;
    vecd[0] -= outd[0];
    vecd[1] -= outd[1];
    vecd[2] -= outd[2];

    double temp[3];
    int result[3][3];
    double dist[3];
    if(renderslice[2] && vecd[2] != 0.0)
    {
        //sz = img->voxelsize[2]*(GLfloat)size[2]/2.0f-0.5;
        temp[2] = ((double)sliceindex[2]-(double)img->size[2]/2.0)*img->voxelsize[2]+0.5;
        temp[0] = outd[0] + (temp[2]-outd[2])*vecd[0] / vecd[2];
        temp[1] = outd[1] + (temp[2]-outd[2])*vecd[1] / vecd[2];
        dist[2] = sqrt((outd[0]-temp[0])*(outd[0]-temp[0])+
                       (outd[1]-temp[1])*(outd[1]-temp[1])+
                       (outd[2]-temp[2])*(outd[2]-temp[2]));
        result[2][0] = floor(temp[0]/img->voxelsize[0] + (double)img->size[0]/2.0);
        result[2][1] = floor(temp[1]/img->voxelsize[1] + (double)img->size[1]/2.0);
        result[2][2] = sliceindex[2];
    }
    else dist[2] = -1;

    if(renderslice[1] && vecd[1] != 0.0)
    {
        temp[1] = ((double)sliceindex[1]-(double)img->size[1]/2.0)*img->voxelsize[1]+0.5;
        temp[0] = outd[0] + (temp[1]-outd[1])*vecd[0] / vecd[1];
        temp[2] = outd[2] + (temp[1]-outd[1])*vecd[2] / vecd[1];

        dist[1] = sqrt((outd[0]-temp[0])*(outd[0]-temp[0])+
                       (outd[1]-temp[1])*(outd[1]-temp[1])+
                       (outd[2]-temp[2])*(outd[2]-temp[2]));
        result[1][0] = floor(temp[0]/img->voxelsize[0] + (double)img->size[0]/2.0);
        result[1][1] = sliceindex[1];
        result[1][2] = floor(temp[2]/img->voxelsize[2] + (double)img->size[2]/2.0);
    }
    else dist[1] = -1;

    if(renderslice[0] && vecd[0] != 0.0)
    {
        temp[0] = ((double)sliceindex[0]-(double)img->size[0]/2.0)*img->voxelsize[0]+0.5;
        temp[1] = outd[1] + (temp[0]-outd[0])*vecd[1] / vecd[0];
        temp[2] = outd[2] + (temp[0]-outd[0])*vecd[2] / vecd[0];
        dist[0] = sqrt((outd[0]-temp[0])*(outd[0]-temp[0])+
                       (outd[1]-temp[1])*(outd[1]-temp[1])+
                       (outd[2]-temp[2])*(outd[2]-temp[2]));
        result[0][0] = sliceindex[0];
        result[0][1] = floor(temp[1]/img->voxelsize[1] + (double)img->size[1]/2.0);
        result[0][2] = floor(temp[2]/img->voxelsize[2] + (double)img->size[2]/2.0);
    }
    else dist[0] = -1;

    int sorter[3];
    int z;
    sorter[0] = 0;
    sorter[1] = 1;
    sorter[2] = 2;
    if(dist[sorter[0]]>dist[sorter[1]])
    {
        z = sorter[1];
        sorter[1] = sorter[0];
        sorter[0] = z;
    }
    if(dist[sorter[0]]>dist[sorter[2]])
    {
        z = sorter[2];
        sorter[2] = sorter[0];
        sorter[0] = z;
    }
    if(dist[sorter[1]]>dist[sorter[2]])
    {
        z = sorter[2];
        sorter[2] = sorter[1];
        sorter[1] = z;
    }
    for(int ii = 0; ii < 3; ii++)
    {
        int i = sorter[ii];
        if(dist[i] >= 0.0)
        {
           if((unsigned int)result[i][0] < img->size[0] && (unsigned int)result[i][1] < img->size[1] && (unsigned int)result[i][2] < img->size[2])
           {
               output[0] = result[i][0];
               output[1] = result[i][1];
               output[2] = result[i][2];
               return true;
           }
        }
    }
    return false;
}





/*
bool ImageGl::Coordinates2Dto3D(int input[2], int output[3])
{
    unsigned int d;
    double x, y, z;
    Byte3DImage* img = NULL;
    if(size().height()<=0 || size().width()<=0)
        return false;
    for(d = 0; d < 4; d++)
    {
        if(image[d] != NULL) img = image[d];
    }
    if(img == NULL)
        return false;
    double xp = input[0] - size().width()/2;
    double yp = size().height()/2 - input[1];
    if(uninverted)invert3(mat_1, modelViewMat);
    uninverted = false;

    // Stala 15.0 jest magiczna
    double ddd = FRUSTUM_SIDE_TO_SIDE/(sqrt(double(size().width()*size().height()))*15.0);
    xp *= ddd;
    yp *= ddd;

    //Obliczanie punktow przeciecia prostej widzenia z plaszczyzanami
    double odleglosc_przeciecia[3];
    odleglosc_przeciecia[0] = -1;
    odleglosc_przeciecia[1] = -1;
    odleglosc_przeciecia[2] = -1;


    if(renderslice[2])
    {
        ddd = mat_1[10]-mat_1[6]*yp-mat_1[2]*xp;
        if(ddd != 0)
            odleglosc_przeciecia[0] = (((int)img->size[2]/2-(int)sliceindex[2])*img->voxelsize[2]-modelViewMat[14]*mat_1[10])/ddd;
    }
    if(renderslice[1])
    {
        ddd = mat_1[9]-mat_1[5]*yp-mat_1[1]*xp;
        if(ddd != 0)
            odleglosc_przeciecia[1] = (((int)img->size[1]/2-(int)sliceindex[1])*img->voxelsize[1]-modelViewMat[14]*mat_1[9])/ddd;
    }
    if(renderslice[0])
    {
        ddd = mat_1[8]-mat_1[4]*yp-mat_1[0]*xp;
        if(ddd != 0)
            odleglosc_przeciecia[2] = (((int)img->size[0]/2-(int)sliceindex[0])*img->voxelsize[0]-modelViewMat[14]*mat_1[8])/ddd;
    }
    //Sortowanie punktow przeciecia od najblizszego
    if(odleglosc_przeciecia[0]>odleglosc_przeciecia[1])
    {
        z = odleglosc_przeciecia[1];
        odleglosc_przeciecia[1] = odleglosc_przeciecia[0];
        odleglosc_przeciecia[0] = z;
    }
    if(odleglosc_przeciecia[0]>odleglosc_przeciecia[2])
    {
        z = odleglosc_przeciecia[2];
        odleglosc_przeciecia[2] = odleglosc_przeciecia[0];
        odleglosc_przeciecia[0] = z;
    }
    if(odleglosc_przeciecia[1]>odleglosc_przeciecia[2])
    {
        z = odleglosc_przeciecia[2];
        odleglosc_przeciecia[2] = odleglosc_przeciecia[1];
        odleglosc_przeciecia[1] = z;
    }

    for(int n = 0; n < 3; n++)
    {
        //if(odleglosc_przeciecia[n] > FRUSTUM_FRONT_DISTANCE)
        if(odleglosc_przeciecia[n] > 0)
        {
            x = xp*odleglosc_przeciecia[n];
            y = yp*odleglosc_przeciecia[n];
            z = odleglosc_przeciecia[n]+modelViewMat[14];

            //Obliczanie wspolrzednych woksela w kostce
            output[0] = lround((mat_1[0]*x+mat_1[4]*y-mat_1[8]*z)/img->voxelsize[0]+img->size[0]/2);
            output[1] = lround((mat_1[1]*x+mat_1[5]*y-mat_1[9]*z)/img->voxelsize[1]+img->size[1]/2);
            output[2] = lround((mat_1[2]*x+mat_1[6]*y-mat_1[10]*z)/img->voxelsize[2]+img->size[2]/2);
            if(output[0]>=0 && output[1]>=0 && output[2]>=0 && output[0]<(int)img->size[0] && output[1]<(int)img->size[1] && output[2]<(int)img->size[2])
            {
                return true;
            }

        }
    }
    return false;
}
*/




void ImageGl::mousePressEvent(QMouseEvent *ev)
{
    mousex = ev->x();
    mousey = ev->y();
    int input[2];
    input[0] = mousex;
    input[1] = mousey;
    emit pointedAt(input);
    ev->accept();
}

void ImageGl::mouseMoveEvent(QMouseEvent *ev)
{
    if(freeze)
    {
        mousex = ev->x();
        mousey = ev->y();
        int input[2];
        input[0] = mousex;
        input[1] = mousey;
        emit pointedAt(input);
        ev->accept();
        return;
    }

    if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
    {
        shift(ev->x()-mousex, mousey-ev->y());
    }
    else
    {
        int width = size().width();
        int height = size().height();
        //int dref = height < width ? height : width;
        double dref = sqrt((double)height*height + (double)width*width);

        if(dref > 1)
        {
            double xcen = 2*ev->x() - width;
            double ycen = 2*ev->y() - height;

            double xsht = ev->x()-mousex;
            double ysht = ev->y()-mousey;

            double xn = xcen/dref;
            double yn = ycen/dref;
            double xnyn = sqrt(xn*xn + yn*yn);

            if(xnyn > 1.0)
            {
                xn /= xnyn;
                yn /= xnyn;
                xnyn = 1.0;
            }

            double rot = xn * ysht - yn * xsht;
            xsht += (yn*rot);
            ysht -= (xn*rot);

            rotatem(rot, 0, 1, modelViewMat);
            rotatem(xsht, 0, 2, modelViewMat);
            rotatem(-ysht, 1, 2, modelViewMat);
        }
    }
    mousex = ev->x();
    mousey = ev->y();
//    uninverted = true;
    update();
    ev->accept();
}

void ImageGl::wheelEvent(QWheelEvent *ev)
{
    setZoom(ev->delta() > 0 ? zoomlevel+2 : zoomlevel-2);
    emit zoomChanged(zoomlevel);
}
void ImageGl::mouseReleaseEvent(QMouseEvent *ev)
{
    ev->ignore();
}
void ImageGl::mouseDoubleClickEvent(QMouseEvent *ev)
{
    ev->ignore();
}
