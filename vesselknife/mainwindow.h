#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QStyledItemDelegate>
#include <QListWidgetItem>
#include <QProcess>
#include <QTimer>
#include "imagegl.h"
#include "../shared/buildtree.h"
#include "../shared/vtkimport.h"
#include "../shared/byteimage.h"

#include <nifti/nifti1.h>
#include <nifti/nifti1_io.h>

const char RELEASE_VERSION[] = "Release 15.08";

namespace Ui {
class MainWindow;
}

class NoEditDelegate: public QStyledItemDelegate
{
public:
    NoEditDelegate(QObject* parent=0): QStyledItemDelegate(parent) {}
    virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        return 0;
    }
};


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);

public slots:
    void processTimer(void);
//    void frameChanged(int);

private slots:
    void rendererSizeChanged(int);
    void pointedAt(int* coords);
    void runNotImplemented();

    void invert();
    void gaussian();
    void vesselness();
    void multiscaleVesselness();
    void floodFill();
    void threshold();
    void skeletonFromBinary();
    void skeletonToTree();
    void whiteForegroundHoleFill();
    void blackForegroundHoleFill();
    void whiteTopHat();
    void blackTopHat();
    void dilate();
    void erode();
    void smoothTreeBranches();
    void smoothDiameter();
    void svdBasedDiameters();
    void extendBranches();
    void crop();
    void levelSetChanVese();
    void disjoinBranches();
    void disjoinDisconnected();
    void joinChecked();
    void longBranchesFormat();
    void shortBranchesFormat();
    void zoomSliderValueChanged(int value);



    void on_actionLoad_triggered();
    //void on_actionColor_triggered();
    void on_spinBoxMin_valueChanged(int val);
    void on_spinBoxMax_valueChanged(int val);
    void on_spinBox_X_valueChanged(int arg1);
    void on_spinBox_Y_valueChanged(int arg1);
    void on_spinBox_Z_valueChanged(int arg1);
    void on_checkBox_X_clicked(bool checked);
    void on_checkBox_Y_clicked(bool checked);
    void on_checkBox_Z_clicked(bool checked);
    void on_imageLevelsButton_clicked();
    void on_setAsRChannel_clicked();
    void on_setAsGChannel_clicked();
    void on_setAsBChannel_clicked();
    void on_setAsAChannel_clicked();
    void on_buttonRemoveSeed_clicked();
    void on_removeBuffer_clicked();


    void on_imageOffButton_clicked(bool checked);


    void on_actionSave_triggered();

    void on_actionSaveTree_triggered();

    void on_actionLoadTree_triggered();

    //void on_toggleView_clicked();

    void on_listWidgetTrees_itemClicked(QListWidgetItem *item);

    void on_listWidgetTrees_itemChanged(QListWidgetItem *item);


    void on_getMatrixButton_clicked();

    void on_setMatrixButton_clicked();


    void on_defaultMatrixButton_clicked();

    void on_removeTree_clicked();

    void on_toolButton_clicked();

    void on_toolButtonBalls_clicked();

    void on_actionExit_triggered();

    void on_stereoCombo_currentIndexChanged(int index);

    void on_stereoSlider_valueChanged(int value);

    void on_toolButtonWires_clicked();

    void on_toolButtonStream_clicked();

    void on_buttonImageInfo_clicked();

    void on_buttonColorPicker_clicked();

    void on_buttonIndicateNode_clicked();

    void on_buttonAddSeed_clicked();

    void on_actionTo_STL_triggered();

    void on_actionTo_VTK_triggered();

    void on_buttonSetVoxel_clicked(bool checked);

    void on_actionAbout_triggered();

    void on_actionContents_triggered();

    //void on_actionReload_raster_triggered();

    void on_actionBackground_color_triggered();

    void on_listWidgetBuffers_currentRowChanged(int currentRow);

    void on_frameSpinBox_valueChanged(int arg1);

    void on_playButton_clicked();

private:
    QTimer timer;

    unsigned char greylevel;
    QString tempTreeFile;
    bool loadImage(QString fileName);

    void setColorToItemIcon(QListWidgetItem *item, QColor color);
    void setTemporatyFileNames(void);
    void deleteTemporatyFileNames(void);

    std::vector<Byte3DImage*> images;
    std::vector<Tree> trees;
    std::vector<QRgb> treecolors;
    Ui::MainWindow *ui;
    ImageGl* renderer;

    bool loadNifti(const char* filename, Byte3DImage* image, unsigned int frame = 0);
    bool saveNifti(const char* filename, Byte3DImage* image);

    Byte3DImage* getCurrentImage(void);
    Tree *getCurrentTree(void);

    void setCurrentBufferAsNthChannel(int i);
    void appendToImages(Byte3DImage* img, QString name);
    void appendToTrees(Tree tree, QString name, QRgb color = 0xff0000);

    bool initTools(void);
    QString getParameter(QString name);
    void getCurrentSeed(int coords[3]);
    void updateImageIcons(void);

    QSlider zoomSlider;


    //void setRunButton(QTreeWidgetItem* item, const char* szpara, QString name);

};

#endif // MAINWINDOW_H
