#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QColorDialog>


/*===========================================================================*/


void MainWindow::setColorToItemIcon(QListWidgetItem *item, QColor color)
{
    int width = ui->listWidgetTrees->style()->pixelMetric(QStyle::PM_IndicatorWidth);
    int height = ui->listWidgetTrees->style()->pixelMetric(QStyle::PM_IndicatorHeight);
    QPixmap pixmap(width, height);
    QIcon icon;
    QPainter pixPaint(&pixmap);
    QBrush brush(color);
    pixPaint.setBrush(brush);
    QPen pend(color.darker(150));
    pixPaint.setPen(pend);
    pixPaint.drawRect(0.0, 0.0, width - 1, height - 1);
    pend.setColor(color.lighter());
    pixPaint.setPen(pend);
    pixPaint.drawLine(0.0, 0.0, width - 1, 0.0);
    pixPaint.drawLine(0.0, 0.0, 0.0, height - 1);
    icon.addPixmap(pixmap);
    item->setIcon(icon);
}

/*===========================================================================*/
void MainWindow::on_listWidgetTrees_itemClicked(QListWidgetItem *item)
{
    if (item == NULL) return;

    QRect rect;
    int width = ui->listWidgetTrees->style()->pixelMetric(QStyle::PM_IndicatorWidth);
    int height = ui->listWidgetTrees->style()->pixelMetric(QStyle::PM_IndicatorHeight);
    QStyleOptionViewItemV4 opt;

    opt.rect = ui->listWidgetTrees->visualItemRect(item);
    opt.decorationSize = QSize(width, height);
    opt.features = opt.HasCheckIndicator | opt.HasDisplay | opt.HasDecoration;
    rect = ui->listWidgetTrees->style()->subElementRect(QStyle::SE_ItemViewItemDecoration, &opt);

    QPoint mouse = ui->listWidgetTrees->mapFromGlobal(QCursor::pos());
    if(mouse.x() >= rect.left() && mouse.x() <= rect.right())
    {
        int row = ui->listWidgetTrees->row(item);
        if(row < 0 || row >= treecolors.size()) return;
        QColor color = QColorDialog::getColor(treecolors[row]);

        if(color.isValid())
        {
            color.setHsv(color.hsvHue(), color.hsvSaturation(), 255);
            setColorToItemIcon(item, color);
            treecolors[row] = color.rgb();
            on_listWidgetTrees_itemChanged(NULL);
        }
    }
}

void MainWindow::on_listWidgetTrees_itemChanged(QListWidgetItem *item)
{
    std::vector<Tree*> trees2;
    std::vector<unsigned int> colors2;
    int rsize = trees.size() < treecolors.size() ? trees.size() : treecolors.size();
    if(ui->listWidgetTrees->count() < rsize) rsize = ui->listWidgetTrees->count();

    for(int r = 0; r < rsize; r++)
    {
        if(ui->listWidgetTrees->item(r)->checkState() == Qt::Checked)
        {
            trees2.push_back(&trees[r]);
            colors2.push_back(treecolors[r]);
        }
    }

    int viewball = 0;
    if(ui->toolButtonBalls->isChecked()) viewball = 1;
    if(ui->toolButtonStream->isChecked()) viewball = 2;

    renderer->setTreesView(trees2, colors2, viewball);
}

