#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "nodeproperties.h"

#include <stdio.h>
#include <stdlib.h>
#include <QFileDialog>
#include <QMessageBox>
#include <QColorDialog>
#include <QDropEvent>
#include <QMimeData>
#include <QDesktopServices>
#include <QUrl>
#include <string>
#include <sstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    renderer = new ImageGl(this);

    setCentralWidget(renderer);

    initTools();

    connect(renderer, SIGNAL(zoomChanged(int)), this, SLOT(rendererSizeChanged(int)));
    connect(renderer, SIGNAL(pointedAt(int*)), this, SLOT(pointedAt(int*)));

    zoomSlider.setOrientation(Qt::Horizontal);
    zoomSlider.setMinimumWidth(200);
    zoomSlider.setMinimum(-256);
    zoomSlider.setMaximum(256);
    zoomSlider.setValue(0);
    //zoomSlider.setMinimumHeight(14);
    zoomSlider.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
    ui->statusBar->addPermanentWidget(&zoomSlider);
    connect(&zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(zoomSliderValueChanged(int)));

    renderer->setZoom(zoomSlider.value());
    greylevel = 0;

    for(int f = 1; f < QApplication::arguments().size(); f++)
    {
        QString filename = QApplication::arguments()[f];

        if(!loadImage(filename))
        {
            Tree tree;
            if(tree.load(filename.toStdString().c_str()))
            {
                appendToTrees(tree, QFileInfo(filename).fileName());
            }
        }
    }
    setTemporatyFileNames();

    ui->menuView->addSeparator();
    ui->menuView->addAction(ui->dockRasterData->toggleViewAction());
    ui->menuView->addAction(ui->dockWidgetRun->toggleViewAction());
    ui->menuView->addAction(ui->dockRendering->toggleViewAction());
    ui->menuView->addAction(ui->dockVectorData->toggleViewAction());
    ui->menuView->addSeparator();
    ui->menuView->addAction(ui->toolBar->toggleViewAction());

    connect(&timer, SIGNAL(timeout()), this, SLOT(processTimer()));

}

MainWindow::~MainWindow()
{
    deleteTemporatyFileNames();
    delete ui;
}



void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        if(urlList.size() >= 1)
        {
            QString filename = urlList.at(0).toLocalFile();
            if(loadImage(filename)) event->acceptProposedAction();
            else
            {
                Tree tree;
                if(tree.load(filename.toStdString().c_str()))
                {
                    appendToTrees(tree, QFileInfo(filename).fileName());
                    event->acceptProposedAction();
                }
            }
        }
    }
}




bool MainWindow::loadImage(QString fileName)
{
    Byte3DImage* img = new Byte3DImage();
    //char* info = NULL;
    if(loadNifti(fileName.toStdString().c_str(), img))
    {

        //For tests
//                    img->voxelsize[0] = 3.5;
//                    img->voxelsize[1] = 3.5;
//                    img->voxelsize[2] = 3.5;

        if(images.size() > 0)
        {
            bool sizeok = true;
            for(unsigned int i = 0; i < BI_DIMENSIONS; i++)
            {
                if(img->size[i] != images[0]->size[i]) sizeok = false;
            }
            if(!sizeok)
            {
                delete img;
                QMessageBox msgBox;
                msgBox.setText("Incorrect size");
                msgBox.setWindowTitle("Image info");
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
                return true;
            }

            if(images.size() > 0)
            {
                if( img->voxelsize[0] != images[0]->voxelsize[0] ||
                    img->voxelsize[1] != images[0]->voxelsize[1] ||
                    img->voxelsize[2] != images[0]->voxelsize[2])
                {
                    QMessageBox msgBox;
                    msgBox.setText("Voxel size mismatch. Fix it?");
                    msgBox.setWindowTitle("Image info");
                    msgBox.setIcon(QMessageBox::Warning);

                    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Ignore | QMessageBox::Abort);
                    int ret = msgBox.exec();
                    switch(ret)
                    {
                    case QMessageBox::Yes:
                        img->voxelsize[0] = images[0]->voxelsize[0];
                        img->voxelsize[1] = images[0]->voxelsize[1];
                        img->voxelsize[2] = images[0]->voxelsize[2];
                        break;
                    case QMessageBox::Abort:
                        delete img;
                        return true;
                    }
                }
            }
        }

        ui->horizontalSlider_X->setMaximum(img->size[0]-1);
        ui->spinBox_X->setMaximum(img->size[0]-1);
        ui->spinBox_X->setValue(img->size[0]/2);
        ui->horizontalSlider_Y->setMaximum(img->size[1]-1);
        ui->spinBox_Y->setMaximum(img->size[1]-1);
        ui->spinBox_Y->setValue(img->size[1]/2);
        ui->horizontalSlider_Z->setMaximum(img->size[2]-1);
        ui->spinBox_Z->setMaximum(img->size[2]-1);
        ui->spinBox_Z->setValue(img->size[2]/2);
//            ui->checkBox_X->setChecked(true);
//            ui->checkBox_Y->setChecked(true);
//            ui->checkBox_Z->setChecked(true);

        appendToImages(img, QFileInfo(fileName).fileName());

        renderer->setImage(0, img, false);
        renderer->setImage(1, img, false);
        renderer->setImage(2, img, false);
        renderer->setImage(3, img, false);

        renderer->update();
        updateImageIcons();

        on_listWidgetTrees_itemChanged(NULL);
        return true;
    }
    else
    {
        delete img;
        return false;
    }
}




void MainWindow::on_actionLoad_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load image"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Nifti (*.nii *.nifti *.hdr *.nii.gz);;All files (*)"));
    if (!fileName.isEmpty())
    {
        loadImage(fileName);
    }
}




void MainWindow::on_actionSave_triggered()
{
    if(images.size() <= 0) return;
    int row = ui->listWidgetBuffers->currentRow();
    if(row < 0 || row >= images.size()) return;
    QListWidgetItem* item  = ui->listWidgetBuffers->currentItem();
    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save image"),
                                                    item->text(),
                                                    tr("Nifti (*.nii *.nifti *.hdr *.nii.gz);;All files (*)"));
    if (!fileName.isEmpty())
    {
        saveNifti(fileName.toStdString().c_str(), images[row]);
    }
}


void MainWindow::setTemporatyFileNames(void)
{
    QFile f;
    tempTreeFile = QDir::tempPath();
    tempTreeFile += "/tmpTree.txt";
    f.setFileName(tempTreeFile);
    if(f.open(QFile::WriteOnly)) f.close();
}

void MainWindow::deleteTemporatyFileNames(void)
{
    QFile::remove(tempTreeFile);
}

void MainWindow::on_actionTo_STL_triggered()
{
    if(trees.size() <= 0) return;
    int row = ui->listWidgetTrees->currentRow();
    if(row < 0 || row >= trees.size()) return;
    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export tree"),
                                                    item->text(),
                                                    tr("Stereolithography (*.stl);;All files (*)"));
    if (!fileName.isEmpty())
    {
        trees[row].save(tempTreeFile.toStdString().c_str(), 0);
        QDir execDir(qApp->applicationDirPath());
        QString command = execDir.absolutePath() + QString("/tree2stl");
        command += " ";
        command += tempTreeFile;
        command += " ";
        command += fileName;
        QProcess process(this);
        if(!process.startDetached(command))
        {
            command = QString("tree2stl");
            command += " ";
            command += tempTreeFile;
            command += " ";
            command += fileName;
            QProcess process(this);
            if(!process.startDetached(command))
            {
                QMessageBox msgBox;
                msgBox.setText("Missing tree2stl program. Export failed.");
                msgBox.setWindowTitle("Export warning");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();
            }
        }
        ui->statusBar->showMessage(command);
    }
}

void MainWindow::on_actionTo_VTK_triggered()
{
    if(trees.size() <= 0) return;
    int row = ui->listWidgetTrees->currentRow();
    if(row < 0 || row >= trees.size()) return;
    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export tree"),
                                                    item->text(),
                                                    tr("Visualization toolkit file format (*.vtk);;All files (*)"));
    if (!fileName.isEmpty())
    {
        trees[row].save(tempTreeFile.toStdString().c_str(), 0);
        QDir execDir(qApp->applicationDirPath());
        QString command = execDir.absolutePath() + QString("/tree2vtk");
        command += " ";
        command += tempTreeFile;
        command += " ";
        command += fileName;
        QProcess process(this);
        if(!process.startDetached(command))
        {
            command = QString("tree2vtk");
            command += " ";
            command += tempTreeFile;
            command += " ";
            command += fileName;
            QProcess process(this);
            if(!process.startDetached(command))
            {
                QMessageBox msgBox;
                msgBox.setText("Missing tree2vtk program. Export failed.");
                msgBox.setWindowTitle("Export warning");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();

            }
        }
        ui->statusBar->showMessage(command);
    }
}

void MainWindow::appendToTrees(Tree tree, QString name, QRgb color)
{
    trees.push_back(tree);
    treecolors.push_back(color);
    QListWidgetItem *item = new QListWidgetItem(ui->listWidgetTrees);
    item->setText(name);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable);
    item->setCheckState(Qt::Checked);
    setColorToItemIcon(item, QColor(color));
    on_listWidgetTrees_itemChanged(NULL);
}


void MainWindow::appendToImages(Byte3DImage* img, QString name)
{
    images.push_back(img);
    QListWidgetItem *item = new QListWidgetItem(ui->listWidgetBuffers);
    item->setText(name);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
    updateImageIcons();
}


Byte3DImage* MainWindow::getCurrentImage(void)
{
    int ile = images.size();
    int row = ui->listWidgetBuffers->currentRow();
    if(row >= ile || ile <= 0 || row < 0) return NULL;
    return images[row];
}

Tree* MainWindow::getCurrentTree(void)
{
    int ile = trees.size();
    int row = ui->listWidgetTrees->currentRow();
    if(row >= ile || ile <= 0 || row < 0) return NULL;
    return &trees[row];
}








bool MainWindow::saveNifti(const char* filename, Byte3DImage* image)
{
    int d;
    nifti_image* nim = NULL;
    int dim[8];
    dim[0] = 3;
    for(d = 0; d < 3; d++) dim[d+1] = image->size[d];
    for(d = 4; d < 8; d++) dim[d] = 0;

    nim = nifti_make_new_nim(dim, DT_UINT8, 1);
    int linesnumber = 1;
    for(d = 1; d < BI_DIMENSIONS; d++) linesnumber *= image->size[d];

    nim->dx = image->voxelsize[0];
    nim->dy = image->voxelsize[1];
    nim->dz = image->voxelsize[2];
    nim->nifti_type = 1; //nifti single file
//    char* name = new char[strlen(filename)+1];
//    strcpy(name, filename);
    nifti_set_filenames(nim, filename, 0, 1);
    if(strlen(nim->fname) > strlen(filename)) strcpy(nim->fname, filename);
//    nim->fname = name;
    uint8 *dptr;
    uint8 *ptr;
    for(d = 0; d < linesnumber; d++)
    {
        dptr = ((uint8*)image->data) + (image->size[0] * d);
        ptr = ((uint8*)nim->data) + (image->size[0] * d);
        memcpy(ptr, dptr, image->size[0]);
    }
    nifti_image_write(nim);
    nifti_image_free(nim);

    return true;
}


bool MainWindow::loadNifti(const char* filename, Byte3DImage* image, unsigned int frame)
{

    unsigned int d;

    if(image != NULL)
    {

        nifti_image* nim = NULL;
        nim = nifti_image_read(filename, 1);
        if(nim == NULL) return false;


        //unsigned int selected = 0;
        unsigned int sizemore = 1;
        for(d = 3; d < nim->dim[0]; d++) sizemore *= nim->dim[d+1];

        //if(frames != NULL) *frames = sizemore;
        if(frame >= sizemore)
        {
            nifti_image_free(nim);
            return false;   //Failed
        }

/*
        if(sizemore > 1)
        {
            ImageSelect dialog(nim->dim, &selected, this);
            dialog.exec();
            selected--;
            if(selected >= sizemore)
            {
                nifti_image_free(nim);
                return false;   //Failed
            }
        }
*/

        unsigned int size[BI_DIMENSIONS];
        for(d = 0; d < nim->dim[0] && d < BI_DIMENSIONS; d++) size[d] = nim->dim[d+1];
        for(; d < BI_DIMENSIONS; d++) size[d] = 1;
        unsigned int sizeall = 1;
        for(d = 0; d < BI_DIMENSIONS; d++) sizeall *= size[d];
        unsigned int linesnumber = 1;
        for(d = 1; d < BI_DIMENSIONS; d++) linesnumber *= size[d];

        switch(nim->datatype)
        {
        case DT_UINT8:
        case DT_INT16:
        case DT_UINT16:
        case DT_FLOAT32:
        case DT_FLOAT64:
        case DT_COMPLEX64:
            image->allocate(size);
            break;

//        case DT_RGB24:
//            image->allocate(size, MZPIXEL_RGB24);
//            break;

        default:
            nifti_image_free(nim);
            return false;   //Failed
        }

        for(d = 0; d < nim->dim[0] && d < BI_DIMENSIONS; d++) image->voxelsize[d] = nim->pixdim[d+1];

        switch(nim->datatype)
        {
        case DT_UINT8:
            {
                uint8 *dptr;
                uint8 *ptr;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((uint8*)nim->data) + frame*sizeall + (size[0] * d);
                    memcpy(dptr, ptr, size[0]);
                }
            }
            //memcpy(image->data, nim->data, sizeall);
            break;

        case DT_INT16:
        {
            uint8 *dptr;
            int16 *ptr = (int16*)nim->data + frame*sizeall;
            double min = *ptr;
            double max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((int16*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    if(min > *ptr) min = *ptr;
                    if(max < *ptr) max = *ptr;
                    ptr++;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((int16*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = (*ptr-min)/max;
                        ptr++; dptr++;
                    }
                }
            }
        }
        break;
        case DT_UINT16:
        {
            uint8 *dptr;
            uint16 *ptr = (uint16*)nim->data + frame*sizeall;
            double min = *ptr;
            double max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((uint16*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    if(min > *ptr) min = *ptr;
                    if(max < *ptr) max = *ptr;
                    ptr++;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((uint16*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = (*ptr-min)/max;
                        ptr++; dptr++;
                    }
                }
            }
        }
        break;
        case DT_FLOAT32:
        {
            uint8 *dptr;
            float *ptr = (float*)nim->data + frame*sizeall;
            float min = *ptr;
            float max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    if(min > *ptr) min = *ptr;
                    if(max < *ptr) max = *ptr;
                    ptr++;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = (*ptr-min)/max;
                        ptr++; dptr++;
                    }
                }
            }
        }
        break;
        case DT_FLOAT64:
            {
                uint8 *dptr;
                double *ptr = (double*)nim->data + frame*sizeall;
                double min = *ptr;
                double max = *ptr;
                for(d = 0; d < linesnumber; d++)
                {
                    ptr = ((double*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        if(min > *ptr) min = *ptr;
                        if(max < *ptr) max = *ptr;
                        ptr++;
                    }
                }
                if(max > min)
                {
                    max -= min;
                    max /= 255.9;
                    for(d = 0; d < linesnumber; d++)
                    {
                        dptr = ((uint8*)image->data) + (image->size[0] * d);
                        ptr = ((double*)nim->data) + frame*sizeall + (size[0] * d);
                        for(unsigned int x = 0; x < size[0]; x++)
                        {
                            *dptr = (*ptr-min)/max;
                            ptr++; dptr++;
                        }
                    }
                }
            }
            break;

        case DT_COMPLEX64:
        {
            uint8 *dptr;
            float *ptr = (float*)nim->data + frame*sizeall;
            float min = *ptr;
            float max = *ptr;
            for(d = 0; d < linesnumber; d++)
            {
                ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                for(unsigned int x = 0; x < size[0]; x++)
                {
                    float re = *ptr; ptr++;
                    float im = *ptr; ptr++;
                    float mag = sqrt(re*re + im*im);
                    if(min > mag) min = mag;
                    if(max < mag) max = mag;
                }
            }
            if(max > min)
            {
                max -= min;
                max /= 255.9;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        float re = *ptr; ptr++;
                        float im = *ptr; ptr++;
                        float mag = sqrt(re*re + im*im);
                        *dptr = (mag-min)/max; dptr++;
                    }
                }
            }
        }
        break;
        case DT_RGB24:
            {
                uint8 *dptr;
                uint8 *ptr = (uint8*)nim->data + frame*sizeall;
                for(d = 0; d < linesnumber; d++)
                {
                    dptr = ((uint8*)image->data) + (image->size[0] * d);
                    ptr = ((uint8*)nim->data) + frame*sizeall + (size[0] * d);
                    for(unsigned int x = 0; x < size[0]; x++)
                    {
                        *dptr = *ptr; dptr++;
                        *dptr = *ptr; dptr++;
                        *dptr = *ptr; dptr++;
                    }
                }
            }
            break;

        default:
            nifti_image_free(nim);
            return false;   //Failed
        }

        std::stringstream ss;
        ss << "File: " << filename << std::endl;

        ss << "Format: ";
        switch(nim->nifti_type)
        {
        case 0: ss << "Analyze"; break;
        case 1: ss << "Nifti-1 (single file)"; break;
        case 2: ss << "Nifti-1 (two files)"; break;
        case 3: ss << "Nifti-ASCII"; break;
        default: ss << "?";
        }
        ss << std::endl;

        ss << "Raster: ";
        for(d = 1; (int)d <= nim->dim[0]; d++)
        {
            ss << nim->dim[d];
            if((int)d < nim->dim[0]) ss << "x";
        }
        ss << std::endl;
        if(sizemore > 1)
        {
            ss << "Frame: ";
            ss << frame << " of " << sizemore;
            ss << std::endl;
        }

        ss << "Original voxel type: ";
        switch(nim->datatype)
        {
        case DT_UINT8: ss << "uint8"; break;
        case DT_INT16: ss << "int16"; break;
        case DT_UINT16: ss << "uint16"; break;
        case DT_FLOAT32: ss << "float32"; break;
        case DT_FLOAT64: ss << "float64"; break;
        case DT_COMPLEX64: ss << "complex64"; break;
        case DT_RGB24:  ss << "rgb24"; break;
        default: ss << "?"; break;
        }
        ss << std::endl;
        ss << "Stored voxel type: uint8";
        ss << std::endl;

        ss << "Voxel size: ";
        for(d = 1; (int)d <= nim->dim[0]; d++)
        {
            ss << nim->pixdim[d];
            if((int)d < nim->dim[0]) ss << "x";
        }
        ss << std::endl;

        ss << "Space units: ";
        switch(nim->xyz_units)
        {
        case NIFTI_UNITS_METER: ss << "m"; break;
        case NIFTI_UNITS_MM: ss << "mm"; break;
        case NIFTI_UNITS_MICRON: ss << "um"; break;
        default: ss << "?";
        }
        ss << std::endl;
        ss << "Time units: ";
        switch(nim->time_units)
        {
        case NIFTI_UNITS_SEC: ss << "s"; break;
        case NIFTI_UNITS_MSEC: ss << "ms"; break;
        case NIFTI_UNITS_USEC: ss << "us"; break;
        default: ss << "?";
        }
        ss << std::endl;

        ss << "Slope: " << nim->scl_slope << std::endl;
        ss << "Intercept: " << nim->scl_inter << std::endl;
        ss << "Calibration minimum : " << nim->cal_min << std::endl;
        ss << "Calibration maximum: " << nim->cal_max << std::endl;

        if(image->info != NULL) delete[] image->info;
        image->info = new char[ss.str().length()+2];
        strcpy(image->info, ss.str().c_str());

        if(image->filePathAndName != NULL) delete[] image->filePathAndName;
        image->filePathAndName = new char[strlen(filename)+2];
        strcpy(image->filePathAndName, filename);
        image->framesInFile = sizemore;
        image->currentFrame = frame;

/*
//Frame arround for testing
        unsigned int x[3];
        x[0] = 0; x[1] = 0;
        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;
        x[0] = 0; x[1] = image->size[1]-1;
        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;
        x[0] = image->size[0]-1; x[1] = 0;
        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;
        x[0] = image->size[0]-1; x[1] = image->size[1]-1;
        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;

        x[2] = 0; x[0] = 0;
        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;
        x[2] = 0; x[0] = image->size[0]-1;
        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;
        x[2] = image->size[2]-1; x[0] = 0;
        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;
        x[2] = image->size[2]-1; x[0] = image->size[0]-1;
        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;

        x[1] = 0; x[2] = 0;
        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
        x[1] = 0; x[2] = image->size[2]-1;
        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
        x[1] = image->size[1]-1; x[2] = 0;
        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
        x[1] = image->size[1]-1; x[2] = image->size[2]-1;
        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
*/

        nifti_image_free(nim);
        return true;
    }

    return false;
}


//void MainWindow::on_actionColor_triggered()
//{
//    on_imageLevelsButton_clicked();
//}

void MainWindow::on_spinBoxMin_valueChanged(int val)
{
    renderer->setImageGrayWindowLow((double)val);
}

void MainWindow::on_spinBoxMax_valueChanged(int val)
{
    renderer->setImageGrayWindowHigh((double)val);
}

void MainWindow::on_spinBox_X_valueChanged(int arg1)
{
    renderer->setCrossSection(1, (unsigned int)arg1);
}

void MainWindow::on_spinBox_Y_valueChanged(int arg1)
{
    renderer->setCrossSection(2, (unsigned int)arg1);
}

void MainWindow::on_spinBox_Z_valueChanged(int arg1)
{
    renderer->setCrossSection(3, (unsigned int)arg1);
}

void MainWindow::on_checkBox_X_clicked(bool checked)
{
    renderer->setCrossSection(1, checked);
}

void MainWindow::on_checkBox_Y_clicked(bool checked)
{
    renderer->setCrossSection(2, checked);
}

void MainWindow::on_checkBox_Z_clicked(bool checked)
{
    renderer->setCrossSection(3, checked);
}


void MainWindow::rendererSizeChanged(int zoomlevel)
{
//    ui->horizontalSliderZoom->blockSignals(true);
//    ui->horizontalSliderZoom->setValue(zoomlevel);
//    ui->horizontalSliderZoom->blockSignals(false);
    zoomSlider.blockSignals(true);
    zoomSlider.setValue(-zoomlevel);
    zoomSlider.blockSignals(false);

}

void MainWindow::zoomSliderValueChanged(int value)
{
    renderer->setZoom(-value);
}

void MainWindow::updateImageIcons(void)
{
    int ile = images.size();
    int cnt = ui->listWidgetBuffers->count();
    if(cnt < ile) ile = cnt;
    for(int i = 0; i < ile; i++)
    {
        Byte3DImage* image = images[i];
        char patt[8];
        patt[4] = 0;
        for(int c = 0; c < 4; c++)
        {
            if(image == renderer->getImage(c)) patt[c] = '1';
            else patt[c] = '0';
        }
        char text[64];
        sprintf(text, ":/icons/icons/rgba_icon_%s.png", patt);
        ui->listWidgetBuffers->item(i)->setIcon(QIcon(text));
    }
}



void MainWindow::setCurrentBufferAsNthChannel(int i)
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    renderer->setImage(i, mi);
    updateImageIcons();
}

void MainWindow::on_setAsRChannel_clicked()
{
    setCurrentBufferAsNthChannel(0);
}

void MainWindow::on_setAsGChannel_clicked()
{
    setCurrentBufferAsNthChannel(1);
}

void MainWindow::on_setAsBChannel_clicked()
{
    setCurrentBufferAsNthChannel(2);
}

void MainWindow::on_setAsAChannel_clicked()
{
    setCurrentBufferAsNthChannel(3);
}

void MainWindow::on_buttonColorPicker_clicked()
{
    ui->buttonAddSeed->setChecked(false);
    ui->buttonIndicateNode->setChecked(false);
    ui->buttonSetVoxel->setChecked(false);
    renderer->setFreeze(false);
}

void MainWindow::on_buttonSetVoxel_clicked(bool checked)
{
    ui->buttonColorPicker->setChecked(false);
    ui->buttonAddSeed->setChecked(false);
    ui->buttonIndicateNode->setChecked(false);
    renderer->setFreeze(checked);
}


void MainWindow::on_buttonIndicateNode_clicked()
{
    ui->buttonColorPicker->setChecked(false);
    ui->buttonAddSeed->setChecked(false);
    ui->buttonSetVoxel->setChecked(false);
    renderer->setFreeze(false);
}

void MainWindow::on_buttonAddSeed_clicked()
{
    ui->buttonColorPicker->setChecked(false);
    ui->buttonIndicateNode->setChecked(false);
    ui->buttonSetVoxel->setChecked(false);
    renderer->setFreeze(false);
}

void MainWindow::pointedAt(int* coords)
{
    if(ui->buttonAddSeed->isChecked())
    {
        if(renderer != NULL)
        {
            int output[3];
            if(renderer->Coordinates2Dto3D(coords, output))
            {
                QListWidgetItem *item = new QListWidgetItem(ui->listWidgetSeed);
                item->setText(QString("%1 %2 %3").arg(output[0]).arg(output[1]).arg(output[2]));
                item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
            }
        }
        ui->buttonAddSeed->setChecked(false);
        return;
    }

    if(ui->buttonColorPicker->isChecked())
    {
        if(renderer != NULL)
        {
            int output[3];
            if(renderer->Coordinates2Dto3D(coords, output))
            {
                Byte3DImage* mi = getCurrentImage();
                if(mi != NULL)
                {
                    if((renderer->getImage(0) == mi || renderer->getImage(1) == mi || renderer->getImage(2) == mi) &&
                       (unsigned int)output[0] < mi->size[0] && (unsigned int)output[0] < mi->size[0] && (unsigned int)output[0] < mi->size[0])
                    {
                        unsigned char* pip = mi->pixel((unsigned int *)output);
                        greylevel = *pip;
                        ui->statusBar->showMessage(QString("(%1 %2 %3) %4").arg(output[0]).arg(output[1]).arg(output[2]).arg((int)greylevel));
                        return;
                    }
                }
                ui->statusBar->showMessage("Select image from the list");
            }
        }
        ui->buttonColorPicker->setChecked(false);
    }

    if(ui->buttonSetVoxel->isChecked())
    {
        if(renderer != NULL)
        {
            int output[3];
            if(renderer->Coordinates2Dto3D(coords, output))
            {
                Byte3DImage* mi = getCurrentImage();
                if(mi != NULL)
                {
                    if((renderer->getImage(0) == mi || renderer->getImage(1) == mi || renderer->getImage(2) == mi) &&
                       (unsigned int)output[0] < mi->size[0] && (unsigned int)output[0] < mi->size[0] && (unsigned int)output[0] < mi->size[0])
                    {
                        unsigned char* pip = mi->pixel((unsigned int *)output);
                        *pip = greylevel;
                        ui->statusBar->showMessage(QString("(%1 %2 %3)").arg(output[0]).arg(output[1]).arg(output[2]));
                        renderer->update();
                        return;
                    }
                }
                ui->statusBar->showMessage("Tree must be checked on the list");
            }
        }
        return;
    }

    if(ui->buttonIndicateNode->isChecked())
    {
        if(renderer != NULL)
        {
            unsigned int ile = trees.size() < ui->listWidgetTrees->count() ? trees.size() : ui->listWidgetTrees->count();
            //Tree* besttree  = NULL;
            unsigned int bestnode  = -1;
            unsigned int bestrow  = -1;
            double bestdistance = 100;

            if(! renderer->Coordinates3Dto2D(NULL, NULL, true)) return;
            for(unsigned int row = 0; row < ile; row++)
            {
                if(ui->listWidgetTrees->item(row)->checkState() == Qt::Checked)
                {
                    Tree* tree = &trees[row];
                    unsigned int nodecount = tree->nodeCount();
                    for(unsigned int nodei = 0; nodei < nodecount; nodei++)
                    {
                        double input[3];
                        double output[3];
                        NodeIn3D node = tree->node(nodei);
                        input[0] = node.x;
                        input[1] = node.y;
                        input[2] = node.z;
                        if(renderer->Coordinates3Dto2D(input, output, false))
                        {
                            double xx = coords[0]-output[0];
                            double yy = coords[1]-output[1];
                            double dd = xx*xx+yy*yy;
                            if(dd < bestdistance)
                            {
                                //besttree = tree;
                                bestrow = row;
                                bestnode = nodei;
                                bestdistance = dd;
                            }
                        }
                    }
                }
            }
            if(bestnode != (unsigned int)-1)
            {
                Tree* tree = &trees[bestrow];
                NodeIn3D node = tree->node(bestnode);

                ui->statusBar->showMessage(QString("(%1 %2 %3) ~%4)").arg(node.x).arg(node.y).arg(node.z).arg(sqrt(bestdistance)));
//                ui->statusBar->showMessage(QString("(%1 %2) d=%3)").arg(ui->listWidgetTrees->item(bestrow)->text()).arg(bestnode).arg(sqrt(bestdistance)));
//                Tree* tree = &trees[bestrow];
//                NodeIn3D node = tree->node(bestnode);
//                node.radius = 1.5;
//                tree->setNode(node, bestnode);

                Tree extracted;
                NodeProperties dialog(&trees[bestrow], &extracted, bestnode, this);
                dialog.exec();
                if(extracted.count() > 0) appendToTrees(extracted, QString("manualyExtracted"), 0xff00);
                else on_listWidgetTrees_itemChanged(NULL);
            }
        }
        ui->buttonIndicateNode->setChecked(false);
    }

}

void MainWindow::on_buttonRemoveSeed_clicked()
{
    QListWidgetItem* item  = ui->listWidgetSeed->currentItem();
    if(item != NULL) delete item;
}

void MainWindow::on_removeBuffer_clicked()
{
    QListWidgetItem* item  = ui->listWidgetBuffers->currentItem();
    int row  = ui->listWidgetBuffers->currentRow();
    if(item == NULL || row < 0) return;
    delete images[row];
    images.erase(images.begin()+row);
    delete item;

    row  = ui->listWidgetBuffers->currentRow();
    if(row < 0)
    {
        renderer->setImage(0, NULL);
        renderer->setImage(1, NULL);
        renderer->setImage(2, NULL);
        renderer->setImage(3, NULL);
    }
    else
    {
        Byte3DImage* img = images[row];
        renderer->setImage(0, img);
        renderer->setImage(1, img);
        renderer->setImage(2, img);
        renderer->setImage(3, img);
    }
    updateImageIcons();
}

void MainWindow::on_imageOffButton_clicked(bool checked)
{
    renderer->setVolumeView(checked);
}

void MainWindow::on_actionLoadTree_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load tree"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Tree (*.txt);;All files (*)"));
    if (!fileName.isEmpty())
    {
        Tree tree;
        if(tree.load(fileName.toStdString().c_str()))
        {
            appendToTrees(tree, QFileInfo(fileName).fileName());
        }
    }
}

//void MainWindow::on_actionImport_tree_triggered()
//{
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                                    tr("Import tree"),
//                                                    NULL,//domyslna nazwa pliku
//                                                    tr("Vtk polydata (*.vtk);;All files (*)"));
//    if (!fileName.isEmpty())
//    {
//        Tree tree = VtkImport::getTree(fileName.toStdString().c_str());
//        if(tree.count() > 0)
//        {
//            appendToTrees(tree, QFileInfo(fileName).fileName());
//        }
//    }
//}

void MainWindow::on_actionSaveTree_triggered()
{
    if(trees.size() <= 0) return;
    int row = ui->listWidgetTrees->currentRow();
    if(row < 0 || row >= trees.size()) return;
    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save tree"),
                                                    item->text(),
                                                    tr("Tree (*.txt);;All files (*)"));
    if (!fileName.isEmpty())
    {
        trees[row].save(fileName.toStdString().c_str(), 0);
    }
}


void MainWindow::on_getMatrixButton_clicked()
{
    QString m;
    double mat[16];
    renderer->getMatrix(mat);
    m = QString("%1 %2 %3 %4\n%5 %6 %7 %8\n%9 %10 %11 %12\n%13 %14 %15 %16").arg(mat[0]).arg(mat[1]).arg(mat[2]).arg(mat[3]).arg(mat[4]).arg(mat[5]).arg(mat[6]).arg(mat[7]).arg(mat[8]).arg(mat[9]).arg(mat[10]).arg(mat[11]).arg(mat[12]).arg(mat[13]).arg(mat[14]).arg(mat[15]);
    ui->matrixTextEdit->setPlainText(m);
}

void MainWindow::on_setMatrixButton_clicked()
{
    QStringList list = ui->matrixTextEdit->toPlainText().split(QRegExp("\\s+"), QString::SkipEmptyParts);
    if(list.count() != 16) return;
    double mat[16];
    for(int i = 0; i < 16; i++)
    {
        mat[i] = list[i].toDouble();
    }
    renderer->setMatrix(mat);
}

void MainWindow::on_defaultMatrixButton_clicked()
{
    QString m = QString("1 0 0 0\n0 1 0 0\n0 0 1 0\n0 0 -1000 1");
    ui->matrixTextEdit->setPlainText(m);
}

void MainWindow::on_removeTree_clicked()
{
    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
    int row  = ui->listWidgetTrees->currentRow();
    if(item == NULL || row < 0) return;
    trees.erase(trees.begin()+row);
    if(row < treecolors.size()) treecolors.erase(treecolors.begin()+row);
    delete item;
    on_listWidgetTrees_itemChanged(NULL);
}

void MainWindow::on_toolButton_clicked()
{
    Tree* tree = getCurrentTree();
    if(tree == NULL) return;
    unsigned int nnodes = tree->nodeCount();
    unsigned int njoints = 0;
    unsigned int ntips = 0;
    unsigned int nnjoints = 0;

    double minr = 0;
    double maxr = 0;

    for(unsigned int n = 0; n < nnodes; n++)
    {
        if(n == 0)
        {
            minr = maxr = tree->node(n).radius;
        }
        else
        {
            if(minr > tree->node(n).radius) minr = tree->node(n).radius;
            if(maxr < tree->node(n).radius) maxr = tree->node(n).radius;
        }
        unsigned int cc = tree->node(n).connections;
        if(cc > 2)
        {
            njoints++;
            nnjoints += cc;
        }
        if(cc < 2) ntips++;
    }

    unsigned int nbranches = tree->count();

    double length = 0.0;
    for(unsigned int b = 0; b < nbranches; b++)
    {
        std::vector<NodeIn3D> branch = tree->branch(b);
        unsigned int nmax = branch.size();
        for(unsigned int n = 1; n < nmax; n++)
        {
            double xx = branch[n-1].x - branch[n].x;
            double yy = branch[n-1].y - branch[n].y;
            double zz = branch[n-1].z - branch[n].z;
            length += sqrt(xx*xx+yy*yy+zz*zz);
        }
    }

    Tree mt = *tree;
    mt.rebuildTreeShortBranches();
    unsigned int nnets = mt.disconnectedNumber();

    QString info = QString("Disconnected groups: %1\nNodes: %2\nBranches: %3\nLength: %4\nJoints: %5\nTips: %6\nRadius: %7 to %8").arg(nnets).arg(nnodes).arg(nbranches).arg(length).arg(njoints).arg(ntips).arg(minr).arg(maxr);

    QMessageBox msgBox;
    msgBox.setText(info);
    msgBox.setWindowTitle("Tree info");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}



void MainWindow::on_toolButtonBalls_clicked()
{
    on_listWidgetTrees_itemChanged(NULL);
    //renderer->setViewBalls(ui->toolButtonBalls->isChecked());
}
void MainWindow::on_toolButtonWires_clicked()
{
    on_listWidgetTrees_itemChanged(NULL);
}

void MainWindow::on_toolButtonStream_clicked()
{
    on_listWidgetTrees_itemChanged(NULL);
}


void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_stereoCombo_currentIndexChanged(int index)
{
    renderer->setStereo(index, ui->stereoSlider->value());
}

void MainWindow::on_stereoSlider_valueChanged(int value)
{
    on_stereoCombo_currentIndexChanged(ui->stereoCombo->currentIndex());
}


void MainWindow::on_buttonImageInfo_clicked()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;

    QMessageBox msgBox;
    msgBox.setText(mi->info);
    msgBox.setWindowTitle("Image info");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void MainWindow::on_actionAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>VesselKnife</h2> " << std::endl;
    ss << "Program for the analysis of 3D angiography images and for the vascular system vectorization and modeling.<br><br>" << std::endl;
    ss << "Copyright 2015 by Piotr M. Szczypinski" << "<br>" << std::endl;
    ss << RELEASE_VERSION << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << "<br> <br>" << std::endl;
    ss << "For more information visit: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareVesselKnife.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareVesselKnife.html </a> <br>" << std::endl;
    ss << "Source codes available: <a href=\"https://gitlab.com/vesselknife/vesselknife\"> https://gitlab.com/vesselknife/vesselknife </a>" << std::endl;

    QMessageBox::about(this, "About VesselKnife", ss.str().c_str());
}

void MainWindow::on_actionContents_triggered()
{
    QDesktopServices::openUrl(QUrl("vesselknife.pdf", QUrl::TolerantMode));
}

//void MainWindow::frameChanged(int frame)
//{
//    renderer->setCrossSection(3, (unsigned int)frame);

//}


//void MainWindow::on_actionReload_raster_triggered()
//{
//    Byte3DImage * im = getCurrentImage();
//    if(im)
//    {
//        if(im->size[2] > 1)
//        {
//            unsigned int selected;
//            unsigned int frames = im->size[2];
//            ImageSelect dialog(frames, &selected, this);
//            connect(&dialog, SIGNAL(frameChanged(int)), this, SLOT(frameChanged(int)));
//            dialog.exec();
//            disconnect(&dialog, SIGNAL(frameChanged(int)), 0, 0);
//        }
//    }
//}

void MainWindow::on_imageLevelsButton_clicked()
{
    on_actionBackground_color_triggered();
}

void MainWindow::on_actionBackground_color_triggered()
{
    QColor color(renderer->backgroundColor());
    color = QColorDialog::getColor(color);
    if(color.isValid()) renderer->setBackgroundColor(color.rgb());
}

void MainWindow::on_listWidgetBuffers_currentRowChanged(int currentRow)
{
    if(timer.isActive())
    {
        timer.stop();
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-start.png"));
    }

    if(images.size() > currentRow)
    {
        Byte3DImage * im = images[currentRow];
        if(im)
        {
            unsigned int frames = im->framesInFile;
            unsigned int frame = im->currentFrame;
            if(frames > 1)
            {
                ui->frameSlider->setMaximum(frames-1);
                ui->frameSpinBox->setMaximum(frames-1);
                ui->frameSlider->setValue(frame);
                ui->frameSpinBox->setValue(frame);
                ui->frameSlider->setEnabled(true);
                ui->frameSpinBox->setEnabled(true);
                ui->playButton->setEnabled(true);
                return;
            }
        }
    }

    ui->frameSlider->setEnabled(false);
    ui->frameSpinBox->setEnabled(false);
    ui->playButton->setEnabled(false);
}

void MainWindow::on_frameSpinBox_valueChanged(int arg1)
{

    int ile = images.size();
    int row = ui->listWidgetBuffers->currentRow();
    if(row >= ile || ile <= 0 || row < 0) return;
    Byte3DImage* imgc = images[row];
    if(imgc->framesInFile <= (unsigned int)arg1) return;
    Byte3DImage* imgn = new Byte3DImage();

    if(loadNifti(imgc->filePathAndName, imgn, arg1))
    {
        images[row] = imgn;
        bool upd = false;
        for(int i = 0; i < 4; i++)
        {
            if(renderer->getImage(i) == imgc)
            {
                renderer->setImage(i, imgn, false);
                upd = true;
            }
        }
        if(upd) renderer->update();
        delete imgc;
    }
    else
    {
        delete imgn;
    }
}

void MainWindow::on_playButton_clicked()
{
    if(timer.isActive())
    {
        timer.stop();
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-start.png"));
    }
    else
    {
        timer.start(100);
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-stop.png"));
    }
}

void MainWindow::processTimer(void)
{
    if(ui->frameSpinBox->value() < ui->frameSpinBox->maximum())
    {
        ui->frameSpinBox->setValue(ui->frameSpinBox->value()+1);
    }
    else
    {
        ui->frameSpinBox->setValue(0);
        timer.stop();
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-start.png"));
    }
}
