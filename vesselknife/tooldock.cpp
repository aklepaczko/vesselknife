#include "../shared/treediameters.h"
#include "../shared/mz2itk.h"
#include "../shared/imagefilters.h"
#include "../shared/imagefilters2.h"
#include "../shared/buildtree.h"
#include "../shared/treesmooth.h"
#include "../shared/treeextend.h"
#include "../shared/smoothdiameter.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <QMessageBox>


void MainWindow::invert()
{
    unsigned int d;
    Byte3DImage* input = getCurrentImage();
    if(input == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    Byte3DImage* output = new Byte3DImage();
    output->allocate(input->size);
    for(d = 0; d < 3; d++)
        output->voxelsize[d] = input->voxelsize[d];

    unsigned int max = output->allsize();

    unsigned char* ptri = input->data;
    unsigned char* ptro = output->data;
    for(d = 0; d < max; d++, ptri++, ptro++) *ptro = 255 - *ptri;
    appendToImages(output, QString("inverted"));
    setCursor(oc);
}

void MainWindow::gaussian()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float sigma = getParameter(QString("gaussianSigma")).toFloat();
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters<float>::gaussianFilter(ii, sigma);
    Byte3DImage* ni = Mz2Itk<float>::getMZNormalized(oi);
    //ii->Delete();
    appendToImages(ni, QString("gaussFiltered"));
    //delete ni;
    setCursor(oc);
}

void MainWindow::vesselness()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float sigma = getParameter(QString("vesselnessSigma")).toFloat();
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters<float>::hessianFilter(ii, sigma);
    Byte3DImage* ni = Mz2Itk<float>::getMZNormalized(oi);
    appendToImages(ni, QString("vesselness"));
    setCursor(oc);
}

void MainWindow::multiscaleVesselness()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float stdmin = getParameter(QString("vesselnessMinStd")).toFloat();
    float stdmax = getParameter(QString("vesselnessMaxStd")).toFloat();
    int scales = getParameter(QString("vesselnessNoOfScales")).toInt();
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi, 0.003921568); //convertion from 0-255 to 0.0-1.0
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters<float>::multiscaleHessianAlgorithmG(ii, stdmin, stdmax, scales);
    Byte3DImage* ni = Mz2Itk<float>::getMZNormalized(oi);
    appendToImages(ni, QString("multiscaleVesselness"));
    setCursor(oc);
}

void MainWindow::whiteForegroundHoleFill()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    unsigned char foreground = ImageFilters< unsigned char >::maxIntensity(ii);
    ImageFilters< unsigned char >::ImageType::Pointer oi = ImageFilters<unsigned char>::holeFill(ii, foreground);
    Byte3DImage* ni = Mz2Itk<unsigned char>::getMZ(oi);
    appendToImages(ni, QString("whiteForegroundHoleFill"));
    setCursor(oc);
}

void MainWindow::blackForegroundHoleFill()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    unsigned char foreground = ImageFilters< unsigned char >::minIntensity(ii);
    ImageFilters< unsigned char >::ImageType::Pointer oi = ImageFilters<unsigned char>::holeFill(ii, foreground);
    Byte3DImage* ni = Mz2Itk<unsigned char>::getMZ(oi);
    appendToImages(ni, QString("blackForegroundHoleFill"));
    setCursor(oc);
}

void MainWindow::whiteTopHat()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float radius = getParameter(QString("whiteTopHatRadius")).toFloat();
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    ImageFilters<float>::ImageType::Pointer oi = ImageFilters<float>::whiteTopHat(ii, radius);
    Byte3DImage* ni = Mz2Itk<float>::getMZNormalized(oi);
    appendToImages(ni, QString("whiteTopHat"));
    setCursor(oc);
}

void MainWindow::blackTopHat()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float radius = getParameter(QString("blackTopHatRadius")).toFloat();
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    ImageFilters<float>::ImageType::Pointer oi = ImageFilters<float>::blackTopHat(ii, radius);
    Byte3DImage* ni = Mz2Itk<float>::getMZNormalized(oi);
    appendToImages(ni, QString("whiteTopHat"));
    setCursor(oc);
}

void MainWindow::dilate()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float radius = getParameter(QString("dilateRadius")).toFloat();
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    ImageFilters<unsigned char>::ImageType::Pointer oi = ImageFilters<unsigned char>::dilate(ii, radius);
    Byte3DImage* ni = Mz2Itk<unsigned char>::getMZNormalized(oi);
    appendToImages(ni, QString("dilated%1").arg(radius));
    setCursor(oc);
}

void MainWindow::erode()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float radius = getParameter(QString("erodeRadius")).toFloat();
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    ImageFilters<unsigned char>::ImageType::Pointer oi = ImageFilters<unsigned char>::erode(ii, radius);
    Byte3DImage* ni = Mz2Itk<unsigned char>::getMZNormalized(oi);
    appendToImages(ni, QString("eroded%1").arg(radius));
    setCursor(oc);
}

void MainWindow::floodFill()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float min = getParameter(QString("floodFillMin")).toFloat();
    float max = getParameter(QString("floodFillMax")).toFloat();
    int co[3];
    getCurrentSeed(co);
    ImageFilters< float >::ImageType::IndexType coords;
    coords[0] = co[0];
    coords[1] = co[1];
    coords[2] = co[2];

    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Mz2Itk<unsigned char>::Image3DType::Pointer oi = ImageFilters<unsigned char>::regionGrowing(ii, min, max, coords);
    Byte3DImage* ni = Mz2Itk<unsigned char>::getMZNormalized(oi);

    appendToImages(ni, QString("floodFill"));
    setCursor(oc);
}

void MainWindow::threshold()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    float min = getParameter(QString("thresholdMin")).toFloat();
    float max = getParameter(QString("thresholdMax")).toFloat();

    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Mz2Itk<unsigned char>::Image3DType::Pointer oi = ImageFilters<unsigned char>::thresholdFilter(ii, min, max);
    Byte3DImage* ni = Mz2Itk<unsigned char>::getMZNormalized(oi);

    appendToImages(ni, QString("threshold"));
    setCursor(oc);
}

void MainWindow::skeletonFromBinary()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Mz2Itk<unsigned char>::Image3DType::Pointer oi = ImageFilters<unsigned char>::skeletonFromBinary(ii);
    Byte3DImage* ni = Mz2Itk<unsigned char>::getMZNormalized(oi);
    appendToImages(ni, QString("skeleton"));
    setCursor(oc);
}


void MainWindow::skeletonToTree()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();

    int co[3];
    getCurrentSeed(co);
    ImageFilters< unsigned char >::ImageType::IndexType coords;
    coords[0] = co[0];
    coords[1] = co[1];
    coords[2] = co[2];

    setCursor(Qt::WaitCursor);
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Tree tree = BuildTree<unsigned char>::skeletonToTree(ii, coords);

    appendToTrees(tree, QString("tree"));
    setCursor(oc);
}

void MainWindow::smoothTreeBranches()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    float d1 = getParameter(QString("smoothMinimizeFirstDerivative")).toFloat();
    float d2 = getParameter(QString("smoothMinimizeSecondDerivative")).toFloat();
    float fa = getParameter(QString("smoothAnchorSpring")).toFloat();
    float faaa = getParameter(QString("smoothAnchorCable")).toFloat();
    int iterations = getParameter(QString("smoothIterations")).toInt();
    int chillout = getParameter(QString("smoothChillout")).toInt();

    TreeSmooth smoothing(mt, d1, d2, fa, faaa);
    Tree tree = smoothing.getResult(iterations, chillout);
    appendToTrees(tree, QString("smooth"));
    setCursor(oc);
}

void MainWindow::smoothDiameter()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    float d1 = getParameter(QString("smoothDiameterMinimizeFirstDerivative")).toFloat();
    float fa = getParameter(QString("smoothDiameterAnchorSpring")).toFloat();
    float faaa = getParameter(QString("smoothDiameterAnchorCable")).toFloat();
    int iterations = getParameter(QString("smoothDiameterIterations")).toInt();
    int chillout = getParameter(QString("smoothDiameterChillout")).toInt();

    SmoothDiameter smoothing(mt, d1, fa, faaa);
    Tree tree = smoothing.getResult(iterations, chillout);
    appendToTrees(tree, QString("smoothDiameter"));
    setCursor(oc);
}


void MainWindow::svdBasedDiameters()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return;
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    int divider = getParameter(QString("svdBasedDiametersDivider")).toInt();
    float gamma = getParameter(QString("svdBasedDiametersGamma")).toFloat();
    float rho = getParameter(QString("svdBasedDiametersRho")).toFloat();

    TreeDiameters diams(mt, mi, gamma, divider, rho);
    Tree tree = diams.getResult();


    //Tests:
    //int co[3];
    //getCurrentSeed(co);
    //Tree tree = diams.getResult((unsigned int*) co);
    //Tree tree = diams.getResult(100000);


    appendToTrees(tree, QString("svdDiameters"));
    setCursor(oc);
}



void MainWindow::crop()
{
    unsigned int min[3];
    unsigned int max[3];

    max[0] = getParameter(QString("cropXmax")).toInt();
    max[1] = getParameter(QString("cropYmax")).toInt();
    max[2] = getParameter(QString("cropZmax")).toInt();
    min[0] = getParameter(QString("cropXmin")).toInt();
    min[1] = getParameter(QString("cropYmin")).toInt();
    min[2] = getParameter(QString("cropZmin")).toInt();

    if(!(images.size()>0 && max[0] > min[0] && max[1] > min[1] && max[2] > min[2])) return;
    Byte3DImage* img = images[0];
    if(!(max[0] < img->size[0] && max[1] < img->size[1] && max[2] < img->size[2])) return;

    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    for(int i = 0; i < images.size(); i++)
    {
        images[i]->crop(min, max);
    }
    ui->horizontalSlider_X->setMaximum(img->size[0]-1);
    ui->spinBox_X->setMaximum(img->size[0]-1);
    ui->spinBox_X->setValue(img->size[0]/2);
    ui->horizontalSlider_Y->setMaximum(img->size[1]-1);
    ui->spinBox_Y->setMaximum(img->size[1]-1);
    ui->spinBox_Y->setValue(img->size[1]/2);
    ui->horizontalSlider_Z->setMaximum(img->size[2]-1);
    ui->spinBox_Z->setMaximum(img->size[2]-1);
    ui->spinBox_Z->setValue(img->size[2]/2);
    ui->checkBox_X->setChecked(true);
    ui->checkBox_Y->setChecked(true);
    ui->checkBox_Z->setChecked(true);
    renderer->setImage(0, img);
    renderer->setImage(1, img);
    renderer->setImage(2, img);
    renderer->setImage(3, img);
    on_listWidgetTrees_itemChanged(NULL);
    setCursor(oc);
}

void MainWindow::extendBranches()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return;
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    float mingoal = getParameter(QString("extendBranchesMinCriterion")).toFloat();
    float sigma = getParameter(QString("extendBranchesSigma")).toFloat();
    bool normalize = getParameter(QString("extendBranchesNormalize")).toInt();
    float alfa1 = getParameter(QString("extendBranchesAlpha1")).toFloat();
    float alfa2 = getParameter(QString("extendBranchesAlpha2")).toFloat();
    int maxnodes = getParameter(QString("extendBranchesMaxNodes")).toInt();

    TreeExtend tool(mt, Mz2Itk<double>::getItk(mi), mingoal, maxnodes, sigma, normalize, alfa1, alfa2);
    Tree tree = tool.getResult();

    appendToTrees(tree, QString("extendedTree"));
    setCursor(oc);
}

void MainWindow::disjoinBranches()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    std::vector<Tree> ntrees;

    unsigned int nb = mt->count();
    for(unsigned int n = 0; n < nb; n++)
    {
        Tree tree;

        std::vector<NodeIn3D> branch = mt->branch(n);
        if(branch.size() > 0)
        {
            tree.addBranch(branch);
            ntrees.push_back(tree);
        }
    }

    trees.insert(trees.end(), ntrees.begin(), ntrees.end());
    nb = ntrees.size();
    for(unsigned int n = 0; n < nb; n++)
    {
        treecolors.push_back(QColor(Qt::red).rgb());
        QListWidgetItem *item = new QListWidgetItem(ui->listWidgetTrees);
        item->setText(QString("disconnect%1").arg(n));
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);
        setColorToItemIcon(item, QColor(Qt::red));
    }
    on_listWidgetTrees_itemChanged(NULL);
    setCursor(oc);
}



void MainWindow::disjoinDisconnected()
{
    Tree* mmt = getCurrentTree();
    if(mmt == 0) return;
    Tree mt = *mmt;
    mt.rebuildTreeShortBranches();

    unsigned int nb = mt.count();
    if(nb <= 0) return;

    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    int* branch_membership = new int[nb];
    int disconnected = mt.disconnectedNumber(branch_membership);
    std::vector<Tree> ntrees;

    for(unsigned int nt = 0; nt < disconnected; nt++)
    {
        Tree tree;
        for(unsigned int n = 0; n < nb; n++)
        {
            if(branch_membership[n] == nt)
            {
                std::vector<NodeIn3D> branch = mt.branch(n);
                if(branch.size() > 0)
                {
                    tree.addBranch(branch);
                }
            }
        }
        if(tree.count() > 0) ntrees.push_back(tree);
    }
    delete[] branch_membership;

    trees.insert(trees.end(), ntrees.begin(), ntrees.end());
    nb = ntrees.size();
    for(unsigned int n = 0; n < nb; n++)
    {
        treecolors.push_back(QColor(Qt::red).rgb());
        QListWidgetItem *item = new QListWidgetItem(ui->listWidgetTrees);
        item->setText(QString("disconnect%1").arg(n));
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);
        setColorToItemIcon(item, QColor(Qt::red));
    }

    on_listWidgetTrees_itemChanged(NULL);
    setCursor(oc);
}


void MainWindow::joinChecked()
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    Tree tree;

    int rsize = trees.size();
    if(ui->listWidgetTrees->count() < rsize) rsize = ui->listWidgetTrees->count();

    bool add = false;
    for(int r = 0; r < rsize; r++)
    {
        if(ui->listWidgetTrees->item(r)->checkState() == Qt::Checked)
        {
            unsigned int nb = trees[r].count();
            for(unsigned int b = 0; b < nb; b++)
            {
                std::vector<NodeIn3D> branch = trees[r].branch(b);
                tree.addBranch(branch, false);
                add = true;
            }
        }
    }
    if(add)
    {
        tree.correctConnectivity();
        appendToTrees(tree, QString("connectedTree"));
    }
    setCursor(oc);
}


void MainWindow::longBranchesFormat()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    Tree tree = *(mt);
    if(tree.rebuildTreeLongBranches())
    {
        appendToTrees(tree, QString("longBranchesFormat"), 0x77ff00);
    }
    setCursor(oc);
}

void MainWindow::shortBranchesFormat()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    Tree tree = *(mt);
    if(tree.rebuildTreeShortBranches())
    {
        appendToTrees(tree, QString("shortBranchesFormat"), 0x77ff00);
    }
    setCursor(oc);
}

void MainWindow::levelSetChanVese()
{
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    int co[3];
    getCurrentSeed(co);
    ImageFilters2< float >::ImageType::IndexType coords;
    coords[0] = co[0];
    coords[1] = co[1];
    coords[2] = co[2];

    unsigned int nb_iteration = getParameter(QString("levelSetChanVeseIterations")).toInt();
    double initialDistance = getParameter(QString("levelSetChanVeseInitialDistance")).toFloat();
    double rms = getParameter(QString("levelSetChanVeseRms")).toFloat();
    double epsilon = getParameter(QString("levelSetChanVeseEpsilon")).toFloat();
    double curvature_weight = getParameter(QString("levelSetChanVeseCurvatureWeight")).toFloat();
    double area_weight = getParameter(QString("levelSetChanVeseAreaWeight")).toFloat();
    double reinitialization_weight = getParameter(QString("levelSetChanVeseReinitialization")).toFloat();
    double volume_weight = getParameter(QString("levelSetChanVeseVolumeWeight")).toFloat();
    double volume = getParameter(QString("levelSetChanVeseVolume")).toFloat();
    double l1 = getParameter(QString("levelSetChanVeseLambda1")).toFloat();
    double l2 = getParameter(QString("levelSetChanVeseLambda2")).toFloat();

    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters2<float>::levelSetChanVese(ii, coords,
                                                                                    nb_iteration,
                                                                                    initialDistance,
                                                                                    rms,
                                                                                    epsilon,
                                                                                    curvature_weight,
                                                                                    area_weight,
                                                                                    reinitialization_weight,
                                                                                    volume_weight,
                                                                                    volume,
                                                                                    l1,
                                                                                    l2);
    Byte3DImage* ni = Mz2Itk<float>::getMZNormalized(oi);

    appendToImages(ni, QString("levelsetChanVese"));
    setCursor(oc);
}

void MainWindow::runNotImplemented()
{
    QMessageBox msgBox;
    msgBox.setText("Not implemented");
    msgBox.setWindowTitle("Warning");
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
    return;
}

QString MainWindow::getParameter(QString name)
{
    QList<QTreeWidgetItem*> items = ui->treeWidget->findItems("", Qt::MatchStartsWith | Qt::MatchRecursive, 0);
    int count = items.count();
    for(int n = 0; n < count; n++)
    {
        if(items[n]->text(0) == name) return items[n]->text(1);
    }
    return QString("");
}

void MainWindow::getCurrentSeed(int coords[3])
{
    QListWidgetItem* item = ui->listWidgetSeed->currentItem();
    if(item != NULL)
    {
        QStringList list = item->text().split(" ");
        if(list.size() >= 3)
        {
            coords[0] = list[0].toInt();
            coords[1] = list[1].toInt();
            coords[2] = list[2].toInt();
            return;
        }
    }
    Byte3DImage* mi = getCurrentImage();
    if(mi == NULL) return;
    coords[0] = mi->size[0]/2;
    coords[1] = mi->size[1]/2;
    coords[2] = mi->size[2]/2;
}


bool MainWindow::initTools(void)
{
    ui->treeWidget->setItemDelegateForColumn(0, new NoEditDelegate(this));

    QList<QTreeWidgetItem*> items = ui->treeWidget->findItems("", Qt::MatchStartsWith | Qt::MatchRecursive, 0);
    int count = items.count();
    for(int n = 0; n < count; n++)
    {
        if(items[n]->parent() == NULL) //root item
        {
            QPushButton *button = new QPushButton("Run");
            ui->treeWidget->setItemWidget(items[n], 1, button);
            connect(button, SIGNAL(clicked()), this, QString("1%1()").arg(items[n]->text(0)).toStdString().c_str());
        }
    }
    return true;
}
