#-------------------------------------------------
#
# Project created by QtCreator 2014-12-28T20:40:55
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
DESTDIR         = ../../Executables

TARGET = VesselKnife
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    imagegl.cpp \
    ../shared/sphear.cpp \
    ../shared/tree.cpp \
    tooldock.cpp \
    treelist.cpp \
    ../shared/treesmooth.cpp \
    ../shared/treediameters.cpp \
    ../shared/treeextend.cpp \
    ../shared/smoothdiameter.cpp \
    nodeproperties.cpp

HEADERS  += mainwindow.h \
    imagegl.h \
    ../shared/sphear.h \
    isetview.h \
    ../shared/imagefilters.h \
    ../shared/tree.h \
    ../shared/buildtree.h \
    ../shared/mz2itk.h \
    ../shared/treesmooth.h \
    ../shared/treediameters.h \
    ../shared/byteimage.h \
    ../shared/treeextend.h \
    ../shared/imagefilters2.h \
    ../shared/smoothdiameter.h \
    nodeproperties.h

FORMS    += mainwindow.ui \
    nodeproperties.ui

RESOURCES += \
    vesselknife.qrc    

macx{
    INCLUDEPATH += /usr/include/nifti
    INCLUDEPATH += "/usr/local/include/ITK-4.6"

    LIBS += \
            /usr/local/lib/libitksys-4.6.dylib\
            /usr/local/lib/libITKBiasCorrection-4.6.dylib\
            /usr/local/lib/libITKBioCell-4.6.dylib\
            /usr/local/lib/libITKCommon-4.6.dylib\
            /usr/local/lib/libITKDICOMParser-4.6.dylib\
            /usr/local/lib/libitkdouble-conversion-4.6.dylib\
            /usr/local/lib/libITKEXPAT-4.6.dylib\
            /usr/local/lib/libITKFEM-4.6.dylib\
            /usr/local/lib/libitkhdf5_cpp-4.6.dylib\
            /usr/local/lib/libitkhdf5-4.6.dylib\
            /usr/local/lib/libITKIOBioRad-4.6.dylib\
            /usr/local/lib/libITKIOBMP-4.6.dylib\
            /usr/local/lib/libITKIOCSV-4.6.dylib\
            /usr/local/lib/libITKIOGDCM-4.6.dylib\
            /usr/local/lib/libITKIOGE-4.6.dylib\
            /usr/local/lib/libITKIOGIPL-4.6.dylib\
            /usr/local/lib/libITKIOHDF5-4.6.dylib\
            /usr/local/lib/libITKIOImageBase-4.6.dylib\
            /usr/local/lib/libITKIOIPL-4.6.dylib\
            /usr/local/lib/libITKIOJPEG-4.6.dylib\
            /usr/local/lib/libITKIOLSM-4.6.dylib\
            /usr/local/lib/libITKIOMesh-4.6.dylib\
            /usr/local/lib/libITKIOMeta-4.6.dylib\
            /usr/local/lib/libITKIOMRC-4.6.dylib\
            /usr/local/lib/libITKIONIFTI-4.6.dylib\
            /usr/local/lib/libITKIONRRD-4.6.dylib\
            /usr/local/lib/libITKIOPNG-4.6.dylib\
            /usr/local/lib/libITKIOSiemens-4.6.dylib\
            /usr/local/lib/libITKIOSpatialObjects-4.6.dylib\
            /usr/local/lib/libITKIOStimulate-4.6.dylib\
            /usr/local/lib/libITKIOTIFF-4.6.dylib\
            /usr/local/lib/libITKIOTransformBase-4.6.dylib\
            /usr/local/lib/libITKIOTransformHDF5-4.6.dylib\
            /usr/local/lib/libITKIOTransformInsightLegacy-4.6.dylib\
            /usr/local/lib/libITKIOTransformMatlab-4.6.dylib\
            /usr/local/lib/libITKIOVTK-4.6.dylib\
            /usr/local/lib/libITKIOXML-4.6.dylib\
            /usr/local/lib/libITKKLMRegionGrowing-4.6.dylib\
            /usr/local/lib/libITKLabelMap-4.6.dylib\
            /usr/local/lib/libITKMesh-4.6.dylib\
            /usr/local/lib/libITKMetaIO-4.6.dylib\
            /usr/local/lib/libitkNetlibSlatec-4.6.dylib\
            /usr/local/lib/libITKniftiio-4.6.dylib\
            /usr/local/lib/libITKNrrdIO-4.6.dylib\
            /usr/local/lib/libitkopenjpeg-4.6.dylib\
            /usr/local/lib/libITKOptimizers-4.6.dylib\
            /usr/local/lib/libITKPath-4.6.dylib\
            /usr/local/lib/libITKPolynomials-4.6.dylib\
            /usr/local/lib/libITKQuadEdgeMesh-4.6.dylib\
            /usr/local/lib/libITKSpatialObjects-4.6.dylib\
            /usr/local/lib/libITKStatistics-4.6.dylib\
            /usr/local/lib/libitkv3p_lsqr-4.6.dylib\
            /usr/local/lib/libitkv3p_netlib-4.6.dylib\
            /usr/local/lib/libitkvcl-4.6.dylib\
            /usr/local/lib/libITKVideoCore-4.6.dylib\
            /usr/local/lib/libITKVideoIO-4.6.dylib\
            /usr/local/lib/libitkvnl_algo-4.6.dylib\
            /usr/local/lib/libitkvnl-4.6.dylib\
            /usr/local/lib/libITKVNLInstantiation-4.6.dylib\
            /usr/local/lib/libITKVTK-4.6.dylib\
            /usr/local/lib/libITKWatersheds-4.6.dylib\
            /usr/local/lib/libITKznz-4.6.dylib
}
else:unix{
    INCLUDEPATH += /usr/include/nifti
    INCLUDEPATH +=  "/usr/include/ITK-4.7/Common"\
                    "/usr/include/ITK-4.7"\
                    "/usr/include/ITK-4.7/Utilities/vxl/vcl"\
                    "/usr/include/ITK-4.7/Utilities/vxl/core"\
                    "/usr/include/ITK-4.7/IO"\
                    "/usr/include/ITK-4.7/Utilities"\
                    "/usr/include/ITK-4.7/BasicFilters"\
                    "/usr/include/ITK-4.7/Review"
    #INCLUDEPATH +=  "/usr/include/vtk-5.8"
    #INCLUDEPATH +=  "/usr/include/InsightToolkit/Common"\
    #                "/usr/include/InsightToolkit"\
    #                "/usr/include/InsightToolkit/Utilities/vxl/vcl"\
    #                "/usr/include/InsightToolkit/Utilities/vxl/core"\
    #                "/usr/include/InsightToolkit/IO"\
    #                "/usr/include/InsightToolkit/Utilities"\
    #                "/usr/include/InsightToolkit/BasicFilters"\
    #                "/usr/include/InsightToolkit/Review"

    #LIBS += -lGL
    LIBS += -lGLU
    #LIBS += -lglut

    LIBS += -lniftiio
    LIBS += -lalglib

    ## vtk 5
    #unix|win32: LIBS += \
    #                    -lvtkGraphics\
    #                    -lvtkWidgets\
    #                    -lvtkRendering\
    #                    -lvtkImaging\
    #                    -lvtkCommon\
    #                    -lvtkFiltering\
    #                    -lvtkHybrid \
    #                    -lvtkVolumeRendering \
    #                    -lvtkIO
    ##                    -lQVTK

    ## itk3
    #unix|win32: LIBS += \
    #                    -lITKBasicFilters\
    #                    -lITKIO\
    #                    -lITKCommon\
    #                    -lITKDICOMParser\
    #                    -lITKEXPAT\
    #                    -lITKFEM\
    #                    -lITKMetaIO\
    #                    -litkNetlibSlatec\
    #                    -lITKniftiio\
    #                    -lITKNrrdIO\
    #                    -lITKStatistics\
    #                    -lITKQuadEdgeMesh\
    #                    -lITKStatistics\
    #                    -litksys\
    #                    -litkv3p_lsqr\
    #                    -litkv3p_netlib\
    #                    -litkvcl\
    #                    -litkvnl\
    #                    -litkvnl_algo\
    #                    -lITKznz\
    #                    -lITKAlgorithms\
    #                    -lITKNumerics\
    #                    -litksys\
    #                    -lITKAlgorithms

    #itk4
    LIBS += \
                        -litksys-4.7\
                        -lITKBiasCorrection-4.7\
                        -lITKBioCell-4.7\
                        -lITKCommon-4.7\
                        -lITKDICOMParser-4.7\
                        -litkdouble-conversion-4.7\
                        -lITKEXPAT-4.7\
                        -lITKFEM-4.7\
                        -lITKIOBioRad-4.7\
                        -lITKIOBMP-4.7\
                        -lITKIOCSV-4.7\
                        -lITKIOGDCM-4.7\
                        -lITKIOGE-4.7\
                        -lITKIOGIPL-4.7\
                        -lITKIOHDF5-4.7\
                        -lITKIOImageBase-4.7\
                        -lITKIOIPL-4.7\
                        -lITKIOJPEG-4.7\
                        -lITKIOLSM-4.7\
                        -lITKIOMesh-4.7\
                        -lITKIOMeta-4.7\
                        -lITKIOMRC-4.7\
                        -lITKIONIFTI-4.7\
                        -lITKIONRRD-4.7\
                        -lITKIOPNG-4.7\
                        -lITKIOSiemens-4.7\
                        -lITKIOSpatialObjects-4.7\
                        -lITKIOStimulate-4.7\
                        -lITKIOTIFF-4.7\
                        -lITKIOTransformBase-4.7\
                        -lITKIOTransformHDF5-4.7\
                        -lITKIOTransformInsightLegacy-4.7\
                        -lITKIOTransformMatlab-4.7\
                        -lITKIOVTK-4.7\
                        -lITKIOXML-4.7\
                        -lITKKLMRegionGrowing-4.7\
                        -lITKLabelMap-4.7\
                        -lITKMesh-4.7\
                        -lITKMetaIO-4.7\
                        -litkNetlibSlatec-4.7\
                        -lITKniftiio-4.7\
                        -lITKNrrdIO-4.7\
                        -litkopenjpeg-4.7\
                        -lITKOptimizers-4.7\
                        -lITKPath-4.7\
                        -lITKPolynomials-4.7\
                        -lITKQuadEdgeMesh-4.7\
                        -lITKSpatialObjects-4.7\
                        -lITKStatistics-4.7\
                        -litkv3p_lsqr-4.7\
                        -litkv3p_netlib-4.7\
                        -litkvcl-4.7\
                        -lITKVideoCore-4.7\
                        -lITKVideoIO-4.7\
                        -litkvnl_algo-4.7\
                        -litkvnl-4.7\
                        -lITKVNLInstantiation-4.7\
                        -lITKVTK-4.7\
                        -lITKWatersheds-4.7\
                        -lITKznz-4.7

}
else:win32{
    RC_FILE = vesselknife.rc

    INCLUDEPATH += D:/alglib/cpp/src

    INCLUDEPATH += E:\Program\VesselTree\vtworker\build\ITKIOFactoryRegistration

    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\Core\Common
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\VNL\src\vxl\core
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\VNL\src\vxl\vcl
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\VNL\src\vxl\v3p\netlib
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\PNG\src
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\NrrdIO\src\NrrdIO
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\TIFF\src\itktiff
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\TIFF\src
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\JPEG\src
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\HDF5\src
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\GDCM\src\gdcm\Source\Common
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\GDCM
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\OpenJPEG\src\openjpeg
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\Expat\src\expat
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\DICOMParser\src\DICOMParser
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\MetaIO\src\MetaIO
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\ZLIB\src
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\IO\ImageBase
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\Netlib
    INCLUDEPATH += E:\Program\ThirdParty\libitk_static_md\Modules\ThirdParty\KWSys\src

    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Nonunit\Review\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\NIFTI\src\nifti\niftilib
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\Watersheds\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\Voronoi\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Video\IO\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Video\Filtering\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Video\Core\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Bridge\VTK\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\TestKernel\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\SpatialFunction\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Registration\RegistrationMethodsv4\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\RegionGrowing\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\QuadEdgeMeshFiltering\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\NeuralNetworks\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Registration\Metricsv4\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\Optimizersv4\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\MarkovRandomFieldsClassifiers\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\LevelSetsv4\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\LabelVoting\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\KLMRegionGrowing\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageFusion\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\VTK\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\TransformMatlab\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\TransformInsightLegacy\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\TransformHDF5\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\TransformBase\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\Stimulate\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\Siemens\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\RAW\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\PNG\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\PNG\src
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\NRRD\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\NrrdIO\src\NrrdIO
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\NIFTI\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\Meta\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\Mesh\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\MRC\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\LSM\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\TIFF\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\TIFF\src
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\JPEG\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\JPEG\src
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\HDF5\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\GIPL\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\GE\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\IPL\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\GDCM\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\CSV\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\BioRad\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\BMP\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\HDF5\src
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\GPUThresholding\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\GPUSmoothing\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Registration\GPUPDEDeformable\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Registration\GPUCommon\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\GPUImageFilterBase\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\GPUAnisotropicSmoothing\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\GPUFiniteDifference\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\GPUCommon\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\GIFTI\src\gifticlib
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\NIFTI\src\nifti\znzlib
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\NIFTI\src\nifti\niftilib
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\GDCM\src\gdcm\Source\DataStructureAndEncodingDefinition
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\GDCM\src\gdcm\Source\MessageExchangeDefinition
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\GDCM\src\gdcm\Source\InformationObjectDefinition
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\GDCM\src\gdcm\Source\Common
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\GDCM\src\gdcm\Source\DataDictionary
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\GDCM\src\gdcm\Source\MediaStorageAndFileFormat
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\OpenJPEG\src\openjpeg
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Registration\FEM\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Registration\PDEDeformable\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\FEM\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Registration\Common\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\SpatialObjects\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\XML\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\Expat\src\expat
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\Eigen\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\DisplacementField\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\DiffusionTensorImage\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\Denoising\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\DeformableMesh\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\Deconvolution\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\DICOMParser\src\DICOMParser
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\Convolution\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\FFT\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\Colormap\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\Classifiers\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\BioCell\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\BiasCorrection\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\Polynomials\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\AntiAlias\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\LevelSets\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\SignedDistanceFunction\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\Optimizers\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageFeature\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageSources\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageGradient\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\Smoothing\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageCompare\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\FastMarching\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\QuadEdgeMesh\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\DistanceMap\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\NarrowBand\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\BinaryMathematicalMorphology\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\LabelMap\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\MathematicalMorphology\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Segmentation\ConnectedComponents\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\Thresholding\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageLabel\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageIntensity\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\Path\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageStatistics\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\SpatialObjects\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\MetaIO\src\MetaIO
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\ZLIB\src
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\Mesh\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\IO\ImageBase\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageCompose\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\AnisotropicSmoothing\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageGrid\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\ImageFunction\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\Transform\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Numerics\Statistics\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\ImageAdaptors\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\CurvatureFlow\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Filtering\ImageFilterBase\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\FiniteDifference\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\Core\Common\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\VNLInstantiation\include
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\VNL\src\vxl\core
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\VNL\src\vxl\vcl
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\VNL\src\vxl\v3p\netlib
    INCLUDEPATH += E:\Program\ThirdParty\InsightToolkit-4.5.2\Modules\ThirdParty\DoubleConversion\src\double-conversion

    #LIBS += -lGLU

    LIBS += E:\Program\ThirdParty\alglib\ReleaseMD32\alglib.lib

    LIBS += advapi32.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkdouble-conversion-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itksys-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkvnl_algo-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkvnl-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkv3p_netlib-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKCommon-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkNetlibSlatec-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKStatistics-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOImageBase-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKMesh-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkzlib-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKMetaIO-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKSpatialObjects-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKPath-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKLabelMap-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKQuadEdgeMesh-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKOptimizers-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKPolynomials-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKBiasCorrection-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKBioCell-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKDICOMParser-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKEXPAT-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOXML-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOSpatialObjects-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKFEM-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkopenjpeg-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmDICT-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmMSFF-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKznz-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKniftiio-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKgiftiio-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkhdf5_cpp-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOBMP-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOBioRad-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOCSV-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOGDCM-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOIPL-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOGE-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOGIPL-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOHDF5-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkjpeg-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOJPEG-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itktiff-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOTIFF-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOLSM-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOMRC-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOMesh-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOMeta-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIONIFTI-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKNrrdIO-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIONRRD-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkpng-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOPNG-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOSiemens-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOStimulate-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOTransformBase-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOTransformHDF5-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOTransformInsightLegacy-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOTransformMatlab-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKIOVTK-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKKLMRegionGrowing-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKVTK-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKVideoCore-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKVideoIO-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKWatersheds-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmIOD-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmDSED-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmCommon-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmjpeg8-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmjpeg12-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkgdcmjpeg16-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\ITKVNLInstantiation-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkv3p_lsqr-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkvcl-4.5.lib
    LIBS += E:\Program\ThirdParty\libitk_static_md\lib\Release\itkhdf5-4.5.lib
}    
    
 
