Selected icons used in qmazda are from KDE Oxygen project. 
They are licensed under GNU LGPL 3:
https://techbase.kde.org/Projects/Oxygen/Licensing

List of the icons from Oxygen project:
dialog-cancel.png
help-about.png
list-remove.png
zoom-original.png
dialog-ok-apply.png
help-contents.png
run-build.png
zoom-out.png
color.png
documentinfo.png
layer-visible-off.png
trash-empty.png
configure.png
document-open.png
layer-visible-on.png
pointer.png
document-print.png
window-close.png
document-save.png
list-add.png
zoom-in.png