#-------------------------------------------------
#
# Project created by QtCreator 2014-12-28T20:40:55
#
#-------------------------------------------------

TEMPLATE = app
TARGET = tree2vtk

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../../Executables

SOURCES += \
    ../shared/tree.cpp \
    tree2vtk.cpp

HEADERS  += \
    ../shared/tree.h

macx{
}
else:unix{
## vtk 5
#    INCLUDEPATH +=  "/usr/include/vtk-5.8"
#    LIBS += \
#                    -lvtkImaging\
#                    -lvtkCommon\
#                    -lvtkFiltering\
#                    -lvtkHybrid \
#                    -lvtkIO

#vtk 6.0
    INCLUDEPATH +=  "/usr/include/vtk-6.0"
    LIBS += -lvtkalglib-6.0 \
        -lvtkChartsCore-6.0 \
        -lvtkCommonColor-6.0 \
        -lvtkCommonDataModel-6.0 \
        -lvtkCommonMath-6.0 \
        -lvtkCommonCore-6.0 \
        -lvtksys-6.0 \
        -lvtkCommonMisc-6.0 \
        -lvtkCommonSystem-6.0 \
        -lvtkCommonTransforms-6.0 \
        -lvtkInfovisCore-6.0 \
        -lvtkFiltersExtraction-6.0 \
        -lvtkCommonExecutionModel-6.0 \
        -lvtkFiltersCore-6.0 \
        -lvtkFiltersGeneral-6.0 \
        -lvtkCommonComputationalGeometry-6.0 \
        -lvtkFiltersStatistics-6.0 \
        -lvtkImagingFourier-6.0 \
        -lvtkImagingCore-6.0 \
        -lvtkRenderingContext2D-6.0 \
        -lvtkRenderingCore-6.0 \
        -lvtkFiltersGeometry-6.0 \
        -lvtkFiltersSources-6.0 \
        -lvtkIOImage-6.0 \
        -lvtkDICOMParser-6.0 \
        -lvtkIOCore-6.0 \
        -lvtkmetaio-6.0 \
        -lvtkIOXMLParser-6.0 \
        -lvtkRenderingFreeType-6.0 \
        -lvtkftgl-6.0 \
        -lvtkRenderingOpenGL-6.0 \
        -lvtkImagingHybrid-6.0 \
        -lvtkDomainsChemistry-6.0 \
        -lvtkIOXML-6.0 \
        -lvtkIOGeometry-6.0 \
        -lvtkjsoncpp-6.0 \
        -lvtkexoIIc-6.0 \
        -lvtkNetCDF-6.0 \
        -lvtkNetCDF_cxx-6.0 \
        -lvtkFiltersAMR-6.0 \
        -lvtkParallelCore-6.0 \
        -lvtkIOLegacy-6.0 \
        -lvtkFiltersFlowPaths-6.0 \
        -lvtkFiltersGeneric-6.0 \
        -lvtkFiltersHybrid-6.0 \
        -lvtkImagingSources-6.0 \
        -lvtkFiltersHyperTree-6.0 \
        -lvtkFiltersImaging-6.0 \
        -lvtkImagingGeneral-6.0 \
        -lvtkFiltersModeling-6.0 \
        -lvtkFiltersParallel-6.0 \
        -lvtkFiltersParallelImaging-6.0 \
        -lvtkFiltersProgrammable-6.0 \
        -lvtkFiltersSelection-6.0 \
        -lvtkFiltersTexture-6.0 \
        -lvtkFiltersVerdict-6.0 \
        -lvtkGeovisCore-6.0 \
        -lvtkInfovisLayout-6.0 \
        -lvtkInteractionStyle-6.0 \
        -lvtkInteractionWidgets-6.0 \
        -lvtkRenderingAnnotation-6.0 \
        -lvtkImagingColor-6.0 \
        -lvtkRenderingVolume-6.0 \
        -lvtkViewsCore-6.0 \
        -lvtkproj4-6.0 \
        -lvtkGUISupportQt-6.0 \
        -lvtkGUISupportQtOpenGL-6.0 \
        -lvtkImagingMath-6.0 \
        -lvtkImagingMorphological-6.0 \
        -lvtkImagingStatistics-6.0 \
        -lvtkImagingStencil-6.0 \
        -lvtkInteractionImage-6.0 \
        -lvtkIOAMR-6.0 \
        -lvtkIOEnSight-6.0 \
        -lvtkIOExodus-6.0 \
        -lvtkIOExport-6.0 \
        -lvtkRenderingGL2PS-6.0 \
        -lvtkIOImport-6.0 \
        -lvtkIOInfovis-6.0 \
        -lvtkIOLSDyna-6.0 \
        -lvtkIOMINC-6.0 \
        -lvtkIOMovie-6.0 \
        -lvtkIONetCDF-6.0 \
        -lvtkIOParallel-6.0 \
        -lvtkIOPLY-6.0 \
        -lvtkIOSQL-6.0 \
        -lvtksqlite-6.0 \
        -lvtkIOVideo-6.0 \
        -lvtkRenderingFreeTypeOpenGL-6.0 \
        -lvtkRenderingHybridOpenGL-6.0 \
        -lvtkRenderingImage-6.0 \
        -lvtkRenderingLabel-6.0 \
        -lvtkRenderingLOD-6.0 \
        -lvtkRenderingVolumeAMR-6.0 \
        -lvtkRenderingVolumeOpenGL-6.0 \
        -lvtkViewsContext2D-6.0 \
        -lvtkViewsGeovis-6.0 \
        -lvtkViewsInfovis-6.0
}
else:win32{
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Imaging\Core
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Imaging\Core
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\ExecutionModel
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\ExecutionModel
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\DataModel
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\DataModel
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\Math
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\Math
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\Core
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\Core
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Utilities\KWSys
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Utilities\KWSys
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\Misc
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\Misc
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\System
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\System
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\Transforms
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\Transforms
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Rendering\Core
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Rendering\Core
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Filters\Extraction
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Filters\Extraction
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Filters\Core
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Filters\Core
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Filters\General
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Filters\General
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Common\ComputationalGeometry
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Common\ComputationalGeometry
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Filters\Statistics
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Filters\Statistics
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Imaging\Fourier
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Imaging\Fourier
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\ThirdParty\alglib
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\ThirdParty\alglib
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Filters\Geometry
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Filters\Geometry
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Filters\Sources
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Filters\Sources
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\Rendering\Image
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\Rendering\Image
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\IO\Core
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\IO\Core
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\IO\Geometry
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\IO\Geometry
    INCLUDEPATH += E:\Program\ThirdParty\VTK-6.1.0\IO\Legacy
    INCLUDEPATH += E:\Program\ThirdParty\libvtk_shared_md\IO\Legacy

    LIBS += \
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkalglib-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkChartsCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonColor-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonComputationalGeometry-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonDataModel-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonExecutionModel-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonMath-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonMisc-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonSystem-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkCommonTransforms-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkDICOMParser-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkDomainsChemistry-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkexoIIc-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkexpat-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersAMR-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersExtraction-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersFlowPaths-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersGeneral-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersGeneric-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersGeometry-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersHybrid-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersHyperTree-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersImaging-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersModeling-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersParallel-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersParallelImaging-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersProgrammable-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersSelection-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersSMP-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersSources-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersStatistics-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersTexture-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkFiltersVerdict-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkfreetype-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkftgl-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkGeovisCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkgl2ps-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkhdf5-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkhdf5_hl-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingColor-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingFourier-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingGeneral-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingHybrid-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingMath-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingMorphological-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingSources-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingStatistics-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkImagingStencil-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkInfovisCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkInfovisLayout-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkInteractionImage-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkInteractionStyle-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkInteractionWidgets-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOAMR-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOEnSight-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOExodus-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOExport-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOGeometry-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOImage-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOImport-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOInfovis-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOLegacy-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOLSDyna-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOMINC-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOMovie-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIONetCDF-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOParallel-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOPLY-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOSQL-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOVideo-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOXML-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkIOXMLParser-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkjpeg-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkjsoncpp-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtklibxml2-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkmetaio-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkNetCDF-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkNetCDF_cxx-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkoggtheora-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkParallelCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkpng-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkproj4-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingAnnotation-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingContext2D-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingFreeType-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingFreeTypeOpenGL-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingGL2PS-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingImage-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingLabel-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingLIC-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingLOD-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingOpenGL-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingVolume-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingVolumeAMR-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkRenderingVolumeOpenGL-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtksqlite-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtksys-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtktiff-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkverdict-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkViewsContext2D-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkViewsCore-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkViewsGeovis-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkViewsInfovis-6.1.lib\
            E:/Program/ThirdParty/libvtk_shared_md/lib/Release/vtkzlib-6.1.lib
}
