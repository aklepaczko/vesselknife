/*
 * VesselKnife tree format to VTK data format converter
 *
 * Copyright 2015 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../shared/tree.h"
#include <string>

#include <vtkGenericDataObjectWriter.h>
#include <vtkStructuredGrid.h>
#include <vtkPointData.h>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkFieldData.h>
#include <vtkDataSetAttributes.h>

//=============================================================================
// converter
bool export2vtk(Tree* tree, const char* filename)
{
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    unsigned int npoints = tree->nodeCount();
    vtkSmartPointer<vtkDoubleArray> array = vtkSmartPointer<vtkDoubleArray>::New();
    array->SetNumberOfComponents(1);
    array->SetNumberOfTuples(npoints);
    array->SetName("radius");

    for(unsigned int ndi = 0; ndi < npoints; ndi++)
    {
        NodeIn3D nd = tree->node(ndi);
        points->InsertNextPoint(nd.x, nd.y, nd.z);
        array->SetTuple1(ndi, nd.radius);
    }

    vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
    unsigned int nbranches = tree->count();
    for(unsigned int bi = 0; bi < nbranches; bi++)
    {
        unsigned int nnodes = tree->count(bi);
        cells->InsertNextCell(nnodes);
        for(unsigned int ni = 0; ni < nnodes; ni++)
        {
            cells->InsertCellPoint(tree->nodeIndex(bi, ni));
        }
    }

    vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();
    poly->SetPoints(points);
    poly->SetPolys(cells);
    poly->GetPointData()->AddArray(array);
    poly->GetPointData()->SetActiveScalars("radius");

    vtkSmartPointer<vtkGenericDataObjectWriter> writer = vtkSmartPointer<vtkGenericDataObjectWriter>::New();

#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(poly);
#else
    writer->SetInputData(poly);
#endif

    writer->SetFileName(filename);
    writer->Update();

    //always true as VTK has no notification on IO completion
    return true;
}

//=============================================================================
// Main function
int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        printf("Usage: %s input output\n", argv[0]);
        printf("Converts vessel tree models saved in VesselKnife format to VTK data foemat\n");
        printf("Version 2015.03.15 by Piotr M. Szczypinski\n");
        return 1;
    }
    Tree tree;
    if(!tree.load(argv[1])) return 2;
    if(!export2vtk(&tree, argv[2])) return 3;
    return 0;
}
