#ifndef IMAGEFILTERS2_H
#define IMAGEFILTERS2_H


#include <itkImage.h>
//#include <itkImageFileReader.h>
//#include <itkImageFileWriter.h>
//#include <itkImageDuplicator.h>
//#include <itkDiscreteGaussianImageFilter.h>
//#include <itkRescaleIntensityImageFilter.h>
//#include <itkConnectedThresholdImageFilter.h>
//#include <itkHessianRecursiveGaussianImageFilter.h>
//#include <itkHessian3DToVesselnessMeasureImageFilter.h>
//#include <itkMinimumMaximumImageCalculator.h>
//#include "./thirdparty/itkBinaryThinningImageFilter3D.h"
//#include <itkWhiteTopHatImageFilter.h>
//#include <itkBlackTopHatImageFilter.h>
//#include <itkGrayscaleDilateImageFilter.h>
//#include <itkGrayscaleErodeImageFilter.h>
//#include <itkBinaryBallStructuringElement.h>
//#include <itkBinaryFillholeImageFilter.h>
//#include <itkBinaryThresholdImageFilter.h>

#include "itkScalarChanAndVeseDenseLevelSetImageFilter.h"
#include "itkScalarChanAndVeseLevelSetFunctionData.h"
#include "itkConstrainedRegionBasedLevelSetFunctionSharedData.h"
#include "itkFastMarchingImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkAtanRegularizedHeavisideStepFunction.h"


template <class T> class ImageFilters2
{
public:
    typedef itk::Image< T, 3 > ImageType;



//Based on example from ITK wiki:
//ITK/Examples/WishList/Segmentation/SinglephaseChanAndVeseDenseFieldLevelSetSegmentation

    static typename ImageType::Pointer levelSetChanVese(typename ImageType::Pointer featureImage,
                                                        typename ImageType::IndexType seedPosition,
                                                        const unsigned int nb_iteration,
                                                        const double initialDistance = 1,
                                                        const double rms = 0.0,
                                                        const double epsilon = 1.0,
                                                        const double curvature_weight = 0.0,
                                                        const double area_weight = 0.0,
                                                        const double reinitialization_weight = 0.0,
                                                        const double volume_weight = 0.0,
                                                        const double volume = 0.0,
                                                        const double l1 = 1.0,
                                                        const double l2 = 1.0
                                                        )
    {
//        typedef typename itk::DiscreteGaussianImageFilter< ImageType, ImageType > F;
//        typedef typename F::Pointer P;
//        P filter = F::New();
//        filter->SetInput(input_image);
//        filter->SetVariance(sigma*sigma);
//        filter->Update();
//        return filter->GetOutput();






//        double rms = 0.;
//        double epsilon = 1.;
//        double curvature_weight = 0.;
//        double area_weight = 0.;
//        double reinitialization_weight = 0.;
//        double volume_weight = 0.;
//        double volume = 0.;
//        double l1 = 1.;
//        double l2 = 1.;

        //
        //  We now define the image type using a particular pixel type and
        //  dimension. In this case the \code{float} type is used for the pixels
        //  due to the requirements of the smoothing filter.
        //
        //const unsigned int Dimension = 3;
        //typedef float T;
        //typedef itk::Image< T, 3 > ImageType;

        typedef typename itk::ScalarChanAndVeseLevelSetFunctionData< ImageType,
          ImageType > DataHelperType;

        typedef typename itk::ConstrainedRegionBasedLevelSetFunctionSharedData<
          ImageType, ImageType, DataHelperType > SharedDataHelperType;

        typedef typename itk::ScalarChanAndVeseLevelSetFunction< ImageType,
          ImageType, SharedDataHelperType > LevelSetFunctionType;


        //  We declare now the type of the numerically discretized Step and Delta functions that
        //  will be used in the level-set computations for foreground and background regions
        //
        typedef typename itk::AtanRegularizedHeavisideStepFunction< T, T >  DomainFunctionType;

        typename DomainFunctionType::Pointer domainFunction = DomainFunctionType::New();
        domainFunction->SetEpsilon( epsilon );

        // We instantiate reader and writer types in the following lines.
        //
//        typedef itk::ImageFileReader< ImageType > ReaderType;
//        typedef itk::ImageFileWriter< ImageType > WriterType;

//        ReaderType::Pointer reader = ReaderType::New();
//        WriterType::Pointer writer = WriterType::New();

//        reader->SetFileName( argv[1] );
//        reader->Update();

//        writer->SetFileName( argv[2] );

        //ImageType::Pointer featureImage = reader->GetOutput();

        //  We declare now the type of the FastMarchingImageFilter that
        //  will be used to generate the initial level set in the form of a distance
        //  map.
        //
        typedef  itk::FastMarchingImageFilter<
          ImageType,
          ImageType >    FastMarchingFilterType;

        typename FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();


        //  The FastMarchingImageFilter requires the user to provide a seed
        //  point from which the level set will be generated. The user can actually
        //  pass not only one seed point but a set of them. Note the the
        //  FastMarchingImageFilter is used here only as a helper in the
        //  determination of an initial level set. We could have used the
        //  \doxygen{DanielssonDistanceMapImageFilter} in the same way.
        //
        //  The seeds are passed stored in a container. The type of this
        //  container is defined as \code{NodeContainer} among the
        //  FastMarchingImageFilter traits.
        //
        typedef typename FastMarchingFilterType::NodeContainer  NodeContainer;
        typedef typename FastMarchingFilterType::NodeType       NodeType;

        typename NodeContainer::Pointer seeds = NodeContainer::New();
//        typename ImageType::IndexType  seedPosition;
//        seedPosition[0] = seed[0];
//        seedPosition[1] = seed[1];
//        seedPosition[2] = seed[2];

        //const double initialDistance = atof( argv[5] );

        NodeType node;

        const double seedValue = - initialDistance;

        node.SetValue( seedValue );
        node.SetIndex( seedPosition );

        //  The list of nodes is initialized and then every node is inserted using
        //  the \code{InsertElement()}.
        //
        seeds->Initialize();
        seeds->InsertElement( 0, node );


        //  The set of seed nodes is passed now to the
        //  FastMarchingImageFilter with the method
        //  \code{SetTrialPoints()}.
        //
        fastMarching->SetTrialPoints(  seeds  );


        //  Since the FastMarchingImageFilter is used here just as a
        //  Distance Map generator. It does not require a speed image as input.
        //  Instead the constant value $1.0$ is passed using the
        //  \code{SetSpeedConstant()} method.
        //
        fastMarching->SetSpeedConstant( 1.0 );

        //  The FastMarchingImageFilter requires the user to specify the
        //  size of the image to be produced as output. This is done using the
        //  \code{SetOutputSize()}. Note that the size is obtained here from the
        //  output image of the smoothing filter. The size of this image is valid
        //  only after the \code{Update()} methods of this filter has been called
        //  directly or indirectly.
        //
        fastMarching->SetOutputSize(featureImage->GetBufferedRegion().GetSize());
        fastMarching->Update();

        //  We declare now the type of the ScalarChanAndVeseDenseLevelSetImageFilter that
        //  will be used to generate a segmentation.
        //

        typedef typename itk::ScalarChanAndVeseDenseLevelSetImageFilter< ImageType, ImageType, ImageType, LevelSetFunctionType, SharedDataHelperType > MultiLevelSetType;
        typename MultiLevelSetType::Pointer levelSetFilter = MultiLevelSetType::New();

        //  We set the function count to 1 since a single level-set is being evolved.
        //
        levelSetFilter->SetFunctionCount( 1 );

        //  Set the feature image and initial level-set image as output of the
        //  fast marching image filter.
        //
        levelSetFilter->SetFeatureImage( featureImage );
        levelSetFilter->SetLevelSet( 0, fastMarching->GetOutput() );

        //  Once activiated the level set evolution will stop if the convergence
        //  criteria or if the maximum number of iterations is reached.  The
        //  convergence criteria is defined in terms of the root mean squared (RMS)
        //  change in the level set function. The evolution is said to have
        //  converged if the RMS change is below a user specified threshold.  In a
        //  real application is desirable to couple the evolution of the zero set
        //  to a visualization module allowing the user to follow the evolution of
        //  the zero set. With this feedback, the user may decide when to stop the
        //  algorithm before the zero set leaks through the regions of low gradient
        //  in the contour of the anatomical structure to be segmented.
        //
        levelSetFilter->SetNumberOfIterations( nb_iteration );
        levelSetFilter->SetMaximumRMSError( rms );

        //  Often, in real applications, images have different pixel resolutions. In such
        //  cases, it is best to use the native spacings to compute derivatives etc rather
        //  than sampling the images.
        //
        levelSetFilter->SetUseImageSpacing( 1 );

        //  For large images, we may want to compute the level-set over the initial supplied
        //  level-set image. This saves a lot of memory.
        //
        levelSetFilter->SetInPlace( false );

        //  For the level set with phase 0, set different parameters and weights. This may
        //  to be set in a loop for the case of multiple level-sets evolving simultaneously.
        //
        levelSetFilter->GetDifferenceFunction(0)->SetDomainFunction( domainFunction );
        levelSetFilter->GetDifferenceFunction(0)->SetCurvatureWeight( curvature_weight );
        levelSetFilter->GetDifferenceFunction(0)->SetAreaWeight( area_weight );
        levelSetFilter->GetDifferenceFunction(0)->SetReinitializationSmoothingWeight( reinitialization_weight );
        levelSetFilter->GetDifferenceFunction(0)->SetVolumeMatchingWeight( volume_weight );
        levelSetFilter->GetDifferenceFunction(0)->SetVolume( volume );
        levelSetFilter->GetDifferenceFunction(0)->SetLambda1( l1 );
        levelSetFilter->GetDifferenceFunction(0)->SetLambda2( l2 );

        levelSetFilter->Update();

        return levelSetFilter->GetOutput();

//        writer->SetInput( levelSetFilter->GetOutput() );

//        try
//          {
//          writer->Update();
//          }
//        catch( itk::ExceptionObject & excep )
//          {
//          std::cerr << "Exception caught !" << std::endl;
//          std::cerr << excep << std::endl;
//          return -1;
//          }

//        return EXIT_SUCCESS;
    }
};


#endif // IMAGEFILTERS2_H
