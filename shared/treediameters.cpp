#include "treediameters.h"

using namespace alglib;


TreeDiameters::TreeDiameters(Tree *tree, Byte3DImage *image, double gamma, int divider, double rho)
{
    this->tree = tree;
    this->image = image;
    this->gamma = gamma;
    this->rho = rho;
    this->divider = divider;
    w.setlength(3);
    cvm.setlength(3, 3);
    u.setlength(3, 3);
    vt.setlength(3, 3);

    if(divider >= 0)
    {
        Sphear sphear(divider);
        radially_spaced_directions_number = sphear.verticesNumber();
        radially_spaced_directions.setlength(radially_spaced_directions_number, 3);
        double vertex[3];
        for(unsigned int direction = 0; direction < radially_spaced_directions_number; direction++)
        {
            sphear.vertex(direction, vertex);
            radially_spaced_directions(direction, 0) = vertex[0] / image->voxelsize[0];
            radially_spaced_directions(direction, 1) = vertex[1] / image->voxelsize[1];
            radially_spaced_directions(direction, 2) = vertex[2] / image->voxelsize[2];
        }
    }
    else
    {
        Sphear sphear(-divider-1);
        radially_spaced_directions_number = sphear.trianglesNumber();
        radially_spaced_directions.setlength(radially_spaced_directions_number, 3);

        unsigned int* tr = new unsigned int[radially_spaced_directions_number*3];
        sphear.triangles(tr);
        double vertex[3];

        for(unsigned int direction = 0; direction < radially_spaced_directions_number; direction++)
        {
            vertex[0] = 0;
            vertex[1] = 0;
            vertex[2] = 0;
            for(unsigned int v = 0; v < 3; v++)
            {
                double vertext[3];
                sphear.vertex(tr[direction*3 + v], vertext);
                vertex[0] += vertext[0];
                vertex[1] += vertext[1];
                vertex[2] += vertext[2];
            }
            //double dist = sqrt(vertex[0]*vertex[0]+vertex[1]*vertex[1]+vertex[2]*vertex[2]);
            radially_spaced_directions(direction, 0) = vertex[0] / image->voxelsize[0];
            radially_spaced_directions(direction, 1) = vertex[1] / image->voxelsize[1];
            radially_spaced_directions(direction, 2) = vertex[2] / image->voxelsize[2];
        }
        delete[] tr;
    }
    pts.setlength(radially_spaced_directions_number, 3);
}

Tree TreeDiameters::getResult()
{
    Tree tree = *this->tree;
    int nodecount = tree.nodeCount();
    for(int i = 0; i < nodecount; i++)
    {
        double center[3];
        center[0] = tree.node(i).x / image->voxelsize[0] + 0.5;
        center[1] = tree.node(i).y / image->voxelsize[1] + 0.5;
        center[2] = tree.node(i).z / image->voxelsize[2] + 0.5;
        findVectors(center);
        tree.setDiameter(i, diameterFromPca());
    }
    return tree;
}



/*
Tree TreeDiameters::getResult()
{
    Tree tree(0.0001);// = *this->tree;
    int nodecount = this->tree->nodeCount();

    //tree.setDiameter(nodecount*2/3, 0.3); //test

    //for(int i = 0; i < nodecount; i++)
    int i = 8; //test
    i = i % nodecount;


    {
        double center[3];
        center[0] = this->tree->node(i).x / image->voxelsize[0] + 0.5;
        center[1] = this->tree->node(i).y / image->voxelsize[1] + 0.5;
        center[2] = this->tree->node(i).z / image->voxelsize[2] + 0.5;
        findVectors(center);

        NodeIn3D cnode;
        cnode.x = this->tree->node(i).x;
        cnode.y = this->tree->node(i).y;
        cnode.z = this->tree->node(i).z;
        cnode.radius = 0.2;
        cnode.connections = 1;

        for(int vv = 0; vv < radially_spaced_directions_number; vv++)
        {
            NodeIn3D newnode;

            newnode.x = pts(vv, 0) + cnode.x;
            newnode.y = pts(vv, 1) + cnode.y;
            newnode.z = pts(vv, 2) + cnode.z;
            newnode.radius = 0.1;
            newnode.connections = 1;

            std::vector<NodeIn3D> newbranch;
            newbranch.push_back(cnode);
            newbranch.push_back(newnode);

            tree.addBranch(newbranch);

            printf("%i %f %f %f\n", vv, pts(vv, 0), pts(vv, 1), pts(vv, 2));
            fflush(stdout);

        }

        this->tree->setDiameter(i, diameterFromPca());
    }
    return tree;
}
*/



double TreeDiameters::diameterFromPca(void)
{
    covm(pts, radially_spaced_directions_number, 3, cvm);


    /*
    for(int col = 0; col < 3; col++)
        for(int row = 0; row < 3; row++)
            cvm(row, col) *= image->voxelsize[col];
*/


    if(rmatrixsvd(cvm, 3, 3, 0, 0, 2, w, u, vt))
    {
        double lamb2, lamb3;
        if(w(2) >= w(1) && w(2) >= w(0)) {lamb2 = w(1); lamb3 = w(0);}
        else if(w(0) >= w(2) && w(0) >= w(1)) {lamb2 = w(1); lamb3 = w(2);}
        else {lamb2 = w(2); lamb3 = w(0);}
        double radius = -gamma/2.0;
        radius = (pow(lamb2, radius) + pow(lamb3, radius))/2.0;
        radius = rho * pow(radius, -1/gamma);
        return radius;
/*
        double www;
        www = w(0) - w(2);
        if(www <= 0) return sqrt(w(1)+w(2));
        if((w(1)-w(2))/www <= firstlevel) return sqrt(w(1)+w(2));
        if((w(1)-w(2))/www >= secondlevel) return sqrt(2.0*w(2));
        www = ((w(1)-w(2))/www - firstlevel)/(secondlevel - firstlevel);
        return www*sqrt(2.0*w(2)) + (1.0 - www)*sqrt(w(1)+w(2));
*/
    }
    else
        return 0;
}

void TreeDiameters::findVectors(double center[3])
{
    unsigned int node[3];
    node[0] = center[0];
    node[1] = center[1];
    node[2] = center[2];
    if(node[0] >= image->size[0] || node[1] >= image->size[1] || node[2] >= image->size[2])
    {
        for(unsigned int direction_index = 0; direction_index < radially_spaced_directions_number; direction_index++)
            for(int dd = 0; dd < 3; dd++)
                pts(direction_index, dd) = 0.0;
        return;
    }

    unsigned char* ppix = image->pixel(node);
    if(*ppix)
    {
        for(unsigned int direction_index = 0; direction_index < radially_spaced_directions_number; direction_index++)
        {
            findBorderIntersection(center, direction_index);
        }
    }
    else
    {
        for(unsigned int direction_index = 0; direction_index < radially_spaced_directions_number; direction_index++)
        {
            for(int dd = 0; dd < 3; dd++)
                pts(direction_index, dd) = 0.0;
        }
    }
}

/**
 * @brief TreeDiameters::findBorderIntersection
 * @param center center (node) location in voxel space
 * @param direction_index index of direction indicating pts() vector
 */
void TreeDiameters::findBorderIntersection(double center[3], unsigned int direction_index)
{
    const double almostzero = 0.000001;
    //double ctr[3]; // center location in voxel space
    double tmp[3]; // temporary distance
    double shift[3]; // shift for x, y, and z step
    double dist[3]; // distance for x, y, and z step
    double dir[3]; // direction in voxel space
    int d;

    // Initialization
    for(d = 0; d < 3; d++)
    {
        dist[d] = -1.0;
        dir[d] = radially_spaced_directions(direction_index, d); // image->voxelsize[d];
        shift[d] = center[d];

        if(dir[d] > 0) shift[d] = ceil(shift[d]);
        else shift[d] = floor(shift[d]);

        if(fabs(dir[d]) > almostzero)
        {
            tmp[d] = shift[d]-center[d];
            dist[d] = 0.0;
            for(int dd = 0; dd < 3; dd++)
            {
                if(dd != d) tmp[dd] = tmp[d]*dir[dd]/dir[d];
                dist[d] += tmp[dd]*tmp[dd];
            }
        }
    }

    // Computation of plane and line intersection point with the subsequent (from center point) planes
    do
    {
        unsigned int node[3]; //center location in voxel index space

        for(d = 0; d < 3; d++)
        {
            if( dist[d] >= 0.0 &&
                (dist[d] <= dist[0] || dist[0] < 0) &&
                (dist[d] <= dist[1] || dist[1] < 0) &&
                (dist[d] <= dist[2] || dist[2] < 0) )

            {
                tmp[d] = shift[d]-center[d];
                if(dir[d] > 0) node[d] = shift[d] + 1.0;
                else node[d] = shift[d];
                for(int dd = 0; dd < 3; dd++)
                {
                    if(dd != d)
                    {
                        tmp[dd] = tmp[d]*dir[dd]/dir[d];
                        node[dd] = tmp[dd] + center[dd] + 0.5;
                    }
                }

                if(node[0] >= image->size[0] || node[1] >= image->size[1] || node[2] >= image->size[2])
                {
                    for(int dd = 0; dd < 3; dd++)
                        pts(direction_index, dd) = tmp[dd]*image->voxelsize[dd];
                    return;
                }
                unsigned char* ppix = image->pixel(node);
                if(*ppix)
                {
                    if(dir[d] > 0) shift[d] += 1.0;
                    else shift[d] -= 1.0;

                    tmp[d] = shift[d]-center[d];
                    dist[d] = 0.0;
                    for(int dd = 0; dd < 3; dd++)
                    {
                        if(dd != d) tmp[dd] = tmp[d]*dir[dd]/dir[d];
                        dist[d] += tmp[dd]*tmp[dd];
                    }
                }
                else
                {
                    for(int dd = 0; dd < 3; dd++)
                        pts(direction_index, dd) = tmp[dd]*image->voxelsize[dd];
                    return;
                }
                break;
            }
        }
        if(d >= 3)
        {
            for(int dd = 0; dd < 3; dd++)
                pts(direction_index, dd) = 0.0;
            return;
        }
    }while(true);
}

