#include "treeextend.h"

#ifdef _WIN32
#define lround(x) int(x+0.5)
#endif

TreeExtend::TreeExtend(Tree *tree, ImageType::Pointer image, double mingoal, int maxnodes, double sigma, bool normalize, double alfa1, double alfa2)
{
    m_image = image;
    m_tree = tree;
    m_sigma = sigma;
    m_mingoal = mingoal;
    m_alfa1 = alfa1;
    m_alfa2 = alfa2;
    m_normalize = normalize;
    m_maxnodes = maxnodes;
    spacing = image->GetSpacing();
}


Tree TreeExtend::getResult()
{
    computeTensorImage();
    return traverseTree();
}


void TreeExtend::computeTensorImage()
{
    filter = HFiltr::New();
    filter->SetInput(m_image);
    filter->SetSigma(m_sigma);
    filter->SetNormalizeAcrossScale(m_normalize);
    filter->Update();
    tensorimage = filter->GetOutput();
}


Tree TreeExtend::traverseTree()
{
    Tree tree = *m_tree;
    int bn = m_tree->count();
    int nn = tree.nodeCount();
    for(int n = 0; n < nn; n++)
    {
        NodeIn3D nnn = tree.node(n);
        nnn.x /= spacing[0];
        nnn.y /= spacing[1];
        nnn.z /= spacing[2];
        tree.setNode(nnn, n);
    }

    for(int b = 0; b < bn; b++)
    {
        std::vector<NodeIn3D> branch = m_tree->branch(b);
        int nn = branch.size();
        if(nn < 2) continue;
        if(branch[nn-1].connections == 1)
        {
            NodeIn3D bk, bg;
            bk = branch[nn-1];
            bk.x /= spacing[0];
            bk.y /= spacing[1];
            bk.z /= spacing[2];
            bg = branch[nn-2];
            bg.x /= spacing[0];
            bg.y /= spacing[1];
            bg.z /= spacing[2];
            std::vector<NodeIn3D> newbranch = extendTip(bk, bg, &tree);
            if(newbranch.size() > 1) tree.addBranch(newbranch, false);
//            unsigned int ndcount = newbranch.size();
//            for(unsigned int nd = 0; nd < ndcount; nd++)
//            {
//                newbranch[nd].x *= spacing[0];
//                newbranch[nd].y *= spacing[1];
//                newbranch[nd].z *= spacing[2];
//            }
//            tree.expandBranch(b, nn-1, newbranch);
        }
        if(branch[0].connections == 1)
        {
            NodeIn3D bk, bg;
            bk = branch[0];
            bk.x /= spacing[0];
            bk.y /= spacing[1];
            bk.z /= spacing[2];
            bg = branch[1];
            bg.x /= spacing[0];
            bg.y /= spacing[1];
            bg.z /= spacing[2];

            std::vector<NodeIn3D> newbranch = extendTip(bk, bg, &tree);
            if(newbranch.size() > 1) tree.addBranch(newbranch, false);
//            unsigned int ndcount = newbranch.size();
//            for(unsigned int nd = 0; nd < ndcount; nd++)
//            {
//                newbranch[nd].x *= spacing[0];
//                newbranch[nd].y *= spacing[1];
//                newbranch[nd].z *= spacing[2];
//            }

        }
    }

    tree.correctConnectivity();
    nn = tree.nodeCount();
    for(int n = 0; n < nn; n++)
    {
        NodeIn3D nnn = tree.node(n);
        nnn.x *= spacing[0];
        nnn.y *= spacing[1];
        nnn.z *= spacing[2];
        tree.setNode(nnn, n);
    }
    return tree;
}


/*
Tree TreeExtend::traverseTree()
{
    Tree tree = *m_tree;
    int bn = m_tree->count();
    int nn = tree.nodeCount();


    for(int n = 0; n < nn; n++)
    {
        NodeIn3D nnn = tree.node(n);
        nnn.x /= spacing[0];
        nnn.y /= spacing[1];
        nnn.z /= spacing[2];
        tree.setNode(nnn, n);
    }


    for(int b = 0; b < bn; b++)
    {
        std::vector<NodeIn3D> branch = m_tree->branch(b);
        int nn = branch.size();
        if(nn < 2) continue;

        if(branch[nn-1].connections == 1)
        {
            NodeIn3D bk, bg;
            bk = branch[nn-1];
            bg = branch[nn-2];
//            std::vector<NodeIn3D> newbranch = extendTip(bk, bg, &tree);
            std::vector<NodeIn3D> newbranch = extendTip(bk, bg);
            tree.expandBranch(b, nn-1, newbranch);
        }
        if(branch[0].connections == 1)
        {
            NodeIn3D bk, bg;
            bk = branch[0];
            bg = branch[1];
//            std::vector<NodeIn3D> newbranch = extendTip(bk, bg, &tree);
            std::vector<NodeIn3D> newbranch = extendTip(bk, bg);
            tree.expandBranch(b, 0, newbranch);
        }
    }

    for(int n = 0; n < nn; n++)
    {
        NodeIn3D nnn = tree.node(n);
        nnn.x *= spacing[0];
        nnn.y *= spacing[1];
        nnn.z *= spacing[2];
        tree.setNode(nnn, n);
    }


    return tree;
}
*/

std::vector<NodeIn3D> TreeExtend::extendTip(NodeIn3D& tip, NodeIn3D& neighbor, Tree* tree)
{
    Coordinates3d current, previous;
    current = (Coordinates3d)tip;
    previous = (Coordinates3d)neighbor;
    std::vector<NodeIn3D> toret;

    toret.push_back(tip);

    for(int nad = 0; nad < m_maxnodes; nad++)
    {
        double shifd[3];
        int shift[3];
        shifd[0] = current.x - previous.x;
        shifd[1] = current.y - previous.y;
        shifd[2] = current.z - previous.z;
        double length = sqrt(shifd[0]*shifd[0] + shifd[1]*shifd[1] + shifd[2]*shifd[2]);
        if(length <= 0.0) break;

        shift[0] = 1.8*shifd[0]/length;
        shift[1] = 1.8*shifd[1]/length;
        shift[2] = 1.8*shifd[2]/length;

        double maxvalue = 0;
        int maxprobe = -1;

        for(int probe = 0; probe < 27; probe++)
        {
            if(dxa[probe][0] == shift[0] && dxa[probe][1] == shift[1] && dxa[probe][2] == shift[2]) continue;
            TensorImageType::IndexType probee;
            probee[0] = lround(current.x) + shift[0] - dxa[probe][0];
            probee[1] = lround(current.y) + shift[1] - dxa[probe][1];
            probee[2] = lround(current.z) + shift[2] - dxa[probe][2];

            TensorImageType::SizeType sizeten;

            sizeten = tensorimage->GetLargestPossibleRegion().GetSize();

            if( probee[0] < 0 || probee[0] >= sizeten[0] ||
                probee[1] < 0 || probee[1] >= sizeten[1] ||
                probee[2] < 0 || probee[2] >= sizeten[2]) return toret;

            double thrvp[3];
            thrvp[0] = current.x - previous.x;
            thrvp[1] = current.y - previous.y;
            thrvp[2] = current.z - previous.z;

            double thrvn[3];
            thrvn[0] = probee[0] - current.x;
            thrvn[1] = probee[1] - current.y;
            thrvn[2] = probee[2] - current.z;

//            printf("%i) ", probe);

            double g = computeGoalFunction(probee, thrvp, thrvn);
            if(maxvalue < g)
            {
                maxvalue = g;
                maxprobe = probe;
            }
        }

//        if(maxvalue >= m_mingoal && maxprobe >= 0)  printf("+++++++++++++ %i %f\n", maxprobe, maxvalue);
//        else printf("----------- %i %f\n", maxprobe, maxvalue);

        if(maxvalue >= m_mingoal && maxprobe >= 0)
        {
            NodeIn3D n2push;
            n2push.connections = 0;
            n2push.radius = 0;
            n2push.x = lround(current.x) + shift[0] - dxa[maxprobe][0];
            n2push.y = lround(current.y) + shift[1] - dxa[maxprobe][1];
            n2push.z = lround(current.z) + shift[2] - dxa[maxprobe][2];

            if(tree != NULL)
            {
                unsigned int count = tree->nodeCount();
                for(unsigned int i = 0; i < count; i++)
                {
                    NodeIn3D node = tree->node(i);
                    double xx = node.x-n2push.x;
                    double yy = node.y-n2push.y;
                    double zz = node.z-n2push.z;
                    double dd = xx*xx+yy*yy+zz*zz;
                    if(dd < 0.2)
                    {
                        toret.push_back(n2push);
                        return toret;
                    }
                }
            }

            toret.push_back(n2push);
            previous = current;
            current = (Coordinates3d)(toret.back());
        }
        else break;
    }
    return toret;
}

double TreeExtend::vesselness(TensorImageType::PixelType::EigenValuesArrayType& eval)
{
    double normalizeValue = vnl_math_min(-1.0 * eval[1], -1.0 * eval[0]);
    if ( normalizeValue > 0 )
    {
        double lineMeasure;
        if ( eval[2] <= 0 )
        {
            lineMeasure = vcl_exp( -0.5 * vnl_math_sqr( eval[2] / ( m_alfa1 * normalizeValue ) ) );
            //lineMeasure = ( -0.5 * vnl_math_sqr( eval[2] / ( m_alfa1 * normalizeValue ) ) );
        }
        else
        {
            lineMeasure = vcl_exp( -0.5 * vnl_math_sqr( eval[2] / ( m_alfa2 * normalizeValue ) ) );
            //lineMeasure = ( -0.5 * vnl_math_sqr( eval[2] / ( m_alfa2 * normalizeValue ) ) );
        }

        lineMeasure *= normalizeValue;
        //lineMeasure += vcl_log(normalizeValue);
        return lineMeasure;
    }
    else return 0;
}


double TreeExtend::computeGoalFunction(TensorImageType::IndexType& probee, double* thrvp, double* thrvn)
{
    TensorImageType::PixelType tensor = tensorimage->GetPixel(probee);
    TensorImageType::PixelType::EigenValuesArrayType eval;
    TensorImageType::PixelType::EigenVectorsMatrixType evec;
    tensor.ComputeEigenAnalysis(eval, evec);



    double normp = sqrt(thrvp[0]*thrvp[0]+thrvp[1]*thrvp[1]+thrvp[2]*thrvp[2]);
    if(normp < 0.0001) return 0.0;
    double normn = sqrt(thrvn[0]*thrvn[0]+thrvn[1]*thrvn[1]+thrvn[2]*thrvn[2]);
    if(normn < 0.0001) return 0.0;
    double normv = sqrt(evec[2][0]*evec[2][0]+evec[2][1]*evec[2][1]+evec[2][2]*evec[2][2]);
    if(normv < 0.0001) return 0.0;


    double vess = vesselness(eval);
    vess = atan(vess);

    double alignpn = (thrvp[0]*thrvn[0] + thrvp[1]*thrvn[1] + thrvp[2]*thrvn[2]) / (normn * normp);
    double alignv = (evec[2][0]*thrvn[0] + evec[2][1]*thrvn[1] + evec[2][2]*thrvn[2]) / (normn * normv);
    alignv = fabs(alignv);
    //alignv *= alignv;
    double goal = alignpn*alignv*vess;

//    double nn = sqrt(thrv[0]*thrv[0]+thrv[1]*thrv[1]+thrv[2]*thrv[2]);
//    printf("goal=%f ves=(%f %f %f)*%f dir=(%f %f %f)*%f eg=[%f %f %f] vess=%f align=%f\n",
//           (float)goal,
//           (float)evec[2][0], (float)evec[2][1], (float)evec[2][2], (float)eval[2],
//           (float)thrv[0]/nn, (float)thrv[1]/nn, (float)thrv[2]/nn, (float)nn,
//           (float)eval[2], (float)eval[1], (float)eval[0],
//           (float)vess, (float)align);


//        printf("goal=%f ves=(%f %f %f)*%f eg=[%f %f %f] vess=%f align=%f*%f\n",
//               (float)goal,
//               (float)evec[2][0], (float)evec[2][1], (float)evec[2][2], (float)eval[2],
//               (float)eval[2], (float)eval[1], (float)eval[0],
//               (float)vess, (float)alignpn, (float)alignv);


    return goal;
}
