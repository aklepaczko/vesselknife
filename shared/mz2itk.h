#ifndef MZ2ITK_H
#define MZ2ITK_H

#include "../shared/byteimage.h"
#include <itkImage.h>


template <class T> class Mz2Itk
{
public:
    typedef itk::Image< T, 3 > Image3DType;


    static typename Image3DType::Pointer getItk(Byte3DImage* input, double multiplicant)
    {
        typename Image3DType::Pointer output = Image3DType::New();
        typename Image3DType::RegionType Region;
        typename Image3DType::RegionType::IndexType Start;
        typename Image3DType::RegionType::SizeType Size;

        double spac[3], orgi[3];
        for(int i = 0; i < 3; i++)
        {
            Start[i] = 0;
            Size[i]  = input->size[i];
            spac[i] = input->voxelsize[i];
            orgi[i] = 0;
        }
        Region.SetIndex( Start );
        Region.SetSize( Size );

        output->SetRegions( Region );
        output->SetOrigin(orgi);
        output->SetSpacing(spac);

        size_t numberOfPixels = output->GetLargestPossibleRegion().GetNumberOfPixels();
        T* data = new T[numberOfPixels];
        T* po = data;


        unsigned char* pi = input->data;
        unsigned char* pimax = pi + input->allsize();
        for(; pi < pimax; ++pi, ++po)
        {
            *po = *pi*multiplicant;
        }

        output->GetPixelContainer()->SetImportPointer(data, numberOfPixels, true); //true - dealocate data on delete, false - no date dealocation
        return output;
    }

    static typename Image3DType::Pointer getItk(Byte3DImage* input)
    {
        typename Image3DType::Pointer output = Image3DType::New();
        typename Image3DType::RegionType Region;
        typename Image3DType::RegionType::IndexType Start;
        typename Image3DType::RegionType::SizeType Size;

        double spac[3], orgi[3];
        for(int i = 0; i < 3; i++)
        {
            Start[i] = 0;
            Size[i]  = input->size[i];
            spac[i] = input->voxelsize[i];
            orgi[i] = 0;
        }
        Region.SetIndex( Start );
        Region.SetSize( Size );

        output->SetRegions( Region );
        output->SetOrigin(orgi);
        output->SetSpacing(spac);

        size_t numberOfPixels = output->GetLargestPossibleRegion().GetNumberOfPixels();
        T* data = new T[numberOfPixels];
        T* po = data;

        unsigned char* pi = input->data;
        unsigned char* pimax = pi + input->allsize();
        for(; pi < pimax; ++pi, ++po)
        {
            *po = (T)*pi;
        }

        output->GetPixelContainer()->SetImportPointer(data, numberOfPixels, true); //true - dealocate data on delete, false - no date dealocation
        return output;
    }


    static Byte3DImage* getMZNormalized(typename Image3DType::Pointer input)
    {
        Byte3DImage* output = new Byte3DImage();
        unsigned int size[BI_DIMENSIONS];
        unsigned int d;
        for(d = 0; d < BI_DIMENSIONS; d++) size[d] = 1;

        for(d = 0; d < 3; d++)
        {
            size[d] = input->GetLargestPossibleRegion().GetSize()[d];
        }
        output->allocate(size);
        for(d = 0; d < 3; d++)
        {
            output->voxelsize[d] = input->GetSpacing()[d];
        }

        T* data = input->GetPixelContainer()->GetImportPointer();
        T* pi = data;
        unsigned int sizex = size[0];
        unsigned int coo[BI_DIMENSIONS];
        for(unsigned int d = 0; d < BI_DIMENSIONS; d++) coo[d] = 0;

        T min, max;
        min = *pi;
        max = *pi;

        for(coo[2] = 0; coo[2] < size[2]; coo[2]++)
        {
            for(coo[1] = 0; coo[1] < size[1]; coo[1]++)
            {
                for(unsigned int x = 0; x < sizex; x++)
                {
                    if(min > *pi) min = *pi;
                    if(max < *pi) max = *pi;
                    pi++;
                }
            }
        }
        max -= min;
        pi = data;
        if(max > (T)0.000001)
        {
            for(coo[2] = 0; coo[2] < size[2]; coo[2]++)
            {
                for(coo[1] = 0; coo[1] < size[1]; coo[1]++)
                {
                    unsigned char* po = output->data + (coo[1] + coo[2]*output->size[1])*output->size[0];
                    for(unsigned int x = 0; x < sizex; x++)
                    {
                        *po = 255.0*(*pi - min)/max;
                        po++; pi++;
                    }
                }
            }
        }
        else
        {
            unsigned char v;
            if(min == 0) v = 0;
            else v = 255;
            for(coo[2] = 0; coo[2] < size[2]; coo[2]++)
            {
                for(coo[1] = 0; coo[1] < size[1]; coo[1]++)
                {
                    unsigned char* po = output->data + (coo[1] + coo[2]*output->size[1])*output->size[0];
                    for(unsigned int x = 0; x < sizex; x++)
                    {
                        *po = v;
                    }
                }
            }
        }
        return output;
    }

    static Byte3DImage* getMZ(typename Image3DType::Pointer input)
    {
        Byte3DImage* output = new Byte3DImage();
        unsigned int size[BI_DIMENSIONS];
        unsigned int d;
        for(d = 0; d < BI_DIMENSIONS; d++) size[d] = 1;

        for(d = 0; d < 3; d++)
        {
            size[d] = input->GetLargestPossibleRegion().GetSize()[d];
        }
        output->allocate(size);
        for(d = 0; d < 3; d++)
        {
            output->voxelsize[d] = input->GetSpacing()[d];
        }

        T* data = input->GetPixelContainer()->GetImportPointer();
        T* pi = data;
        unsigned int sizex = size[0];
        unsigned int coo[BI_DIMENSIONS];
        for(unsigned int d = 0; d < BI_DIMENSIONS; d++) coo[d] = 0;

        pi = data;

        for(coo[2] = 0; coo[2] < size[2]; coo[2]++)
        {
            for(coo[1] = 0; coo[1] < size[1]; coo[1]++)
            {
                unsigned char* po = output->data + (coo[1] + coo[2]*output->size[1])*output->size[0];
                for(unsigned int x = 0; x < sizex; x++)
                {
                    *po = *pi;
                    po++; pi++;
                }
            }
        }

        return output;
    }


    static Byte3DImage* getMZ(typename Image3DType::Pointer input, double shift, double multiplicant)
    {
        Byte3DImage* output = new Byte3DImage();
        unsigned int size[BI_DIMENSIONS];
        unsigned int d;
        for(d = 0; d < BI_DIMENSIONS; d++) size[d] = 1;

        for(d = 0; d < 3; d++)
        {
            size[d] = input->GetLargestPossibleRegion().GetSize()[d];
        }
        output->allocate(size);
        for(d = 0; d < 3; d++)
        {
            output->voxelsize[d] = input->GetSpacing()[d];
        }

        T* data = input->GetPixelContainer()->GetImportPointer();
        T* pi = data;
        unsigned int sizex = size[0];
        unsigned int coo[BI_DIMENSIONS];
        for(unsigned int d = 0; d < BI_DIMENSIONS; d++) coo[d] = 0;

        pi = data;

        for(coo[2] = 0; coo[2] < size[2]; coo[2]++)
        {
            for(coo[1] = 0; coo[1] < size[1]; coo[1]++)
            {
                unsigned char* po = output->data + (coo[1] + coo[2]*output->size[1])*output->size[0];
                for(unsigned int x = 0; x < sizex; x++)
                {
                    *po = (*pi-shift)*multiplicant;
                    po++; pi++;
                }
            }
        }

        return output;
    }


    static void getMinMax(typename Image3DType::Pointer input, T* min, T* max)
    {
        unsigned int size[BI_DIMENSIONS];
        unsigned int d;
        for(d = 0; d < BI_DIMENSIONS; d++) size[d] = 1;

        for(d = 0; d < 3; d++)
        {
            size[d] = input->GetLargestPossibleRegion().GetSize()[d];
        }

        T* data = input->GetPixelContainer()->GetImportPointer();
        T* pi = data;
        unsigned int sizex = size[0];
        unsigned int coo[BI_DIMENSIONS];
        for(unsigned int d = 0; d < BI_DIMENSIONS; d++) coo[d] = 0;

        *min = *pi;
        *max = *pi;

        for(coo[2] = 0; coo[2] < size[2]; coo[2]++)
        {
            for(coo[1] = 0; coo[1] < size[1]; coo[1]++)
            {
                for(unsigned int x = 0; x < sizex; x++)
                {
                    if(*min > *pi) *min = *pi;
                    if(*max < *pi) *max = *pi;
                    pi++;
                }

            }
        }
    }
};



#endif // MZ2ITK_H
