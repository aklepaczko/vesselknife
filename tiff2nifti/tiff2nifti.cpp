#-------------------------------------------------
#
# Project created by QtCreator 2014-12-28T20:40:55
#
#-------------------------------------------------

TEMPLATE = app
TARGET = tiff2nifti

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = ../Executables

SOURCES += \
    tiff2nifti.cpp

    
unix|win32: LIBS += -lniftiio

macx{
    LIBS += /usr/local/lib/libtiff.dylib
}
else:unix{
    LIBS += -ltiff
}
else:win32{
    INCLUDEPATH += D:/OpenCV/opencv/sources/3rdparty/libtiff
    LIBS += D:/OpenCV/opencv/build/x86/vc10/staticlib/libtiff.lib
    LIBS += D:/OpenCV/opencv/build/x86/vc10/staticlib/zlib.lib
}    