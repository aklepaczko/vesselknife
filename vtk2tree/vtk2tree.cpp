/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../shared/tree.h"
#include "../shared/vtkimport.h"

//=============================================================================
//=============================================================================
// Main function
int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        printf("Usage: %s input output\n", argv[0]);
        printf("Converts vessel tree models saved in vtk format to tree models for VesselKnife\n");
        printf("Version 2015.03.05 by Piotr M. Szczypinski\n");
        return 1;
    }
    Tree tree = VtkImport::getTree(argv[1]);
    if(tree.count() <= 0) return 2;
    if(!tree.save(argv[2], 0)) return 3;
    return 0;
}
